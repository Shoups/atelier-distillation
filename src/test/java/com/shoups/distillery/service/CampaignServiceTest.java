package com.shoups.distillery.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.enums.CampaignStatus;
import com.shoups.distillery.enums.ErrorCode;
import com.shoups.distillery.exception.CampaignAlreadyInProgressException;
import com.shoups.distillery.exception.CampaignCannotCloseException;
import com.shoups.distillery.exception.CampaignCannotDeleteException;
import com.shoups.distillery.exception.CampaignCannotStartException;
import com.shoups.distillery.exception.CampaignEmptyFieldException;
import com.shoups.distillery.exception.CampaignException;
import com.shoups.distillery.exception.CampaignIncorrectDatesException;
import com.shoups.distillery.exception.CampaignNotFoundException;
import com.shoups.distillery.exception.CampaignOverlappingDatesException;
import com.shoups.distillery.repository.CampaignRepository;

@ExtendWith(MockitoExtension.class)
class CampaignServiceTest {
	
	private static final LocalDate TODAY = LocalDate.of(2021, 12, 1);
	
	private static final LocalDateTime NOW = LocalDateTime.of(2021, 12, 1, 12, 0, 0);
	
	@InjectMocks
	private CampaignService tested;

	@Mock
	CampaignRepository campaignRepository;
	
    @Mock
    private TimeFactory timeFactory;

	@BeforeEach
	void setUp() {
		var campaign1 = Campaign.builder()
				.id(1l)
				.name("Campaign1")
				.startDate(LocalDate.of(2021, 1, 1))
				.endDate(LocalDate.of(2021, 6, 30))
				.status(CampaignStatus.CLOSED)
				.creationDate(NOW)
				.build();
		
		var campaign2 = Campaign.builder()
				.id(2l)
				.name("Campaign2")
				.startDate(LocalDate.of(2021, 8, 1))
				.endDate(LocalDate.of(2021, 12, 31))
				.status(CampaignStatus.IN_PROGRESS)
				.creationDate(NOW)
				.build();
		
		var campaign3 = Campaign.builder()
				.id(3l)
				.name("Campaign3")
				.startDate(LocalDate.of(2022, 1, 1))
				.endDate(LocalDate.of(2022, 1, 31))
				.status(CampaignStatus.INCOMING)
				.creationDate(NOW)
				.build();
		
		lenient().when(campaignRepository.findAll()).thenReturn(List.of(campaign1, campaign2));
		lenient().when(campaignRepository.findById(1l)).thenReturn(Optional.of(campaign1));
		lenient().when(campaignRepository.findById(2l)).thenReturn(Optional.of(campaign2));
		lenient().when(campaignRepository.findById(3l)).thenReturn(Optional.of(campaign3));
		lenient().when(timeFactory.getLocalDateNow()).thenReturn(TODAY);
		lenient().when(timeFactory.getLocalDateTimeNow()).thenReturn(NOW);
	}
	
    @Test
    void insertCampaign_nominal() throws CampaignException {
		var campaignBeforeAllCampaigns = Campaign.builder()
				.name("test")
				.startDate(LocalDate.of(2020, 12, 1))
				.endDate(LocalDate.of(2020, 12, 31))
				.build();
		
		var savedCampaign = tested.insertCampaign(campaignBeforeAllCampaigns);
		assertThat(savedCampaign.getStartDate()).isEqualTo(LocalDate.of(2020, 12, 1));
		assertThat(savedCampaign.getEndDate()).isEqualTo(LocalDate.of(2020, 12, 31));
		assertThat(savedCampaign.getStatus()).isEqualTo(CampaignStatus.INCOMING);
		assertThat(savedCampaign.getCreationDate()).isEqualTo(NOW);
		
		var campaignAfterAllCampaigns = Campaign.builder()
				.name("test")
				.startDate(LocalDate.of(2022, 1, 1))
				.endDate(LocalDate.of(2022, 6, 1))
				.build();
		
		savedCampaign = tested.insertCampaign(campaignAfterAllCampaigns);
		assertThat(savedCampaign.getStartDate()).isEqualTo(LocalDate.of(2022, 1, 1));
		assertThat(savedCampaign.getEndDate()).isEqualTo(LocalDate.of(2022, 6, 1));
		assertThat(savedCampaign.getStatus()).isEqualTo(CampaignStatus.INCOMING);
		assertThat(savedCampaign.getCreationDate()).isEqualTo(NOW);
		
		var campaignBetweenOtherCampaigns = Campaign.builder()
				.name("test")
				.startDate(LocalDate.of(2021, 7, 1))
				.endDate(LocalDate.of(2021, 7, 31))
				.build();
		
		savedCampaign = tested.insertCampaign(campaignBetweenOtherCampaigns);
		assertThat(savedCampaign.getStartDate()).isEqualTo(LocalDate.of(2021, 7, 1));
		assertThat(savedCampaign.getEndDate()).isEqualTo(LocalDate.of(2021, 7, 31));
		assertThat(savedCampaign.getStatus()).isEqualTo(CampaignStatus.INCOMING);
		assertThat(savedCampaign.getCreationDate()).isEqualTo(NOW);
    }
	
    @Test
    void insertCampaign_whenEmptyName() {
		var campaignWithNullName = Campaign.builder()
				.build();
		
		CampaignEmptyFieldException exception = assertThrows(CampaignEmptyFieldException.class, () -> tested.insertCampaign(campaignWithNullName));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_EMPTY_FIELD.getMessage());
		assertThat(exception.getField()).isEqualTo("name");
		
		var campaignWithEmptyName = Campaign.builder()
				.name("")
				.build();
		
		exception = assertThrows(CampaignEmptyFieldException.class, () -> tested.insertCampaign(campaignWithEmptyName));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_EMPTY_FIELD.getMessage());
		assertThat(exception.getField()).isEqualTo("name");
    }

    @Test
    void insertCampaign_whenEmptyDates() {
		var campaignWithNullStartDate = Campaign.builder()
				.name("test")
				.build();
		
		CampaignEmptyFieldException exception = assertThrows(CampaignEmptyFieldException.class, () -> tested.insertCampaign(campaignWithNullStartDate));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_EMPTY_FIELD.getMessage());
		assertThat(exception.getField()).isEqualTo("startDate");
		
		var campaignWithNullEndDate = Campaign.builder()
				.name("test")
				.startDate(LocalDate.of(2021, 6, 30))
				.build();
		
		exception = assertThrows(CampaignEmptyFieldException.class, () -> tested.insertCampaign(campaignWithNullEndDate));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_EMPTY_FIELD.getMessage());
		assertThat(exception.getField()).isEqualTo("endDate");
    }
    
    @Test
    void insertCampaign_whenIncorrectDates() {
		var campaignWithIncorrectDates = Campaign.builder()
				.name("test")
				.startDate(LocalDate.of(2021, 6, 30))
				.endDate(LocalDate.of(2021, 1, 1))
				.build();
		
		var exception = assertThrows(CampaignIncorrectDatesException.class, () -> tested.insertCampaign(campaignWithIncorrectDates));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_INCORRECT_DATES.getMessage());
    }

    @Test
    void insertCampaign_whenOverlappingDates() {
		var campaignOverlapsCampaign1Start = Campaign.builder()
				.name("test")
				.startDate(LocalDate.of(2020, 12, 1))
				.endDate(LocalDate.of(2021, 1, 2))
				.build();
		
		var exception = assertThrows(CampaignOverlappingDatesException.class, () -> tested.insertCampaign(campaignOverlapsCampaign1Start));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_OVERLAPPING_DATES.getMessage());
		
		var campaignOverlapsCampaign1End = Campaign.builder()
				.name("test")
				.startDate(LocalDate.of(2021, 6, 1))
				.endDate(LocalDate.of(2021, 7, 2))
				.build();
		
		exception = assertThrows(CampaignOverlappingDatesException.class, () -> tested.insertCampaign(campaignOverlapsCampaign1End));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_OVERLAPPING_DATES.getMessage());
		
		var campaignOverlapsCampaign1Within = Campaign.builder()
				.name("test")
				.startDate(LocalDate.of(2021, 2, 1))
				.endDate(LocalDate.of(2021, 3, 1))
				.build();
		
		exception = assertThrows(CampaignOverlappingDatesException.class, () -> tested.insertCampaign(campaignOverlapsCampaign1Within));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_OVERLAPPING_DATES.getMessage());
		
		var campaignOverlapsCampaign1Completely = Campaign.builder()
				.name("test")
				.startDate(LocalDate.of(2020, 12, 31))
				.endDate(LocalDate.of(2021, 7, 31))
				.build();
		
		exception = assertThrows(CampaignOverlappingDatesException.class, () -> tested.insertCampaign(campaignOverlapsCampaign1Completely));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_OVERLAPPING_DATES.getMessage());
		
		var campaignOverlapsCampaign2Start = Campaign.builder()
				.name("test")
				.startDate(LocalDate.of(2021, 7, 1))
				.endDate(LocalDate.of(2021, 8, 2))
				.build();
		
		exception = assertThrows(CampaignOverlappingDatesException.class, () -> tested.insertCampaign(campaignOverlapsCampaign2Start));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_OVERLAPPING_DATES.getMessage());
    }

    @Test
    void updateCampaign_nominal() throws CampaignException {
		var campaignBeforeAllCampaigns = Campaign.builder()
				.id(3l)
				.name("test")
				.startDate(LocalDate.of(2020, 12, 1))
				.endDate(LocalDate.of(2020, 12, 31))
				.build();
		
		var savedCampaign = tested.updateCampaign(campaignBeforeAllCampaigns);
		assertThat(savedCampaign.getId()).isEqualTo(3l);
		assertThat(savedCampaign.getName()).isEqualTo("test");
		assertThat(savedCampaign.getStartDate()).isEqualTo(LocalDate.of(2020, 12, 1));
		assertThat(savedCampaign.getEndDate()).isEqualTo(LocalDate.of(2020, 12, 31));
		assertThat(savedCampaign.getStatus()).isEqualTo(CampaignStatus.INCOMING);
		assertThat(savedCampaign.getCreationDate()).isEqualTo(NOW);

		var campaignAfterAllCampaigns = Campaign.builder()
				.id(3l)
				.name("test")
				.startDate(LocalDate.of(2022, 1, 1))
				.endDate(LocalDate.of(2022, 6, 1))
				.build();
		
		savedCampaign = tested.updateCampaign(campaignAfterAllCampaigns);
		assertThat(savedCampaign.getId()).isEqualTo(3l);
		assertThat(savedCampaign.getName()).isEqualTo("test");
		assertThat(savedCampaign.getStartDate()).isEqualTo(LocalDate.of(2022, 1, 1));
		assertThat(savedCampaign.getEndDate()).isEqualTo(LocalDate.of(2022, 6, 1));
		assertThat(savedCampaign.getStatus()).isEqualTo(CampaignStatus.INCOMING);
		assertThat(savedCampaign.getCreationDate()).isEqualTo(NOW);
		
		var campaignBetweenOtherCampaigns = Campaign.builder()
				.id(3l)
				.name("test")
				.startDate(LocalDate.of(2021, 7, 1))
				.endDate(LocalDate.of(2021, 7, 31))
				.build();
		
		savedCampaign = tested.updateCampaign(campaignBetweenOtherCampaigns);
		assertThat(savedCampaign.getId()).isEqualTo(3l);
		assertThat(savedCampaign.getName()).isEqualTo("test");
		assertThat(savedCampaign.getStartDate()).isEqualTo(LocalDate.of(2021, 7, 1));
		assertThat(savedCampaign.getEndDate()).isEqualTo(LocalDate.of(2021, 7, 31));
		assertThat(savedCampaign.getStatus()).isEqualTo(CampaignStatus.INCOMING);
		assertThat(savedCampaign.getCreationDate()).isEqualTo(NOW);
		
		var campaignChangeDates = Campaign.builder()
				.id(3l)
				.name("test")
				.startDate(LocalDate.of(2022, 1, 5))
				.endDate(LocalDate.of(2022, 1, 15))
				.build();
		
		savedCampaign = tested.updateCampaign(campaignChangeDates);
		assertThat(savedCampaign.getId()).isEqualTo(3l);
		assertThat(savedCampaign.getName()).isEqualTo("test");
		assertThat(savedCampaign.getStartDate()).isEqualTo(LocalDate.of(2022, 1, 5));
		assertThat(savedCampaign.getEndDate()).isEqualTo(LocalDate.of(2022, 1, 15));
		assertThat(savedCampaign.getStatus()).isEqualTo(CampaignStatus.INCOMING);
		assertThat(savedCampaign.getCreationDate()).isEqualTo(NOW);
    }
    
    @Test
    void updateCampaign_whenCampaignNotFound() throws CampaignException {
		var campaignNotFound = Campaign.builder()
				.id(4l)
				.name("test")
				.startDate(LocalDate.of(2022, 1, 5))
				.endDate(LocalDate.of(2022, 1, 15))
				.build();
		
		var exception = assertThrows(CampaignNotFoundException.class, () -> tested.updateCampaign(campaignNotFound));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_NOT_FOUND.getMessage());
    }
    
    @Test
    void updateCampaign_whenOverlappingDates() throws CampaignException {
		var campaignNotFound = Campaign.builder()
				.id(4l)
				.name("test")
				.startDate(LocalDate.of(2021, 9, 1))
				.endDate(LocalDate.of(2021, 10, 1))
				.build();
		
		var exception = assertThrows(CampaignOverlappingDatesException.class, () -> tested.updateCampaign(campaignNotFound));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_OVERLAPPING_DATES.getMessage());
    }

    @Test
    void startCampaign_nominal_case1() throws CampaignException {
    	when(timeFactory.getLocalDateNow()).thenReturn(LocalDate.of(2022, 1, 1));
		
		var savedCampaign = tested.startCampaign(3l);
		assertThat(savedCampaign.getName()).isEqualTo("Campaign3");
		assertThat(savedCampaign.getStartDate()).isEqualTo(LocalDate.of(2022, 1, 1));
		assertThat(savedCampaign.getEndDate()).isEqualTo(LocalDate.of(2022, 1, 31));
		assertThat(savedCampaign.getStatus()).isEqualTo(CampaignStatus.IN_PROGRESS);
		assertThat(savedCampaign.getCreationDate()).isEqualTo(NOW);
    }

    @Test
    void startCampaign_nominal_case2() throws CampaignException {
		when(timeFactory.getLocalDateNow()).thenReturn(LocalDate.of(2022, 1, 31));
		
		var savedCampaign = tested.startCampaign(3l);
		assertThat(savedCampaign.getName()).isEqualTo("Campaign3");
		assertThat(savedCampaign.getStartDate()).isEqualTo(LocalDate.of(2022, 1, 1));
		assertThat(savedCampaign.getEndDate()).isEqualTo(LocalDate.of(2022, 1, 31));
		assertThat(savedCampaign.getStatus()).isEqualTo(CampaignStatus.IN_PROGRESS);
		assertThat(savedCampaign.getCreationDate()).isEqualTo(NOW);
    }
    
    @Test
    void startCampaign_whenCannotStart() {
		var exception = assertThrows(CampaignCannotStartException.class, () -> tested.startCampaign(2l));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_CANNOT_START.getMessage());
		
		when(timeFactory.getLocalDateNow()).thenReturn(LocalDate.of(2021, 12, 31));
		exception = assertThrows(CampaignCannotStartException.class, () -> tested.startCampaign(3l));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_CANNOT_START.getMessage());
		
		when(timeFactory.getLocalDateNow()).thenReturn(LocalDate.of(2022, 2, 1));
		exception = assertThrows(CampaignCannotStartException.class, () -> tested.startCampaign(3l));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_CANNOT_START.getMessage());
    }
    
    @Test
    void startCampaign_whenCampaignAlreadyInProgress() {
		var campaignInProgress = Campaign.builder()
				.id(1l)
				.name("Campaign1")
				.startDate(LocalDate.of(2021, 8, 1))
				.endDate(LocalDate.of(2021, 12, 31))
				.status(CampaignStatus.IN_PROGRESS)
				.creationDate(NOW)
				.build();
    	
    	when(campaignRepository.findByStatusOrderByCreationDateDesc(CampaignStatus.IN_PROGRESS)).thenReturn(List.of(campaignInProgress));
    	
		var exception = assertThrows(CampaignAlreadyInProgressException.class, () -> tested.startCampaign(2l));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_ALREADY_IN_PROGRESS.getMessage());
    }
    
    @Test
    void closeCampaign_nominal() throws CampaignException {
    	when(timeFactory.getLocalDateNow()).thenReturn(LocalDate.of(2022, 1, 1));
		
		var savedCampaign = tested.closeCampaign(2l);
		assertThat(savedCampaign.getName()).isEqualTo("Campaign2");
		assertThat(savedCampaign.getStartDate()).isEqualTo(LocalDate.of(2021, 8, 1));
		assertThat(savedCampaign.getEndDate()).isEqualTo(LocalDate.of(2021, 12, 31));
		assertThat(savedCampaign.getStatus()).isEqualTo(CampaignStatus.CLOSED);
		assertThat(savedCampaign.getCreationDate()).isEqualTo(NOW);
    }
    
    @Test
    void closeCampaign_whenCannotClose() {
		var exception = assertThrows(CampaignCannotCloseException.class, () -> tested.closeCampaign(1l));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_CANNOT_CLOSE.getMessage());

		exception = assertThrows(CampaignCannotCloseException.class, () -> tested.closeCampaign(3l));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_CANNOT_CLOSE.getMessage());
    	
    	when(timeFactory.getLocalDateNow()).thenReturn(LocalDate.of(2021, 12, 30));
		exception = assertThrows(CampaignCannotCloseException.class, () -> tested.closeCampaign(2l));
		assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_CANNOT_CLOSE.getMessage());
    }
    
    @Test
    void deleteCampaign_nominal() throws CampaignException {
    	assertDoesNotThrow(() -> tested.deleteCampaign(2l));
    }
    
    @Test
    void deleteCampaign_whenCannotDelete() {
		var campaign1 = Campaign.builder()
				.id(2l)
				.name("Campaign2")
				.startDate(LocalDate.of(2021, 8, 1))
				.endDate(LocalDate.of(2021, 12, 31))
				.status(CampaignStatus.IN_PROGRESS)
				.creationDate(NOW)
				.distillations(List.of(Distillation.builder()
						.id(1l)
						.build()))
				.build();
		
		lenient().when(campaignRepository.findById(1l)).thenReturn(Optional.of(campaign1));
		
    	var exception = assertThrows(CampaignCannotDeleteException.class, () -> tested.deleteCampaign(1l));
    	assertThat(exception.getMessage()).isEqualTo(ErrorCode.CAMPAIGN_CANNOT_DELETE.getMessage());
    }
}