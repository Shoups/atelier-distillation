package com.shoups.distillery.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.shoups.distillery.enums.Period;
import com.shoups.distillery.exception.NoDistillationFoundException;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.entity.Distiller;
import com.shoups.distillery.entity.Stock;
import com.shoups.distillery.enums.CampaignStatus;
import com.shoups.distillery.exception.EmptyStockForCampaignException;

public class XLSWriterTest {

	private static final LocalDateTime NOW_DATE_TIME = LocalDateTime.of(2021, 12, 1, 12, 0, 0);
	
	private static final LocalDate NOW_DATE = LocalDate.of(2021, 12, 1);
	
	private Campaign campaign;

	private List<Distillation> distillations;
	
	@BeforeEach
	void setUp() {
		var distiller1 = Distiller.builder()
				.firstname("Jean-Claude")
				.name("Dusse")
				.address("33 Rue des Bronzés")
				.zipcode("75000")
				.city("Galaswinda")
				.phone("01 01 01 01 01")
				.build();
		
		var distiller2 = Distiller.builder()
				.firstname("Bernard")
				.name("Morin")
				.address("33 Rue des Bronzés")
				.zipcode("75000")
				.city("Galaswinda")
				.phone("02 02 02 02 02")
				.build();
		
		var stock1 = Stock.builder()
				.receptionDate(NOW_DATE)
				.pureAlcohol(50)
				.franchisedPureAlcohol(40)
				.notFranchisedPureAlcohol(30)
				.expeditionDate(NOW_DATE)
				.build();
		
		var stock2 = Stock.builder()
				.receptionDate(NOW_DATE)
				.pureAlcohol(55)
				.franchisedPureAlcohol(45)
				.notFranchisedPureAlcohol(35)
				.expeditionDate(NOW_DATE)
				.build();
		
		var stock3 = Stock.builder()
				.receptionDate(NOW_DATE)
				.pureAlcohol(60)
				.franchisedPureAlcohol(50)
				.notFranchisedPureAlcohol(30)
				.expeditionDate(NOW_DATE)
				.build();

		campaign = Campaign.builder()
				.id(1l)
				.name("Campagne 2022/2023")
				.startDate(LocalDate.of(2021, 1, 1))
				.endDate(LocalDate.of(2021, 6, 30))
				.status(CampaignStatus.CLOSED)
				.creationDate(NOW_DATE_TIME)
				.build();

		var distillation1 = Distillation.builder()
				.distillationDate(LocalDate.of(2021, 12, 31))
				.period(Period.DAY)
				.dsaNumber("12345")
				.fruit("Mirabelle")
				.quantity(55)
				.distiller(distiller1)
				.stock(stock1)
				.campaign(campaign)
				.build();
		
		var distillation2 = Distillation.builder()
				.distillationDate(LocalDate.of(2021, 1, 1))
				.period(Period.MORNING)
				.dsaNumber("78945")
				.fruit("Melon")
				.quantity(88)
				.distiller(distiller2)
				.stock(stock2)
				.campaign(campaign)
				.build();
		
		var distillation3 = Distillation.builder()
				.distillationDate(LocalDate.of(2021, 6, 1))
				.period(Period.AFTERNOON)
				.dsaNumber("45687")
				.fruit("Kiwi")
				.quantity(100)
				.distiller(distiller2)
				.stock(stock3)
				.campaign(campaign)
				.build();
		
		var distillation4 = Distillation.builder()
				.distillationDate(LocalDate.of(2021, 6, 16))
				.period(Period.DAY)
				.dsaNumber("321654")
				.fruit("Poire")
				.quantity(88)
				.distiller(distiller2)
				.stock(stock3)
				.campaign(campaign)
				.build();

		campaign.setDistillations(List.of(distillation1, distillation2, distillation3, distillation4));

		distillations = List.of(distillation1, distillation2, distillation3, distillation4);
	}

	@Disabled
	@Test
	public void testWriteExportCampaignXLS() throws IOException, EmptyStockForCampaignException {
		var inputStream = XLSWriter.writeExportCampaignXLS(campaign);
		File targetFile = new File("D:/tmp/exportCampaign.xlsx");
		Files.copy(inputStream, targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		IOUtils.closeQuietly(inputStream);
	}

	@Disabled
	@Test
	public void testWriteExportDistillationXLS() throws IOException, NoDistillationFoundException {
		var inputStream = XLSWriter.writeExportDistillationXLS(NOW_DATE, NOW_DATE.plusDays(7), distillations);
//		File targetFile = new File("D:/tmp/exportDistillation.xlsx");
		File targetFile = new File("D:\\Perso\\ADS\\Export\\exportDistillation.xlsx");
		Files.copy(inputStream, targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		IOUtils.closeQuietly(inputStream);
	}
}
