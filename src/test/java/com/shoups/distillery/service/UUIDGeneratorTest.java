package com.shoups.distillery.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class UUIDGeneratorTest {

    private final UUIDGenerator tested = new UUIDGenerator();

    @Test
    void uuidIsGenerated() {
    	for (int i = 0; i < 8; i++) {
			System.out.println(tested.generate());
		}
    	
        assertThat(tested.generate()).isNotNull();
    }

    @Test
    void uuidIsGeneratedToUpperCaseString() {
        assertThat(tested.generateToUpperCaseString()).isNotNull();
    }
}
