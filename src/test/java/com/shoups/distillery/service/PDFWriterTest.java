package com.shoups.distillery.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.itextpdf.text.DocumentException;
import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.entity.Distiller;
import com.shoups.distillery.entity.Stock;
import com.shoups.distillery.enums.CampaignStatus;
import com.shoups.distillery.exception.EmptyStockForCampaignException;

public class PDFWriterTest {

	private static final LocalDateTime NOW_DATE_TIME = LocalDateTime.of(2021, 12, 1, 12, 0, 0);
	
	private static final LocalDate NOW_DATE = LocalDate.of(2021, 12, 1);
	
	private Campaign campaign;
	
	@BeforeEach
	void setUp() {
		var distiller1 = Distiller.builder()
				.firstname("Jean-Claude")
				.name("Dusse")
				.address("33 Rue des Bronzés")
				.zipcode("75000")
				.city("Galaswinda")
				.build();
		
		var distiller2 = Distiller.builder()
				.firstname("Bernard")
				.name("Morin")
				.address("33 Rue des Bronzés")
				.zipcode("75000")
				.city("Galaswinda")
				.build();
		
		var stock1 = Stock.builder()
				.receptionDate(NOW_DATE)
				.pureAlcohol(50)
				.franchisedPureAlcohol(40)
				.notFranchisedPureAlcohol(30)
				.expeditionDate(NOW_DATE)
				.build();
		
		var stock2 = Stock.builder()
				.receptionDate(NOW_DATE)
				.pureAlcohol(55)
				.franchisedPureAlcohol(45)
				.notFranchisedPureAlcohol(35)
				.expeditionDate(NOW_DATE)
				.build();
		
		var stock3 = Stock.builder()
				.receptionDate(NOW_DATE)
				.pureAlcohol(60)
				.franchisedPureAlcohol(50)
				.notFranchisedPureAlcohol(30)
				.expeditionDate(NOW_DATE)
				.build();
		
		var distillation1 = Distillation.builder()
				.distillationDate(LocalDate.of(2021, 12, 31))
				.dsaNumber("12345")
				.quantity(55)
				.distiller(distiller1)
				.stock(stock1)
				.build();
		
		var distillation2 = Distillation.builder()
				.distillationDate(LocalDate.of(2021, 1, 1))
				.dsaNumber("78945")
				.quantity(88)
				.distiller(distiller2)
				.stock(stock2)
				.build();
		
		var distillation3 = Distillation.builder()
				.distillationDate(LocalDate.of(2021, 6, 1))
				.dsaNumber("45687")
				.quantity(100)
				.distiller(distiller2)
				.stock(stock3)
				.build();
		
		var distillation4 = Distillation.builder()
				.distillationDate(LocalDate.of(2021, 6, 16))
				.dsaNumber("321654")
				.quantity(88)
				.distiller(distiller2)
				.stock(stock3)
				.build();
		
		campaign = Campaign.builder()
				.id(1l)
				.name("Campaign1")
				.startDate(LocalDate.of(2021, 1, 1))
				.endDate(LocalDate.of(2021, 6, 30))
				.status(CampaignStatus.CLOSED)
				.creationDate(NOW_DATE_TIME)
				.distillations(List.of(distillation1, distillation2, distillation3, distillation4))
				.build();
	}

	@Disabled
	@Test
	public void testWriteExportCampaignPDF() throws DocumentException, IOException, EmptyStockForCampaignException {
		var inputStream = PDFWriter.writeExportCampaignPDF(campaign);
		File targetFile = new File("D:/tmp/export.pdf");
		Files.copy(inputStream, targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		IOUtils.closeQuietly(inputStream);
	}
}
