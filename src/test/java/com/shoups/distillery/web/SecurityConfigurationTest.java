package com.shoups.distillery.web;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SecurityConfigurationTest {

	@Autowired
	private SecurityConfiguration tested;

    @Test
    void encodePassword() {
    	String encodedPassword = tested.passwordEncoder().encode("123");
    	System.out.println(encodedPassword);
    	assertThat(encodedPassword).isNotBlank();
    }
}