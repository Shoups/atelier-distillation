package com.shoups.distillery.enums;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.shoups.distillery.exception.CampaignException;

@ExtendWith(MockitoExtension.class)
class PeriodTest {

	@Test
	void removePeriod() throws CampaignException {
		var periods = new ArrayList<>(Arrays.asList(Period.values()));
		
		assertThat(periods.size()).isEqualTo(3);
        assertThat(periods).contains(Period.MORNING, Period.AFTERNOON, Period.DAY);
        
		periods.remove(Period.AFTERNOON);
		
		assertThat(periods.size()).isEqualTo(2);
        assertThat(periods).contains(Period.MORNING, Period.DAY);
	}
}