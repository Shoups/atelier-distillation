package com.shoups.distillery.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import com.shoups.distillery.entity.Distiller;

@ActiveProfiles("test")
@DataJpaTest
class DistillerRepositoryTest {

	@Autowired
	private DistillerRepository tested;

    @Test
    void findByEmail() {
        String email = "email@domain.com";
        
        Distiller distiller = Distiller.builder()
        		.name("Name")
        		.firstname("Firstname")
        		.address("Address")
        		.zipcode("12345")
        		.city("City")
        		.email(email)
        		.creationDate(LocalDateTime.now())
        		.build();

        tested.save(distiller);

        assertThat(distiller.getId()).isNotNull();
        assertThat(tested.findByEmail(email).get().getEmail()).isEqualTo("email@domain.com");
    }
}