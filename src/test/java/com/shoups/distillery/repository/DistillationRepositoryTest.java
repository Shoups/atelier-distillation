package com.shoups.distillery.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import com.shoups.distillery.entity.Distillation;

@ActiveProfiles("test")
@DataJpaTest
class DistillationRepositoryTest {

	@Autowired
	private DistillationRepository tested;
	
	private Distillation distillation1;
	private Distillation distillation2;
	private Distillation distillation3;
	private Distillation distillation4;
	private Distillation distillation5;
	
	@BeforeEach
	void setUp() {
		distillation1 = Distillation.builder()
				.id(1l)
				.distillationDate(LocalDate.of(2021, 12, 12))
				.build();
		
		distillation2 = Distillation.builder()
				.id(2l)
				.distillationDate(LocalDate.of(2021, 12, 13))
				.build();
		
		distillation3 = Distillation.builder()
				.id(3l)
				.distillationDate(LocalDate.of(2021, 12, 15))
				.build();
		
		distillation4 = Distillation.builder()
				.id(4l)
				.distillationDate(LocalDate.of(2021, 12, 19))
				.build();
		
		distillation5 = Distillation.builder()
				.id(5l)
				.distillationDate(LocalDate.of(2021, 12, 20))
				.build();
		
		tested.saveAll(List.of(distillation1, distillation2, distillation3, distillation4, distillation5));
	}

    @Test
    void findByDistillationDateBetween() {
    	List<Distillation> distillationList = tested.findByDistillationDateBetween(LocalDate.of(2021, 12, 13), LocalDate.of(2021, 12, 19));

        assertThat(distillationList.size()).isEqualTo(3);
        assertThat(distillationList).contains(distillation2, distillation3, distillation4);
    }
}