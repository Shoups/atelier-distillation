package com.shoups.distillery.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import com.shoups.distillery.entity.Campaign;

@ActiveProfiles("test")
@DataJpaTest
class CampaignRepositoryTest {

	@Autowired
	private CampaignRepository tested;
	
	private Campaign campaign1;
	private Campaign campaign2;
	private Campaign campaign3;
	private Campaign campaign4;
	private Campaign campaign5;
	
	@BeforeEach
	void setUp() {
		campaign1 = Campaign.builder()
				.id(1l)
				.name("campaign1")
				.startDate(LocalDate.of(2021, 1, 1))
				.endDate(LocalDate.of(2021, 5, 31))
				.build();
		
		campaign2 = Campaign.builder()
				.id(2l)
				.name("campaign2")
				.startDate(LocalDate.of(2021, 5, 1))
				.endDate(LocalDate.of(2021, 6, 1))
				.build();
		
		campaign3 = Campaign.builder()
				.id(3l)
				.name("campaign3")
				.startDate(LocalDate.of(2021, 6, 5))
				.endDate(LocalDate.of(2021, 6, 10))
				.build();
		
		campaign4 = Campaign.builder()
				.id(4l)
				.name("campaign4")
				.startDate(LocalDate.of(2021, 6, 30))
				.endDate(LocalDate.of(2021, 7, 5))
				.build();
		
		campaign5 = Campaign.builder()
				.id(5l)
				.name("campaign5")
				.startDate(LocalDate.of(2021, 7, 1))
				.endDate(LocalDate.of(2021, 7, 31))
				.build();
		
		tested.saveAll(List.of(campaign1, campaign2, campaign3, campaign4, campaign5));
	}

    @Test
    void findByStartDateAfterAndEndDateBefore() {
    	List<Campaign> campaignList = tested.findByEndDateGreaterThanEqualAndStartDateLessThanEqual(LocalDate.of(2021, 6, 1), LocalDate.of(2021, 6, 30));

        assertThat(campaignList.size()).isEqualTo(3);
        assertThat(campaignList).contains(campaign2, campaign3, campaign4);
    }
}