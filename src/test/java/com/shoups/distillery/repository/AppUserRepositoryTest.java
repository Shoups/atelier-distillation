package com.shoups.distillery.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import com.shoups.distillery.entity.AppUser;

@ActiveProfiles("test")
@DataJpaTest
class AppUserRepositoryTest {

	@Autowired
	private AppUserRepository tested;

    @Test
    void findByEmail() {
        String email = "email@domain.com";
        String userName = "username";
        
        AppUser appUser = AppUser.builder()
        		.userName(userName)
        		.email(email)
        		.firstname("Firstname")
        		.name("Name")
        		.build();

        tested.save(appUser);

        assertThat(appUser.getUserId()).isNotNull();
        assertThat(tested.findByUserNameOrEmail(userName, email).get().getEmail()).isEqualTo("email@domain.com");
        assertThat(tested.findByUserNameOrEmail(userName, null).get().getEmail()).isEqualTo("email@domain.com");
        assertThat(tested.findByUserNameOrEmail(null, email).get().getEmail()).isEqualTo("email@domain.com");
        assertThat(tested.findByUserNameOrEmail(null, null).isEmpty());
        assertThat(tested.findByUserNameOrEmail("DUMMY", "DUMMY").isEmpty());
        assertThat(tested.findByUserNameOrEmail("DUMMY", null).isEmpty());
        assertThat(tested.findByUserNameOrEmail(null, "DUMMY").isEmpty());
    }
}