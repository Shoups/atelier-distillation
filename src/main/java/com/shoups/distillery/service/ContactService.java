package com.shoups.distillery.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shoups.distillery.enums.ContactType;
import com.shoups.distillery.enums.ErrorCode;
import com.shoups.distillery.exception.EmailException;
import com.shoups.distillery.model.ContactForm;
import com.shoups.distillery.web.SendmailConfiguration;

import freemarker.template.TemplateException;

@Service
public class ContactService {

	private static final String TEMPLATE_NAME = "contact.ftl";

	@Autowired
	private EmailService emailService;

	@Autowired
	private SendmailConfiguration sendmailConfiguration;

	public void sendContactForm(ContactForm contactForm) throws EmailException {
		String recipient;

		if (contactForm.getType().equals(ContactType.SITE_PROBLEM)) {
			recipient = sendmailConfiguration.getWebmasterRecipientEmail();
		} else {
			recipient = sendmailConfiguration.getContactRecipientEmail();
		}

		var subject = "Formulaire de contact : " + contactForm.getType().getLabel() + ", de la part de " + contactForm.getName();
		Map<String, Object> templateModel = new HashMap<>();
		templateModel.put("contactForm", contactForm);

		try {
			emailService.sendTemplateMail(recipient, subject, TEMPLATE_NAME, templateModel);
		} catch (IOException | TemplateException | MessagingException e) {
			throw new EmailException(ErrorCode.EMAIL_SEND_ERROR);
		}
	}
}
