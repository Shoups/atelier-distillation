package com.shoups.distillery.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.exception.CampaignNotFoundException;
import com.shoups.distillery.exception.EmailException;
import com.shoups.distillery.exception.NoActiveCampaignException;
import com.shoups.distillery.model.DistillationForm;
import com.shoups.distillery.model.Event;
import com.shoups.distillery.web.DistilleryConfiguration;

@Service
public class CalendarService {

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private DistillerService distillerService;
	
	@Autowired
	private DistillationService distillationService;
	
	@Autowired
	private DistilleryConfiguration distilleryConfiguration;
	
    public List<Event> getCampaignEventsBetweenDates(LocalDate startDate, LocalDate endDate) {
    	var eventList = new ArrayList<Event>();
    	campaignService.findByEndDateGreaterThanEqualAndStartDateLessThanEqual(startDate, endDate).forEach(distillation -> eventList.add(Event.fromCampaign(distillation)));
    	return eventList;
    }
    
    public Campaign getActiveCampaignForDistillationDate(LocalDate distillationDate) throws NoActiveCampaignException {
    	return campaignService.getActiveCampaignForDistillationDate(distillationDate);
    }
    
    public List<Event> getDistillationEventsBetweenDates(LocalDate startDate, LocalDate endDate, boolean useDefaultTitle) {
    	var eventList = new ArrayList<Event>();
    	distillationService.findByDistillationDateBetween(startDate, endDate).forEach(distillation -> eventList.add(Event.fromDistillation(distillation, useDefaultTitle, distilleryConfiguration)));
    	return eventList;
    }
    
    public List<Event> getNonWorkingDaysEventsBetweenDates(LocalDate startDate, LocalDate endDate) {
    	var eventList = new ArrayList<Event>();
    	
    	startDate.datesUntil(endDate).forEach(date -> {
    		if (date.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
    			eventList.add(Event.fromNonWorkingDay(date));
    		} else if (BusinessDayHelper.isHoliday(date)) {
    			eventList.add(Event.fromNonWorkingDay(date));
    		}
    	});
    	
    	return eventList;
    }
    
    public List<Distillation> getDistillationsByDate(LocalDate date) {
    	return distillationService.findByDistillationDate(date);
    }
    
    public void createDistillation(DistillationForm distillationForm) throws CampaignNotFoundException, EmailException {
    	var campaign = campaignService.findById(distillationForm.getCampaignId());
    	var distiller = distillerService.createOrUpdateDistiller(distillationForm);
    	var distillation = distillationService.createDistillation(campaign, distiller, distillationForm);
    	distillationForm.setDistillationId(distillation.getId());
    	distillationService.sendDistillationCreatedMails(distillationForm);
    }
}
