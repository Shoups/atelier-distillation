package com.shoups.distillery.service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.shoups.distillery.exception.NoDistillationFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.exception.EmptyStockForCampaignException;
import org.springframework.util.ResourceUtils;

@Slf4j
public class XLSWriter {

	private static final Supplier<Stream<String>> HEADERS_CAMPAIGN = ()-> Stream.of(
			"Réception des matières premières",
			"N° DSA",
			"Nom du bouilleur de cru",
			"Localité",
			"Quantité matières premières",
			"Alcool pur obtenu",
			"Alcool pur en franchisé",
			"Alcool pur en non franchisé",
			"Réexpédition des produits finis");

	private static final Supplier<Stream<String>> HEADERS_DISTILLATION = ()-> Stream.of(
			"Nom – Prénom du bouilleur de crû ou de la personne ayant la procuration",
			"Adresse et commune",
			"Téléphone",
			"Jour",
			"Horaire",
			"Type de fruits",
			"Quantité",
			"N° DSA");

	private static final int[] COLUMN_WIDTHS_CAMPAIGN = new int[] { 40, 15, 30, 50, 20, 20, 20, 20, 40 };
	private static final int[] COLUMN_WIDTHS_DISTILLATION = new int[] { 50, 75, 25,20, 20, 30, 20, 15 };

	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	private static final String ALAMBIC_IMAGE_PATH = "classpath:static/img/alambic.jpg";

	public static InputStream writeExportCampaignXLS(Campaign campaign) throws IOException, EmptyStockForCampaignException {
		Workbook workbook = new XSSFWorkbook();

		var distillations = campaign.getDistillations()
				.stream()
				.filter(distillation -> distillation.getStock() != null)
				.sorted(Comparator.comparing(Distillation::getDistillationDate))
				.collect(Collectors.toList());

		if (distillations.isEmpty()) {
			throw new EmptyStockForCampaignException(campaign);
		}

		distillations.forEach(distillation -> {
			processCampaignDistillation(workbook, campaign.getName(), distillation);
		});

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		workbook.write(out);
		workbook.close();
		return new ByteArrayInputStream(out.toByteArray());
	}

	private static void processCampaignDistillation(Workbook workbook, String campaignName, Distillation distillation) {
		var sheetName = StringUtils.capitalize(DateFormatSymbols.getInstance(Locale.FRENCH).getMonths()[distillation.getDistillationDate().getMonthValue() - 1]);

		Sheet sheet = getOrCreateSheetCampaign(workbook, campaignName, sheetName);

		var defaultContentStyle = getDefaultContentStyle(workbook);
		var dateContentStyle = getDateContentStyle(workbook);

		addDetailsRowsCampaign(sheet, defaultContentStyle, dateContentStyle, distillation);

		int columnIndex = 0;

		for (int columnWidth : COLUMN_WIDTHS_CAMPAIGN) {
			sheet.setColumnWidth(columnIndex, 180 * columnWidth);
			columnIndex ++;
		}
	}

	private static Sheet getOrCreateSheetCampaign(Workbook workbook , String campaignName, String sheetName) {
		for (int i = workbook.getNumberOfSheets() - 1; i >= 0; i--) {
			Sheet sheet = workbook.getSheetAt(i);
			if (sheet.getSheetName().equals(sheetName)) {
				return sheet;
			}
		}
		Sheet sheet = workbook.createSheet(sheetName);
		createHeaderCampaign(workbook, sheet, campaignName, sheetName);
		return sheet;
	}

	private static void createHeaderCampaign(Workbook workbook, Sheet sheet, String campaignName, String month) {
		CellStyle headerStyle = workbook.createCellStyle();
		XSSFFont headerFont = ((XSSFWorkbook) workbook).createFont();
		headerFont.setFontName("Calibri");
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setBold(true);
		headerStyle.setFont(headerFont);
		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		Row header1 = sheet.createRow(1);
		createCell(header1, 0, headerStyle, "DIRECTION REGIONALE DES DOUANES DE NANCY");

		Row header2 = sheet.createRow(2);
		createCell(header2, 0, headerStyle, "RECETTE PRINCIPALE D'EPINAL");

		sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 8));
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 8));

		setBorders(sheet, 1, 2, 0, 8);

		CellStyle infoStyle = workbook.createCellStyle();
		XSSFFont infoFont = ((XSSFWorkbook) workbook).createFont();
		infoFont.setFontName("Calibri");
		infoFont.setFontHeightInPoints((short) 10);
		infoStyle.setFont(infoFont);
		infoStyle.setAlignment(HorizontalAlignment.LEFT);
		infoStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		Row infoRow1 = sheet.createRow(5);
		createCell(infoRow1, 0, infoStyle, "CAMPAGNE DE DISTILLATION :");
		createCell(infoRow1, 2, infoStyle, campaignName);

		Row infoRow2 = sheet.createRow(7);
		createCell(infoRow2, 0, infoStyle, "SYNDICAT");
		createCell(infoRow2, 2, infoStyle, "de HADOL");

		Row infoRow3 = sheet.createRow(9);
		createCell(infoRow3, 0, infoStyle, "COMMUNE");
		createCell(infoRow3, 2, infoStyle, "HADOL");
		createCell(infoRow3, 4, infoStyle, "N° d’entrepositaire agréé :");

		Row infoRow4 = sheet.createRow(11);
		createCell(infoRow4, 0, infoStyle, "COMPTABILITE MATIERE – MOIS :");
		createCell(infoRow4, 2, infoStyle, month);

		createTableHeaderCampaign(workbook, sheet);
	}

	private static void createTableHeaderCampaign(Workbook workbook, Sheet sheet) {
		var tableHeaderStyle = getTableHeaderStyle(workbook);

		Row tableHeaderRow = sheet.createRow(13);

		AtomicInteger cellIndex = new AtomicInteger(0);

		HEADERS_CAMPAIGN.get().forEach(columnTitle -> {
			createCell(tableHeaderRow, cellIndex.get(), tableHeaderStyle, columnTitle);
			cellIndex.getAndIncrement();
		});

		tableHeaderRow.setHeight((short) 600);
	}

	private static void addDetailsRowsCampaign(Sheet sheet, CellStyle defaultStyle, CellStyle dateStyle, Distillation distillation) {
		Row row = sheet.createRow(sheet.getLastRowNum() + 1);

		createDateCell(row, 0, dateStyle, distillation.getStock().getReceptionDate());
		createCell(row, 1, defaultStyle, distillation.getDsaNumber());
		createCell(row, 2, defaultStyle, distillation.getDistiller().getFirstname() + " " + distillation.getDistiller().getName());
		createCell(row, 3, defaultStyle, distillation.getDistiller().getAddress() + " " + distillation.getDistiller().getZipcode() + " " + distillation.getDistiller().getCity());
		createNumberCell(row, 4, defaultStyle, distillation.getQuantity());
		createNumberCell(row, 5, defaultStyle, distillation.getStock().getPureAlcohol());
		createNumberCell(row, 6, defaultStyle, distillation.getStock().getFranchisedPureAlcohol());
		createNumberCell(row, 7, defaultStyle, distillation.getStock().getNotFranchisedPureAlcohol());
		createDateCell(row, 8, dateStyle, distillation.getStock().getExpeditionDate());
	}

	public static InputStream writeExportDistillationXLS(LocalDate startDate, LocalDate endDate, List<Distillation> distillations) throws IOException, NoDistillationFoundException {
		Workbook workbook = new XSSFWorkbook();

		if (distillations.isEmpty()) {
			throw new NoDistillationFoundException();
		}

		var sheetName = "S" + DateTimeFormatter.ofPattern("w").format(startDate) + org.codehaus.plexus.util.StringUtils.capitalise(startDate.getMonth().getDisplayName(TextStyle.SHORT, Locale.FRENCH));

		Sheet sheet = workbook.createSheet(sheetName);

		AtomicBoolean writeHeader = new AtomicBoolean(true);

		distillations.forEach(distillation -> {
			processDistillation(workbook, sheet, distillation, writeHeader.get(), startDate, endDate);
			writeHeader.set(false);
		});

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		workbook.write(out);
		workbook.close();
		return new ByteArrayInputStream(out.toByteArray());
	}

	private static void processDistillation(Workbook workbook, Sheet sheet, Distillation distillation, boolean writeHeader, LocalDate startDate, LocalDate endDate) {
		if (writeHeader) {
			createHeaderDistillation(workbook, sheet, distillation.getCampaign().getName(), startDate, endDate);
		}

		var defaultContentStyle = getDefaultContentStyle(workbook);
		var dateContentStyle = getDateContentStyle(workbook);

		addDetailsRowsDistillation(sheet, defaultContentStyle, dateContentStyle, distillation);

		int columnIndex = 0;

		for (int columnWidth : COLUMN_WIDTHS_DISTILLATION) {
			sheet.setColumnWidth(columnIndex, 180 * columnWidth);
			columnIndex ++;
		}
	}

	private static void createHeaderDistillation(Workbook workbook, Sheet sheet, String campaignName, LocalDate startDate, LocalDate endDate) {
		XSSFFont headerFont = ((XSSFWorkbook) workbook).createFont();
		headerFont.setFontName("Calibri");
		headerFont.setFontHeightInPoints((short) 11);
		headerFont.setBold(false);
		CellStyle headerStyle = workbook.createCellStyle();
		headerStyle.setFont(headerFont);
		headerStyle.setAlignment(HorizontalAlignment.LEFT);
		headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFFont titleFont = ((XSSFWorkbook) workbook).createFont();
		titleFont.setFontName("Calibri");
		titleFont.setFontHeightInPoints((short) 12);
		titleFont.setBold(true);
		CellStyle titleStyle = workbook.createCellStyle();
		titleStyle.setFont(titleFont);
		titleStyle.setAlignment(HorizontalAlignment.CENTER);
		titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFFont tableHeaderFont = ((XSSFWorkbook) workbook).createFont();
		tableHeaderFont.setFontName("Calibri");
		tableHeaderFont.setFontHeightInPoints((short) 11);
		tableHeaderFont.setBold(true);
		CellStyle tableHeaderStyle = workbook.createCellStyle();
		tableHeaderStyle.setFont(tableHeaderFont);
		tableHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
		tableHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		Row header1 = sheet.createRow(0);
		createCell(header1, 1, headerStyle, "ASSOCIATION DES BOUILLEURS DE CRU de HADOL");

		Row header2 = sheet.createRow(1);
		createCell(header2, 1, headerStyle, "ATELIER SIS 347 Chemin des Rapailles Senade");

		Row header3 = sheet.createRow(2);
		createCell(header3, 1, headerStyle, "ENREGISTRE SOUS LE NUMERO 4911");

		Row header4 = sheet.createRow(3);
		createCell(header4, 1, headerStyle, "RESPONSABLE DE L'ATELIER René SCHWOERER");

		Row header5 = sheet.createRow(4);
		createCell(header5, 1, headerStyle, "TELEPHONE DU RESPONSABLE 06 17 58 47 44");

		sheet.addMergedRegion(new CellRangeAddress(7, 7, 0, 7));
		sheet.addMergedRegion(new CellRangeAddress(8, 8, 0, 7));
		sheet.addMergedRegion(new CellRangeAddress(9, 9, 0, 7));
		sheet.addMergedRegion(new CellRangeAddress(10, 10, 0, 7));

		Row header6 = sheet.createRow(7);
		createCell(header6, 0, titleStyle, "CALENDRIER PREVISIONNEL POUR LA DISTILLATION");

		Row header7 = sheet.createRow(8);
		createCell(header7, 0, headerStyle, campaignName);

		Row header8 = sheet.createRow(9);
		createCell(header8, 0, headerStyle, "SEMAINE DU " + DATE_FORMAT.format(startDate) + " AU " + DATE_FORMAT.format(endDate));

		Row header9 = sheet.createRow(10);
		createCell(header9, 0, headerStyle, "ALAMBIC N°4911");

		setBorders(sheet, 0, 4, 1, 1);
		setBorders(sheet, 7, 7, 0, 7);

		try {
			var imageFile = ResourceUtils.getFile(ALAMBIC_IMAGE_PATH);
			int imageId = workbook.addPicture(Files.readAllBytes(imageFile.toPath()), Workbook.PICTURE_TYPE_JPEG);

			CreationHelper helper = workbook.getCreationHelper();
			Drawing drawing = sheet.createDrawingPatriarch();

			ClientAnchor anchor = helper.createClientAnchor();
			anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);

			anchor.setCol1(0);
			anchor.setCol2(1);
			anchor.setRow1(0);
			anchor.setRow2(7);

			drawing.createPicture(anchor, imageId);
		} catch (IOException e) {
			log.error("File {} not found", ALAMBIC_IMAGE_PATH);
		}

		createTableHeaderDistillation(workbook, sheet);
	}

	private static void createTableHeaderDistillation(Workbook workbook, Sheet sheet) {
		var tableHeaderStyle = getTableHeaderStyle(workbook);

		Row tableHeaderRow = sheet.createRow(13);

		AtomicInteger cellIndex = new AtomicInteger(0);

		HEADERS_DISTILLATION.get().forEach(columnTitle -> {
			createCell(tableHeaderRow, cellIndex.get(), tableHeaderStyle, columnTitle);
			cellIndex.getAndIncrement();
		});

		tableHeaderRow.setHeight((short) 900);
	}

	private static void addDetailsRowsDistillation(Sheet sheet, CellStyle defaultStyle, CellStyle dateStyle, Distillation distillation) {
		Row row = sheet.createRow(sheet.getLastRowNum() + 1);

		createCell(row, 0, defaultStyle, distillation.getDistiller().getFirstname() + " " + distillation.getDistiller().getName());
		createCell(row, 1, defaultStyle, distillation.getDistiller().getAddress() + " " + distillation.getDistiller().getZipcode() + " " + distillation.getDistiller().getCity());
		createCell(row, 2, defaultStyle, distillation.getDistiller().getPhone());
		createDateCell(row, 3, dateStyle, distillation.getDistillationDate());
		createCell(row, 4, defaultStyle, distillation.getPeriod().getLabel());
		createCell(row, 5, defaultStyle, distillation.getFruit());
		createCell(row, 6, defaultStyle, String.valueOf(distillation.getQuantity()));
		createCell(row, 7, defaultStyle, distillation.getDsaNumber());
	}

	private static void createCell(Row row, int cellIndex, CellStyle style, String value) {
		Cell cell = row.createCell(cellIndex);
		cell.setCellValue(value);
		cell.setCellStyle(style);
	}

	private static void createNumberCell(Row row, int cellIndex, CellStyle style, int value) {
		Cell cell = row.createCell(cellIndex);
		cell.setCellValue(value);
		cell.setCellStyle(style);
	}

	private static void createDateCell(Row row, int cellIndex, CellStyle style, LocalDate value) {
		Cell cell = row.createCell(cellIndex);
		cell.setCellValue(value);
		cell.setCellStyle(style);
	}

	private static CellStyle getTableHeaderStyle(Workbook workbook) {
		CellStyle tableHeaderStyle = workbook.createCellStyle();
		XSSFFont tableHeaderFont = ((XSSFWorkbook) workbook).createFont();
		tableHeaderFont.setFontName("Calibri");
		tableHeaderFont.setFontHeightInPoints((short) 11);
		tableHeaderFont.setBold(true);
		tableHeaderStyle.setFont(tableHeaderFont);
		tableHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
		tableHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		tableHeaderStyle.setBorderBottom(BorderStyle.THIN);
		tableHeaderStyle.setBorderTop(BorderStyle.THIN);
		tableHeaderStyle.setBorderLeft(BorderStyle.THIN);
		tableHeaderStyle.setBorderRight(BorderStyle.THIN);
		tableHeaderStyle.setWrapText(true);
		return tableHeaderStyle;
	}

	private static CellStyle getDefaultContentStyle(Workbook workbook) {
		CellStyle defaultContentStyle = workbook.createCellStyle();
		XSSFFont tableContentFont = ((XSSFWorkbook) workbook).createFont();
		tableContentFont.setFontName("Calibri");
		tableContentFont.setFontHeightInPoints((short) 11);
		defaultContentStyle.setFont(tableContentFont);
		defaultContentStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		defaultContentStyle.setBorderBottom(BorderStyle.THIN);
		defaultContentStyle.setBorderTop(BorderStyle.THIN);
		defaultContentStyle.setBorderLeft(BorderStyle.THIN);
		defaultContentStyle.setBorderRight(BorderStyle.THIN);
		return defaultContentStyle;
	}

	private static CellStyle getDateContentStyle(Workbook workbook) {
		CreationHelper creationHelper = workbook.getCreationHelper();
		CellStyle dateContentStyle = workbook.createCellStyle();
		dateContentStyle.cloneStyleFrom(getDefaultContentStyle(workbook));
		dateContentStyle.setAlignment(HorizontalAlignment.LEFT);
		dateContentStyle.setDataFormat(creationHelper.createDataFormat().getFormat("dd/mm/yyyy"));
		return dateContentStyle;
	}

	private static void setBorders(Sheet sheet, int firstRow, int lastRow, int firstCol, int lastCol) {
		CellRangeAddress headerRange = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		RegionUtil.setBorderBottom(BorderStyle.THIN, headerRange, sheet);
		RegionUtil.setBorderTop(BorderStyle.THIN, headerRange, sheet);
		RegionUtil.setBorderLeft(BorderStyle.THIN, headerRange, sheet);
		RegionUtil.setBorderRight(BorderStyle.THIN, headerRange, sheet);
	}
}
