package com.shoups.distillery.service;

import java.io.IOException;
import java.util.Map;

import javax.mail.MessagingException;

import freemarker.template.TemplateException;

public interface EmailService {
	
	void sendTemplateMail(String to, String subject, String templateName, Map<String, Object> templateModel) throws IOException, TemplateException, MessagingException;
}
