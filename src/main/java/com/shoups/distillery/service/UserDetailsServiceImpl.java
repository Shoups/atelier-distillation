package com.shoups.distillery.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.shoups.distillery.entity.AppUser;
import com.shoups.distillery.entity.UserRole;
import com.shoups.distillery.repository.AppUserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AppUserRepository appUserRepository;
    
    @Autowired  
    private HttpServletRequest request;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        AppUser appUser = this.appUserRepository.findByUserNameOrEmail(userName, userName).orElseThrow(() -> new UsernameNotFoundException("User or email " + userName + " not found"));
        
        log.debug("Utilisateur {} trouvé", userName);
        
        List<UserRole> appRoles = appUser.getUserRoles();
        
        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
        
        if (appRoles != null) {
        	for (UserRole userRole : appRoles) {
                GrantedAuthority authority = new SimpleGrantedAuthority(userRole.getAppRole().getRoleName());
                grantList.add(authority);
        	}
        }
        
        UserDetails userDetails = (UserDetails) new User(appUser.getUserName(), appUser.getEncryptedPassword(), grantList);

        request.getSession().setAttribute("user", appUser);
        request.getSession().setAttribute("isAdmin", isAdmin(userDetails));
        
        return userDetails;
	}
	
	public static boolean isAdmin(UserDetails userDetails) {
        return userDetails.getAuthorities().stream()
        		.anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_ADMIN"));
	}
}
