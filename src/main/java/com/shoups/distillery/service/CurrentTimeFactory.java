package com.shoups.distillery.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.springframework.stereotype.Component;

@Component
public class CurrentTimeFactory implements TimeFactory {

    private final ZoneId zoneId;

    protected CurrentTimeFactory() {
        this.zoneId = ZoneId.of("Europe/Paris");
    }
    
    @Override
    public Instant getInstantNow() {
        return Instant.now();
    }

    @Override
    public LocalDate getLocalDateNow() {
        return LocalDate.ofInstant(getInstantNow(), zoneId);
    }

    @Override
    public LocalDateTime getLocalDateTimeNow() {
        return LocalDateTime.ofInstant(getInstantNow(), zoneId);
    }
}
