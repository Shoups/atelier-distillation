package com.shoups.distillery.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;

public interface TimeFactory {
	
	Instant getInstantNow();

    LocalDate getLocalDateNow();

    LocalDateTime getLocalDateTimeNow();
}
