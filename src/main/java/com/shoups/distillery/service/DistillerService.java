package com.shoups.distillery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shoups.distillery.entity.Distiller;
import com.shoups.distillery.model.DistillationForm;
import com.shoups.distillery.repository.DistillerRepository;

@Service
public class DistillerService {
	
	@Autowired
    private DistillerRepository distillerRepository;
	
    @Autowired
    private TimeFactory timeFactory;
    
    public Distiller createOrUpdateDistiller(Distiller distiller) {
    	var foundDistiller = distillerRepository.findByEmail(distiller.getEmail());
    	
    	Distiller distillerToPersist;
    	
    	if (foundDistiller.isPresent()) {
    		distillerToPersist = foundDistiller.get();
    		distillerToPersist.setName(distiller.getName());
    		distillerToPersist.setFirstname(distiller.getFirstname());
    		distillerToPersist.setAddress(distiller.getAddress());
    		distillerToPersist.setZipcode(distiller.getZipcode());
    		distillerToPersist.setCity(distiller.getCity());
    		distillerToPersist.setPhone(distiller.getPhone());
    		return distillerRepository.save(distillerToPersist);
    	} else {
    		distiller.setCreationDate(timeFactory.getLocalDateTimeNow());
    		return distillerRepository.save(distiller);
    	}
    }
    
    public Distiller createOrUpdateDistiller(DistillationForm distillationForm) {
    	var distiller = Distiller.builder()
				.name(distillationForm.getName())
				.firstname(distillationForm.getFirstname())
				.address(distillationForm.getAddress())
				.zipcode(distillationForm.getZipcode())
				.city(distillationForm.getCity())
				.phone(distillationForm.getPhone())
				.email(distillationForm.getEmail())
				.build();
    	
    	return createOrUpdateDistiller(distiller);
    }
}
