package com.shoups.distillery.service;

import java.util.UUID;

import org.springframework.stereotype.Service;

@Service
public class UUIDGenerator {

    public UUID generate() {
        return UUID.randomUUID();
    }

    public String generateToUpperCaseString() {
        return generate().toString().toUpperCase();
    }
}
