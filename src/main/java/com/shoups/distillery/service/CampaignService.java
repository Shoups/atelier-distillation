package com.shoups.distillery.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;
import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.CampaignStatus;
import com.shoups.distillery.exception.CampaignAlreadyInProgressException;
import com.shoups.distillery.exception.CampaignCannotCloseException;
import com.shoups.distillery.exception.CampaignCannotDeleteException;
import com.shoups.distillery.exception.CampaignCannotStartException;
import com.shoups.distillery.exception.CampaignEmptyFieldException;
import com.shoups.distillery.exception.CampaignException;
import com.shoups.distillery.exception.CampaignIncorrectDatesException;
import com.shoups.distillery.exception.CampaignNotFoundException;
import com.shoups.distillery.exception.CampaignOverlappingDatesException;
import com.shoups.distillery.exception.NoActiveCampaignException;
import com.shoups.distillery.repository.CampaignRepository;

@Service
public class CampaignService {
	
	@Autowired
    private CampaignRepository campaignRepository;
	
    @Autowired
    private TimeFactory timeFactory;
    
    public Campaign findById(Long id) throws CampaignNotFoundException {
    	return campaignRepository.findById(id).orElseThrow(() -> new CampaignNotFoundException(id));
    }
    
    public List<Campaign> getCampaignList() {
    	return campaignRepository.findByOrderByStartDateDesc();
    }
    
    public Campaign getActiveCampaign() throws NoActiveCampaignException {
    	var campaigns = campaignRepository.findByStatusOrderByCreationDateDesc(CampaignStatus.IN_PROGRESS);
    	if (campaigns.size() > 0) {
    		return campaigns.get(0);
    	}
    	throw new NoActiveCampaignException();
    }
    
    public Campaign getActiveCampaignForDistillationDate(LocalDate distillationDate) throws NoActiveCampaignException {
    	var campaigns = campaignRepository.findByStatusOrderByCreationDateDesc(CampaignStatus.IN_PROGRESS);
    	if (campaigns.size() > 0) {
    		var campaign = campaigns.get(0);
			if (distillationDate.isBefore(campaign.getStartDate()) || distillationDate.isAfter(campaign.getEndDate())) {
				throw new NoActiveCampaignException();
			}
    		
    		return campaign;
    	}
    	throw new NoActiveCampaignException();
    }
    
    public List<Campaign> findByEndDateGreaterThanEqualAndStartDateLessThanEqual(LocalDate startDate, LocalDate endDate) {
    	return campaignRepository.findByEndDateGreaterThanEqualAndStartDateLessThanEqual(startDate, endDate);
    }
    
    public Campaign insertCampaign(Campaign campaign) throws CampaignException {
    	checkValidity(campaign);
    	
        for (Campaign existingCampaign : campaignRepository.findAll()) {
        	if (campaign.getStartDate().isBefore(existingCampaign.getEndDate()) && campaign.getEndDate().isAfter(existingCampaign.getStartDate())) {
        		throw new CampaignOverlappingDatesException(campaign);
        	}
		}
        
		campaign.setCreationDate(timeFactory.getLocalDateTimeNow());
		campaign.setStatus(CampaignStatus.INCOMING);
		campaignRepository.save(campaign);
		return campaign;
    }
    
    public Campaign updateCampaign(Campaign campaign) throws CampaignException {
    	checkValidity(campaign);
    	
		for (Campaign existingCampaign : campaignRepository.findAll()) {
			if (campaign.getId() != existingCampaign.getId() && campaign.getStartDate().isBefore(existingCampaign.getEndDate()) && campaign.getEndDate().isAfter(existingCampaign.getStartDate())) {
				throw new CampaignOverlappingDatesException(campaign);
			}
		}
		
		Campaign savedCampaign = campaignRepository.findById(campaign.getId()).orElseThrow(() -> new CampaignNotFoundException(campaign.getId()));
		savedCampaign.setName(campaign.getName());
		savedCampaign.setStartDate(campaign.getStartDate());
		savedCampaign.setEndDate(campaign.getEndDate());
        
		campaignRepository.save(savedCampaign);
		return savedCampaign;
    }
    
    public Campaign startCampaign(Long id) throws CampaignException {
    	var campaign = campaignRepository.findById(id).orElseThrow(() -> new CampaignNotFoundException(id));
    	
    	if (campaignRepository.findByStatusOrderByCreationDateDesc(CampaignStatus.IN_PROGRESS).size() > 0) {
    		throw new CampaignAlreadyInProgressException(campaign);
    	}
    	
    	if (!campaign.getStatus().equals(CampaignStatus.INCOMING)) {
    		throw new CampaignCannotStartException(campaign);
    	}
    	
		if (campaign.getStartDate().isAfter(timeFactory.getLocalDateNow()) || campaign.getEndDate().isBefore(timeFactory.getLocalDateNow())) {
			throw new CampaignCannotStartException(campaign);
		}
    	
		campaign.setStatus(CampaignStatus.IN_PROGRESS);
		campaignRepository.save(campaign);
		return campaign;
    }
    
    public Campaign closeCampaign(Long id) throws CampaignException {
    	var campaign = campaignRepository.findById(id).orElseThrow(() -> new CampaignNotFoundException(id));
    	
    	if (!campaign.getStatus().equals(CampaignStatus.IN_PROGRESS)) {
    		throw new CampaignCannotCloseException(campaign);
    	}
    	
		if (campaign.getEndDate().isAfter(timeFactory.getLocalDateNow())) {
			throw new CampaignCannotCloseException(campaign);
		}
    	
		campaign.setStatus(CampaignStatus.CLOSED);
		campaignRepository.save(campaign);
		return campaign;
    }
    
    public void deleteCampaign(Long id) throws CampaignException {
    	var campaign = campaignRepository.findById(id).orElseThrow(() -> new CampaignNotFoundException(id));
    	
    	if (campaign.getDistillations().size() > 0) {
    		throw new CampaignCannotDeleteException(campaign);
    	}
    	
		campaignRepository.delete(campaign);
    }
    
    private void checkValidity(Campaign campaign) throws CampaignException {
        if (Strings.isNullOrEmpty(campaign.getName())) {
        	throw new CampaignEmptyFieldException(campaign, "name");
        }
        
        var startDate = campaign.getStartDate();
        var endDate = campaign.getEndDate();
        
        if (startDate == null) {
        	throw new CampaignEmptyFieldException(campaign, "startDate");
        }
        
        if (endDate == null) {
        	throw new CampaignEmptyFieldException(campaign, "endDate");
        }
        
        if (startDate.isAfter(endDate)) {
        	throw new CampaignIncorrectDatesException(campaign);
        }
    }
}
