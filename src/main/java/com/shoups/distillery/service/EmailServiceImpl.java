package com.shoups.distillery.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.shoups.distillery.web.SendmailConfiguration;

import freemarker.template.Template;
import freemarker.template.TemplateException;

@Component
public class EmailServiceImpl implements EmailService {

	@Autowired
	public JavaMailSender mailSender;
	
	@Autowired
	private FreeMarkerConfigurer freemarkerConfigurer;
	
	@Autowired
	SendmailConfiguration sendmailConfiguration;

	@Override
	public void sendTemplateMail(String to, String subject, String templateName, Map<String, Object> templateModel) throws IOException, TemplateException, MessagingException {
	    Template freemarkerTemplate = freemarkerConfigurer.getConfiguration().getTemplate(templateName);
	    String htmlBody = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerTemplate, templateModel);
	    sendHtmlMessage(to, subject, htmlBody);
	}
	
	private void sendHtmlMessage(String to, String subject, String htmlBody) throws MessagingException, UnsupportedEncodingException {
		InternetAddress from = new InternetAddress(sendmailConfiguration.getSenderEmail(), sendmailConfiguration.getSenderName());
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
		helper.setFrom(from);
		helper.setTo(to);
		helper.setSubject(subject);
		helper.setText(htmlBody, true);
		mailSender.send(message);
	}
}
