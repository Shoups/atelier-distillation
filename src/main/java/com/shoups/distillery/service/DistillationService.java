package com.shoups.distillery.service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.entity.Distiller;
import com.shoups.distillery.enums.DistillationStatus;
import com.shoups.distillery.enums.ErrorCode;
import com.shoups.distillery.exception.DistillationCannotBeAcceptedException;
import com.shoups.distillery.exception.DistillationCannotBeRefusedException;
import com.shoups.distillery.exception.DistillationException;
import com.shoups.distillery.exception.DistillationNotFoundException;
import com.shoups.distillery.exception.EmailException;
import com.shoups.distillery.model.DistillationForm;
import com.shoups.distillery.repository.DistillationRepository;
import com.shoups.distillery.web.SendmailConfiguration;

import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DistillationService {
	
	private static final String DISTILLATION_CREATED_USER_TEMPLATE_NAME = "distillation-user.ftl";
	
	private static final String DISTILLATION_CREATED_ADMIN_TEMPLATE_NAME = "distillation-admin.ftl";
	
	private static final String DISTILLATION_ACCEPTED_TEMPLATE_NAME = "distillation-accepted.ftl";
	
	private static final String DISTILLATION_REFUSED_TEMPLATE_NAME = "distillation-refused.ftl";
	
	@Autowired
    private DistillationRepository distillationRepository;
	
    @Autowired
    private TimeFactory timeFactory;

	@Autowired
	private EmailService emailService;

	@Autowired
	private SendmailConfiguration sendmailConfiguration;
	
	@Autowired
	private  UUIDGenerator uuidGenerator;
    
	public Distillation findById(Long id) throws DistillationNotFoundException {
		return distillationRepository.findById(id).orElseThrow(() -> new DistillationNotFoundException(id));
	}
	
	public Distillation findByToken(String token) throws DistillationNotFoundException {
		return distillationRepository.findByToken(token).orElseThrow(() -> new DistillationNotFoundException(token));
	}
	
	public List<Distillation> findByCampaign(Campaign campaign) {
		return distillationRepository.findByCampaignOrderByStatusAscDistillationDateDesc(campaign);
	}
	
	public List<Distillation> findByDistillationDate(LocalDate date) {
		return distillationRepository.findByDistillationDate(date);
	}
	
	public List<Distillation> findByDistillationDateBetween(LocalDate startDate, LocalDate endDate) {
		return distillationRepository.findByDistillationDateBetween(startDate, endDate);
	}
    
    public Distillation createDistillation(Campaign campaign, Distiller distiller, DistillationForm distillationForm) {
    	var distillation = Distillation.builder()
    			.distillationDate(distillationForm.getDistillationDate())
    			.period(distillationForm.getPeriod())
    			.fruit(distillationForm.getFruit())
    			.quantity(distillationForm.getQuantity())
    			.dsaNumber(distillationForm.getDsaNumber())
    			.status(DistillationStatus.WAITING)
    			.creationDate(timeFactory.getLocalDateTimeNow())
    			.token(uuidGenerator.generateToUpperCaseString())
    			.distiller(distiller)
    			.campaign(campaign)
    			.build();
    	
    	return distillationRepository.save(distillation);
    }
    
    public Distillation acceptDistillation(Long id) throws DistillationException {
    	var distillation = distillationRepository.findById(id).orElseThrow(() -> new DistillationNotFoundException(id));
    	
    	if (!distillation.getStatus().equals(DistillationStatus.WAITING)) {
    		throw new DistillationCannotBeAcceptedException(distillation);
    	}
    	
    	distillation.setStatus(DistillationStatus.ACCEPTED);
    	distillationRepository.save(distillation);
    	
    	try {
			sendDistillationAcceptedMail(distillation);
		} catch (EmailException e) {
			log.error("Erreur lors de l'envoi du mail d'acceptation d'une demande de distillation : {}", e.getMessage());
		}
    	
		return distillation;
    }
    
    public Distillation refuseDistillation(Long id) throws DistillationException {
    	var distillation = distillationRepository.findById(id).orElseThrow(() -> new DistillationNotFoundException(id));
    	
    	if (!distillation.getStatus().equals(DistillationStatus.WAITING)) {
    		throw new DistillationCannotBeRefusedException(distillation);
    	}
    	
    	distillation.setStatus(DistillationStatus.REFUSED);
    	distillationRepository.save(distillation);
    	
    	try {
			sendDistillationRefusedMail(distillation);
		} catch (EmailException e) {
			log.error("Erreur lors de l'envoi du mail de refus d'une demande de distillation : {}", e.getMessage());
		}
    	
		return distillation;
    }
    
	public void sendDistillationCreatedMails(DistillationForm distillationForm) throws EmailException {
		var viewUri = ServletUriComponentsBuilder.fromCurrentServletMapping().toUriString() + "/admin/distillation/view/" + distillationForm.getDistillationId();
		
		Map<String, Object> templateModel = new HashMap<>();
		templateModel.put("distillationForm", distillationForm);
		templateModel.put("link", viewUri);

		try {
			emailService.sendTemplateMail(sendmailConfiguration.getContactRecipientEmail(), "Nouvelle demande de distillation", DISTILLATION_CREATED_ADMIN_TEMPLATE_NAME, templateModel);
			emailService.sendTemplateMail(distillationForm.getEmail(), "Confirmation de votre demande de distillation", DISTILLATION_CREATED_USER_TEMPLATE_NAME, templateModel);
		} catch (IOException | TemplateException | MessagingException e) {
			throw new EmailException(ErrorCode.EMAIL_SEND_ERROR);
		}
	}
	
	public void sendDistillationAcceptedMail(Distillation distillation) throws EmailException {
		var viewUri = ServletUriComponentsBuilder.fromCurrentServletMapping().toUriString() + "/stock/" + distillation.getToken();
		
		Map<String, Object> templateModel = new HashMap<>();
		templateModel.put("distillation", distillation);
		templateModel.put("link", viewUri);

		try {
			emailService.sendTemplateMail(distillation.getDistiller().getEmail(), "Votre demande de distillation a été acceptée", DISTILLATION_ACCEPTED_TEMPLATE_NAME, templateModel);
		} catch (IOException | TemplateException | MessagingException e) {
			throw new EmailException(ErrorCode.EMAIL_SEND_ERROR);
		}
	}
	
	public void sendDistillationRefusedMail(Distillation distillation) throws EmailException {
		Map<String, Object> templateModel = new HashMap<>();
		templateModel.put("distillation", distillation);

		try {
			emailService.sendTemplateMail(distillation.getDistiller().getEmail(), "Votre demande de distillation a été refusée", DISTILLATION_REFUSED_TEMPLATE_NAME, templateModel);
		} catch (IOException | TemplateException | MessagingException e) {
			throw new EmailException(ErrorCode.EMAIL_SEND_ERROR);
		}
	}
}
