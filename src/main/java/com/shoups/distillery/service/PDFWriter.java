package com.shoups.distillery.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormatSymbols;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.exception.EmptyStockForCampaignException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PDFWriter {

	private static final Supplier<Stream<String>> HEADERS = ()-> Stream.of(
			"Réception des matières premières",
			"N° DSA",
			"Nom du bouilleur de cru",
			"Localité",
			"Quantité matières premières",
			"Alcool pur obtenu",
			"Alcool pur en franchisé",
			"Alcool pur en non franchisé",
			"Réexpédition des produits finis");
	
	private static final float[] COLUMN_WIDTHS = new float[] { 40f, 15f, 30f, 50f, 20f, 20f, 20f, 20f, 40f };
	
	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	public static InputStream writeExportCampaignPDF(Campaign campaign) throws FileNotFoundException, DocumentException, EmptyStockForCampaignException {
		Document document = new Document(PageSize.A4, 50, 50, 50, 50);
		document.setPageSize(PageSize.A4.rotate());
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		PdfWriter.getInstance(document, out);
		
		Map<String, List<Distillation>> distillationsByMonth = campaign.getDistillations().stream()
				.filter(distillation -> distillation.getStock() != null)
				.sorted(Comparator.comparing(Distillation::getDistillationDate))
				.collect(Collectors.groupingBy(distillation -> StringUtils.capitalize(DateFormatSymbols.getInstance(Locale.FRENCH).getMonths()[distillation.getDistillationDate().getMonthValue() - 1])));
		
		Font titleFont = FontFactory.getFont(FontFactory.HELVETICA, 20, BaseColor.BLACK);
		Font bodyFont = FontFactory.getFont(FontFactory.HELVETICA, 12, BaseColor.BLACK);
		
		if (distillationsByMonth.isEmpty()) {
			throw new EmptyStockForCampaignException(campaign);
		}
		
		document.open();
		
		distillationsByMonth.forEach((month, distillations) -> {
			try {
				addParagraph(document, String.format("DIRECTION REGIONALE DES DOUANES DE NANCY\nRECETTE PRINCIPALE D'EPINAL\n\n"), titleFont, Element.ALIGN_CENTER);
				addParagraph(document, "CAMPAGNE DE DISTILLATION " + campaign.getName(), bodyFont, Element.ALIGN_LEFT);
				addParagraph(document, "SYNDICAT de HADOL", bodyFont, Element.ALIGN_LEFT);
				addParagraph(document, "COMMUNE HADOL", bodyFont, Element.ALIGN_LEFT);
				addParagraph(document, "COMPTABILITE MATIERE – MOIS " + month, bodyFont, Element.ALIGN_LEFT);
				
				PdfPTable table = new PdfPTable(9);
				
				addTableHeader(table);
					
				distillations.forEach(distillation -> {
					addRows(table, distillation);
				});
				
				table.setSpacingBefore(30f);
		        table.setWidths(COLUMN_WIDTHS);
		        table.setWidthPercentage(100);
				
				document.add(table);
				document.newPage();
			} catch (DocumentException e) {
				log.error("Erreur lors de l'export des distillations de la campagne d'ID {} en PDF ", campaign.getId());
			}
		});
		
		document.close();
		
		return new ByteArrayInputStream(out.toByteArray());
	}
	
	private static void addParagraph(Document document, String text, Font font, int alignement) throws DocumentException {
		Paragraph paragraph = new Paragraph(30, text, font);
		paragraph.setAlignment(alignement);
		document.add(paragraph);
	}
	
	private static void addTableHeader(PdfPTable table) {
		HEADERS.get().forEach(columnTitle -> {
			PdfPCell header = new PdfPCell();
			header.setHorizontalAlignment(Element.ALIGN_CENTER);
			header.setVerticalAlignment(Element.ALIGN_MIDDLE);
			header.setBackgroundColor(BaseColor.LIGHT_GRAY);
			header.setBorderWidth(1);
			header.setPhrase(new Phrase(columnTitle));
			table.addCell(header);
		});
	}
	
	private static void addRows(PdfPTable table, Distillation distillation) {
		table.addCell(createAlignCenterCell(DATE_FORMAT.format(distillation.getStock().getReceptionDate())));
		table.addCell(createAlignRightCell(distillation.getDsaNumber()));
		table.addCell(createAlignLeftCell(distillation.getDistiller().getFirstname() + " " + distillation.getDistiller().getName()));
		table.addCell(createAlignLeftCell(distillation.getDistiller().getAddress() + " " + distillation.getDistiller().getZipcode() + " " + distillation.getDistiller().getCity()));
		table.addCell(createAlignCenterCell(distillation.getQuantity() + " kg"));
		table.addCell(createAlignCenterCell(String.valueOf(distillation.getStock().getPureAlcohol())));
		table.addCell(createAlignCenterCell(String.valueOf(distillation.getStock().getFranchisedPureAlcohol())));
		table.addCell(createAlignCenterCell(String.valueOf(distillation.getStock().getNotFranchisedPureAlcohol())));
		table.addCell(createAlignCenterCell(DATE_FORMAT.format(distillation.getStock().getExpeditionDate())));
	}
	
	private static PdfPCell createAlignCenterCell(String content) {
		PdfPCell centeredCell = new PdfPCell(new Phrase(content));
		centeredCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		centeredCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		return centeredCell;
	}
	
	private static PdfPCell createAlignLeftCell(String content) {
		PdfPCell centeredCell = new PdfPCell(new Phrase(content));
		centeredCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		centeredCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		return centeredCell;
	}
	
	private static PdfPCell createAlignRightCell(String content) {
		PdfPCell centeredCell = new PdfPCell(new Phrase(content));
		centeredCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		centeredCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		return centeredCell;
	}
}
