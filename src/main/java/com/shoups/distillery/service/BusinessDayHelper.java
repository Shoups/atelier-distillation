package com.shoups.distillery.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;

public class BusinessDayHelper {

    private BusinessDayHelper() {
        // No need to init
    }

    public static boolean isBusinessDay(final LocalDate date) {
        // Week-end
        if (date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
            return false;
        }

        return !isHoliday(date);
    }

    public static boolean isBankBusinessDay(final LocalDate date) {
        // Week-end
        if (date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
            return false;
        }

        return !isBankHoliday(date);
    }

    public static boolean isHoliday(final LocalDate date) {
        // New Year's Day : 01/01
        if (date.getDayOfMonth() == 1 && date.getMonth().equals(Month.JANUARY)) {
            return true;
        }

        // Labour day : 01/05
        if (date.getDayOfMonth() == 1 && date.getMonth().equals(Month.MAY)) {
            return true;
        }

        // Victory 1945 : 08/05
        if (date.getDayOfMonth() == 8 && date.getMonth().equals(Month.MAY)) {
            return true;
        }

        // Bastille day : 14/07
        if (date.getDayOfMonth() == 14 && date.getMonth().equals(Month.JULY)) {
            return true;
        }

        // Assumption day : 15/08
        if (date.getDayOfMonth() == 15 && date.getMonth().equals(Month.AUGUST)) {
            return true;
        }

        // All Saints' day : 01/11
        if (date.getDayOfMonth() == 1 && date.getMonth().equals(Month.NOVEMBER)) {
            return true;
        }

        // Armistice day : 11/11
        if (date.getDayOfMonth() == 11 && date.getMonth().equals(Month.NOVEMBER)) {
            return true;
        }

        // Christmas day : 25/12
        if (date.getDayOfMonth() == 25 && date.getMonth().equals(Month.DECEMBER)) {
            return true;
        }

        // Easter monday
        if (date.isEqual(getEasterMonday(date.getYear()))) {
            return true;
        }

        // Ascension day
        if (date.isEqual(getAscensionDay(date.getYear()))) {
            return true;
        }

        // Pentecost monday
        if (date.isEqual(getPentecostMonday(date.getYear()))) {
            return true;
        }

        return false;
    }

    public static boolean isBankHoliday(final LocalDate date) {
        // Good friday
        if (date.isEqual(getGoodFriday(date.getYear()))) {
            return true;
        }

        // St. Stephen's Day : 26/12
        if (date.getDayOfMonth() == 26 && date.getMonth().equals(Month.DECEMBER)) {
            return true;
        }

        return isHoliday(date);
    }

    public static LocalDate getNextBankBusinessDay(final LocalDate date) {
        LocalDate nextBankBusinessDay = date.plusDays(1);
        while (!isBankBusinessDay(nextBankBusinessDay)) {
            nextBankBusinessDay = nextBankBusinessDay.plusDays(1);
        }
        return nextBankBusinessDay;
    }

    protected static LocalDate getEasterMonday(final int year) {
        return getEasterDate(year).plusDays(1);
    }

    protected static LocalDate getGoodFriday(final int year) {
        return getEasterDate(year).minusDays(2);
    }

    protected static LocalDate getAscensionDay(final int year) {
        return getEasterDate(year).plusDays(39);
    }

    protected static LocalDate getPentecostMonday(final int year) {
        return getEasterDate(year).plusDays(50);
    }

    protected static LocalDate getEasterDate(final int year) {
        int n = year % 19;
        int c = year / 100;
        int u = year % 100;
        int s = c / 4;
        int t = c % 4;
        int p = (c + 8) / 25;
        int q = (c - p + 1) / 3;
        int e = (19 * n + c - s - q + 15) % 30;
        int b = u / 4;
        int d = u % 4;
        int l = (32 + 2 * t + 2 * b - e - d) % 7;
        int h = (n + 11 * e + 22 * l) / 451;
        int f = e + l - 7 * h + 114;
        int month = f / 31;
        int day = f % 31 + 1;

        return LocalDate.of(year, month, day);
    }
}
