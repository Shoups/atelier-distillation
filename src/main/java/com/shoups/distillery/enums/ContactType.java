package com.shoups.distillery.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ContactType {

	INFORMATION("Demande d'information"),
	WEBMASTER("Contact webmaster"),
	SITE_PROBLEM("Problème sur le site"),
	OTHER("Autre");
	
	private final String label;
	
	public static ContactType getByName(String name) {
		for (ContactType contactType : ContactType.values()) {
			if (contactType.name().equalsIgnoreCase(name)) {
				return contactType;
			}
		}
		return INFORMATION;
	}
}
