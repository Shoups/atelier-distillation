package com.shoups.distillery.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Period {

	MORNING("Matin", "#ffb500"),
	AFTERNOON("Après-midi", "#0091c1"),
	DAY("Journée", "#00c17b");
	
	private final String label;
	
	private final String color;
}
