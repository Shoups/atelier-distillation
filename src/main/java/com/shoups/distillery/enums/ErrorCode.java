package com.shoups.distillery.enums;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCode {

	EMAIL_SEND_ERROR("Une erreur s'est produite lors de l'envoi de l'e-mail", HttpStatus.INTERNAL_SERVER_ERROR),
	CAMPAIGN_NOT_FOUND("La campagne est introuvable", HttpStatus.NOT_FOUND),
	CAMPAIGN_EMPTY_FIELD("Ce champ ne doit pas être vide", HttpStatus.BAD_REQUEST),
	CAMPAIGN_INCORRECT_DATES("La date de fin doit être ultérieure à la date début", HttpStatus.BAD_REQUEST),
	CAMPAIGN_OVERLAPPING_DATES("Les dates de la campagne sont en conflit avec une autre", HttpStatus.CONFLICT),
	CAMPAIGN_ALREADY_IN_PROGRESS("Une campagne est déjà en cours", HttpStatus.BAD_REQUEST),
	CAMPAIGN_CANNOT_START("La campagne ne peut pas être démarrée", HttpStatus.BAD_REQUEST),
	CAMPAIGN_CANNOT_CLOSE("La campagne ne peut pas être clôturée", HttpStatus.BAD_REQUEST),
	CAMPAIGN_CANNOT_DELETE("La campagne ne peut pas être supprimée", HttpStatus.BAD_REQUEST),
	EMPTY_STOCK_FOR_CAMPAIGN("La campagne n'a aucune comptabilité des matières liée", HttpStatus.NOT_FOUND),
	NO_ACTIVE_CAMPAIGN("Aucune campagne active", HttpStatus.NOT_FOUND),
	DISTILLATION_NOT_FOUND("La distillation est introuvable", HttpStatus.NOT_FOUND),
	NO_DISTILLATION_FOUND("Aucune distillation trouvée", HttpStatus.NOT_FOUND),
	DISTILLATION_CANNOT_BE_ACCEPTED("La distillation ne peut pas être acceptée", HttpStatus.BAD_REQUEST),
	DISTILLATION_CANNOT_BE_REFUSED("La campagne ne peut pas être refusée", HttpStatus.BAD_REQUEST);
	
	private final String message;
	
	private final HttpStatus httpStatus;
}