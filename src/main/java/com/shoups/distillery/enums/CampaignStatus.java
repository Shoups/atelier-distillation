package com.shoups.distillery.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CampaignStatus {

	INCOMING("A venir", "#f6ffc1"),
	IN_PROGRESS("En cours", "#d7ffd8"),
	CLOSED("Clôturée", "#dedede");
	
	private final String label;
	
	private final String color;
}
