package com.shoups.distillery.enums;

public enum ExportFormat {
	PDF,
	XLS;
	
	public static ExportFormat getByValue(String value) {
		for (ExportFormat exportFormat : ExportFormat.values()) {
			if (exportFormat.toString().equalsIgnoreCase(value)) {
				return exportFormat;
			}
		}
		throw new IllegalArgumentException("Format d'export invalide : " + value);
	}
}
