package com.shoups.distillery.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DistillationStatus {

	WAITING("En attente", "#bfbfbf"),
	ACCEPTED("Acceptée", "#00ff54"),
	REFUSED("Refusée", "#ff0054"),
	NOT_AVAILABLE("Non disponible", "#333333");
	
	private final String label;
	
	private final String color;
}
