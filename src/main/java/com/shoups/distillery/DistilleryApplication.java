package com.shoups.distillery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistilleryApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistilleryApplication.class, args);
	}
}
