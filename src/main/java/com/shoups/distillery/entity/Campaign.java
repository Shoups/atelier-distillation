package com.shoups.distillery.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

import com.shoups.distillery.enums.CampaignStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Campaign implements Serializable {
	
	private static final long serialVersionUID = -4720483164225994757L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;

	@NotBlank
	private String name;
	
	@Column(name="start_date")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate startDate;
	
	@Column(name="end_date")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate endDate;
	
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('INCOMING', 'IN_PROGRESS', 'CLOSED')")
	private CampaignStatus status;
	
    @Column(name="creation_date")
	private LocalDateTime creationDate;
    
    @OneToMany(mappedBy="campaign")
    @Builder.Default
    @ToString.Exclude
    private List<Distillation> distillations = new ArrayList<>();
}