package com.shoups.distillery.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Stock  implements Serializable {
	
	private static final long serialVersionUID = 6348777549279216845L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;

	@Column(name="reception_date")
	private LocalDate receptionDate;
	
	private String locality;
	
	private int quantity;
	
	@Column(name="pure_alcohol")
	private int pureAlcohol;
	
	@Column(name="franchised_pure_alcohol")
	private int franchisedPureAlcohol;
	
	@Column(name="not_franchised_pure_alcohol")
	private int notFranchisedPureAlcohol;
	
	@Column(name="expedition_date")
	private LocalDate expeditionDate;
	
    @Column(name="creation_date")
	private LocalDateTime creationDate;
	
    @OneToOne(fetch = FetchType.EAGER)
    @ToString.Exclude
    private Distillation distillation;
}
