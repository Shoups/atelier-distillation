package com.shoups.distillery.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.shoups.distillery.enums.DistillationStatus;
import com.shoups.distillery.enums.Period;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Distillation implements Serializable {
	
	private static final long serialVersionUID = -3966898979067181567L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;

	@Column(name="distillation_date")
	private LocalDate distillationDate;
	
	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = "ENUM('MORNING', 'AFTERNOON')")
	private Period period;
	
	private String fruit;
	
	private int quantity;
	
	@Column(name="dsa_number")
	private String dsaNumber;
	
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('WAITING', 'ACCEPTED', 'REFUSED')")
	private DistillationStatus status;
	
    @Column(name="creation_date")
	private LocalDateTime creationDate;
    
    private String token;
	
    @ManyToOne
    @ToString.Exclude
    private Distiller distiller;

    @ManyToOne
    @ToString.Exclude
    private Campaign campaign;
    
    @OneToOne(mappedBy="distillation")
    @ToString.Exclude
    private Stock stock;
}