package com.shoups.distillery.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "app_user")
public class AppUser {
	
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    @Column(name="user_id")
    private Long userId;

    @Column(name="user_name")
    private String userName;
    
    private String email;
    
    private String name;
    
    private String firstname;

    @Column(name="encrypted_password")
    private String encryptedPassword;

    private boolean enabled;
    
    @OneToMany(mappedBy = "appUser", fetch = FetchType.EAGER)
	private List<UserRole> userRoles;
}
