package com.shoups.distillery.model;

import javax.validation.constraints.NotBlank;

import com.shoups.distillery.enums.ContactType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ContactForm {
	
	private ContactType type;
	
	@NotBlank
	private String name;
	
	@NotBlank
	private String email;
	
	@NotBlank
	private String object;
	
	@NotBlank
	private String text;
}