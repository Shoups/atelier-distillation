package com.shoups.distillery.model;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import com.shoups.distillery.enums.Period;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class DistillationForm {
	
	private Long campaignId;
	
	private Long distillationId;
	
	@NotBlank
	private String name;
	
	@NotBlank
	private String firstname;
	
	@NotBlank
	private String address;

	@NotBlank
	private String zipcode;
	
	@NotBlank
	private String city;

	@NotBlank
	private String phone;
	
	@NotBlank
	@Email
	private String email;
	
	private LocalDate distillationDate;
	
	private Period period;
	
	@NotBlank
	private String fruit;
	
	@Positive
	private int quantity;
	
	private String dsaNumber;
}