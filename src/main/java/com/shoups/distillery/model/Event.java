package com.shoups.distillery.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.enums.DistillationStatus;
import com.shoups.distillery.enums.Period;
import com.shoups.distillery.web.DistilleryConfiguration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class Event {
	
	private static final DateTimeFormatter CALENDAR_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	private static final DateTimeFormatter CALENDAR_DATETIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
	
	private String title;
	
	private String start;
	
	private String end;
	
	private String color;
	
	private String display;
	
	public static Event fromDistillation(Distillation distillation, boolean useDefaultTitle, DistilleryConfiguration distilleryConfiguration) {
		return Event.builder()
			.title("")
			.start(getDistillationStartDateTime(distillation, distilleryConfiguration))
			.end(getDistillationEndDateTime(distillation, distilleryConfiguration))
			.color(distillation.getStatus().equals(DistillationStatus.WAITING) ? DistillationStatus.WAITING.getColor() : distillation.getPeriod().getColor())
			.build();
	}
	
	public static Event fromCampaign(Campaign campaign) {
		return Event.builder()
			.title("")
			.start(CALENDAR_DATE_FORMAT.format(campaign.getStartDate()))
			.end(CALENDAR_DATE_FORMAT.format(campaign.getEndDate().atTime(23, 59, 59)))
			.display("background")
			.color(campaign.getStatus().getColor())
			.build();
	}
	
	public static Event fromNonWorkingDay(LocalDate date) {
		return Event.builder()
			.title("")
			.start(CALENDAR_DATE_FORMAT.format(date.atStartOfDay()))
			.end(CALENDAR_DATE_FORMAT.format(date.atTime(23, 59, 59)))
			.display("background")
			.color(DistillationStatus.NOT_AVAILABLE.getColor())
			.build();
	}
	
	private static String getDistillationStartDateTime(Distillation distillation, DistilleryConfiguration distilleryConfiguration) {
		return (distillation.getPeriod().equals(Period.MORNING) || distillation.getPeriod().equals(Period.DAY))
				? CALENDAR_DATETIME_FORMAT.format(distillation.getDistillationDate().atTime(distilleryConfiguration.getCalendarMorningStartHour(), 0, 0))
				: CALENDAR_DATETIME_FORMAT.format(distillation.getDistillationDate().atTime(distilleryConfiguration.getCalendarAfternoonStartHour(), 0, 0));
	}
	
	private static String getDistillationEndDateTime(Distillation distillation, DistilleryConfiguration distilleryConfiguration) {
		return (distillation.getPeriod().equals(Period.MORNING))
				? CALENDAR_DATETIME_FORMAT.format(distillation.getDistillationDate().atTime(distilleryConfiguration.getCalendarMorningEndHour(), 0, 0))
				: CALENDAR_DATETIME_FORMAT.format(distillation.getDistillationDate().atTime(distilleryConfiguration.getCalendarAfternoonEndHour(), 0, 0));
	}
}
