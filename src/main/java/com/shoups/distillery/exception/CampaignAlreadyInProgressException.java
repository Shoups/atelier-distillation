package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class CampaignAlreadyInProgressException extends CampaignException {

	private static final long serialVersionUID = 3956292485197857620L;

	public CampaignAlreadyInProgressException(Campaign campaign) {
		super(campaign, ErrorCode.CAMPAIGN_ALREADY_IN_PROGRESS);
	}
}
