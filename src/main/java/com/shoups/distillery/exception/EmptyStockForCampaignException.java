package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class EmptyStockForCampaignException extends CampaignException {

	private static final long serialVersionUID = -8775908150165507403L;
	
	public EmptyStockForCampaignException(Campaign campaign) {
		super(campaign, ErrorCode.EMPTY_STOCK_FOR_CAMPAIGN);
	}
}
