package com.shoups.distillery.exception;

import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class NoActiveCampaignException extends CampaignException {

	private static final long serialVersionUID = -7202691613251773456L;

	public NoActiveCampaignException() {
		super(null, ErrorCode.NO_ACTIVE_CAMPAIGN);
	}
}
