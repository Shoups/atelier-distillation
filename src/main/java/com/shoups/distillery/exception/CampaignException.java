package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class CampaignException extends DistilleryException {

	private static final long serialVersionUID = -2084881898357184315L;
	
	private final Campaign campaign;
	
	public CampaignException(Campaign campaign, ErrorCode errorCode) {
		super(errorCode);
		this.campaign = campaign;
	}
}
