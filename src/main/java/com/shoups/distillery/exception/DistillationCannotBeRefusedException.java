package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class DistillationCannotBeRefusedException extends DistillationException {

	private static final long serialVersionUID = -4592267685278440222L;

	public DistillationCannotBeRefusedException(Distillation distillation) {
		super(distillation, ErrorCode.DISTILLATION_CANNOT_BE_REFUSED);
	}
}
