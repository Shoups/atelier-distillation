package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class CampaignCannotStartException extends CampaignException {

	private static final long serialVersionUID = -3693429894187850627L;

	public CampaignCannotStartException(Campaign campaign) {
		super(campaign, ErrorCode.CAMPAIGN_CANNOT_START);
	}
}
