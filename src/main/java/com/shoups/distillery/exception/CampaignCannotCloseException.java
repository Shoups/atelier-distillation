package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class CampaignCannotCloseException extends CampaignException {

	private static final long serialVersionUID = 1117028504338062738L;

	public CampaignCannotCloseException(Campaign campaign) {
		super(campaign, ErrorCode.CAMPAIGN_CANNOT_CLOSE);
	}
}
