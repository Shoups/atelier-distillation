package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class DistillationException extends DistilleryException {

	private static final long serialVersionUID = 1200282703914877862L;
	
	private final Distillation distillation;
	
	public DistillationException(Distillation distillation, ErrorCode errorCode) {
		super(errorCode);
		this.distillation = distillation;
	}
}
