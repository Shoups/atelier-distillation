package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.ErrorCode;
import lombok.Getter;

@Getter
public class NoDistillationFoundException extends DistilleryException {

	private static final long serialVersionUID = -5314374960253621062L;

	public NoDistillationFoundException() {
		super(ErrorCode.NO_DISTILLATION_FOUND);
	}
}
