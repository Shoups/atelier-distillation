package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class CampaignIncorrectDatesException extends CampaignException {

	private static final long serialVersionUID = 4935926183387102989L;

	public CampaignIncorrectDatesException(Campaign campaign) {
		super(campaign, ErrorCode.CAMPAIGN_INCORRECT_DATES);
	}
}
