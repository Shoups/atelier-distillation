package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class DistillationCannotBeAcceptedException extends DistillationException {

	private static final long serialVersionUID = -3693429894187850627L;

	public DistillationCannotBeAcceptedException(Distillation distillation) {
		super(distillation, ErrorCode.DISTILLATION_CANNOT_BE_ACCEPTED);
	}
}
