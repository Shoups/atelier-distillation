package com.shoups.distillery.exception;

import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class DistillationNotFoundException extends DistillationException {

	private static final long serialVersionUID = -2923634661452701433L;
	
	private final Long id;
	
	private final String token;
	
	public DistillationNotFoundException(Long id) {
		super(null, ErrorCode.DISTILLATION_NOT_FOUND);
		this.id = id;
		this.token = "";
	}
	
	public DistillationNotFoundException(String token) {
		super(null, ErrorCode.DISTILLATION_NOT_FOUND);
		this.id = -1l;
		this.token = token;
	}
}
