package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class CampaignEmptyFieldException extends CampaignException {

	private static final long serialVersionUID = 9213116265344639974L;

	private final String field;
	
	public CampaignEmptyFieldException(Campaign campaign, String field) {
		super(campaign, ErrorCode.CAMPAIGN_EMPTY_FIELD);
		this.field = field;
	}
}
