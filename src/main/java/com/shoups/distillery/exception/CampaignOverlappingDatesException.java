package com.shoups.distillery.exception;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class CampaignOverlappingDatesException extends CampaignException {

	private static final long serialVersionUID = 6756338625985252519L;
	
	public CampaignOverlappingDatesException(Campaign campaign) {
		super(campaign, ErrorCode.CAMPAIGN_OVERLAPPING_DATES);
	}
}
