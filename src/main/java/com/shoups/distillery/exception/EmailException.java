package com.shoups.distillery.exception;

import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class EmailException extends DistilleryException {

	private static final long serialVersionUID = -4656916933398648885L;

	public EmailException(ErrorCode errorCode) {
		super(errorCode);
	}
}
