package com.shoups.distillery.exception;

import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class DistilleryException extends Exception {

	private static final long serialVersionUID = -4155599781431186179L;

	private final ErrorCode errorCode;
	
	public DistilleryException(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}
	
	@Override
	public String getMessage() {
		return this.errorCode.getMessage();
	}
}
