package com.shoups.distillery.exception;

import com.shoups.distillery.enums.ErrorCode;

import lombok.Getter;

@Getter
public class CampaignNotFoundException extends CampaignException {

	private static final long serialVersionUID = 9213116265344639974L;

	private final Long id;
	
	public CampaignNotFoundException(Long id) {
		super(null, ErrorCode.CAMPAIGN_NOT_FOUND);
		this.id = id;
	}
}
