package com.shoups.distillery.web;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

@Data
@Component
@Validated
@ConfigurationProperties(prefix = "distillery")
public class DistilleryConfiguration {

	private boolean calendarDisplayEventTime;
	
	private int calendarMorningStartHour;
	
	private int calendarMorningEndHour;
	
	private int calendarAfternoonStartHour;
	
	private int calendarAfternoonEndHour;
}
