package com.shoups.distillery.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.shoups.distillery.entity.Stock;
import com.shoups.distillery.enums.DistillationStatus;
import com.shoups.distillery.enums.ErrorCode;
import com.shoups.distillery.exception.DistillationNotFoundException;
import com.shoups.distillery.exception.EmailException;
import com.shoups.distillery.service.DistillationService;
import com.shoups.distillery.service.EmailService;
import com.shoups.distillery.service.StockService;
import com.shoups.distillery.service.TimeFactory;
import com.shoups.distillery.web.SendmailConfiguration;

import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class StockController {

	private static final String STOCK_CREATED_USER_TEMPLATE_NAME = "stock-user.ftl";
	
	private static final String STOCK_CREATED_ADMIN_TEMPLATE_NAME = "stock-admin.ftl";
	
	@Autowired
	private DistillationService distillationService;
	
	@Autowired
	private StockService stockService;
	
	@Autowired
	private EmailService emailService;

	@Autowired
	private SendmailConfiguration sendmailConfiguration;
	
    @Autowired
    private TimeFactory timeFactory;
	
	@GetMapping("/stock/{token}")
	public String fillStockShowForm(@PathVariable String token, Model model) {
		try {
			var distillation = distillationService.findByToken(token);
			
			if (distillation.getStatus().equals(DistillationStatus.ACCEPTED) && distillation.getDistillationDate().isBefore(timeFactory.getLocalDateNow())) {
				model.addAttribute("distillation", distillation);
				
				var foundStock = stockService.findByDistillation(distillation);
				
		    	if (foundStock.isPresent()) {
		    		model.addAttribute("action", "edit");
		    		model.addAttribute("stock", foundStock);
		    	} else {
		    		model.addAttribute("action", "add");
		    		model.addAttribute("stock", Stock.builder().distillation(distillation).build());
		    	}
			} else {
				log.warn("La distillation du token {} n'est pas échue", token);
				model.addAttribute("errorMessage", "Cette distillation n'est pas échue, vous ne pouvez pas encore remplir la comptabilité des matières.");
				return "error";
			}
		} catch (DistillationNotFoundException e) {
			log.warn("La distillation du token {} est introuvable", token);
			model.addAttribute("errorMessage", "La distillation est introuvable.");
			return "error";
		}
		
		return "stock-edit";
	}
	
	@PostMapping("/stock")
	public String fillStock(Stock stock, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			bindingResult.getAllErrors().forEach(error -> log.warn("Erreur lors de la création de la comptabilité des matières : {}", error.getDefaultMessage()));
			return "stock-edit";
		}
		
		stock.setCreationDate(timeFactory.getLocalDateTimeNow());
		
		var persistedStock = stockService.createStock(stock);
		
    	try {
    		sendStockCreatedMail(persistedStock);
		} catch (EmailException e) {
			log.error("Erreur lors de l'envoi du mail de création de la comptabilité des matières : {}", e.getMessage());
		}
		
		log.info("Comptabilité des matières créée avec succès");
		model.addAttribute("stock", persistedStock);
		return "stock-confirm";
	}
	
	public void sendStockCreatedMail(Stock stock) throws EmailException {
		var viewUri = ServletUriComponentsBuilder.fromCurrentServletMapping().toUriString() + "/stock/" + stock.getDistillation().getToken();
		var viewAdminUri = ServletUriComponentsBuilder.fromCurrentServletMapping().toUriString() + "/admin/stock/" + stock.getDistillation().getToken();
		
		Map<String, Object> templateModel = new HashMap<>();
		templateModel.put("stock", stock);
		templateModel.put("link", viewUri);
		templateModel.put("adminLink", viewAdminUri);

		try {
			emailService.sendTemplateMail(sendmailConfiguration.getContactRecipientEmail(), "Saisie de la comptabilité des matières", STOCK_CREATED_ADMIN_TEMPLATE_NAME, templateModel);
			emailService.sendTemplateMail(stock.getDistillation().getDistiller().getEmail(), "Confirmation de la saisie de la comptabilité des matières", STOCK_CREATED_USER_TEMPLATE_NAME, templateModel);
		} catch (IOException | TemplateException | MessagingException e) {
			throw new EmailException(ErrorCode.EMAIL_SEND_ERROR);
		}
	}
}
