package com.shoups.distillery.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.itextpdf.text.DocumentException;
import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.ExportFormat;
import com.shoups.distillery.exception.CampaignException;
import com.shoups.distillery.exception.CampaignNotFoundException;
import com.shoups.distillery.exception.EmptyStockForCampaignException;
import com.shoups.distillery.service.CampaignService;
import com.shoups.distillery.service.PDFWriter;
import com.shoups.distillery.service.TimeFactory;
import com.shoups.distillery.service.XLSWriter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/admin/campaign")
public class CampaignController {

	@Autowired
	private CampaignService campaignService;
	
	@Autowired
	private TimeFactory timeFactory;
	
	private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HHmmss");
    
	@GetMapping("list")
	public String listCampaigns(Model model) {
		model.addAttribute("campaigns", campaignService.getCampaignList());
		return "admin/campaign-list";
	}
	
	@GetMapping("add")
	public String addCampaignShowForm(Model model) {
		model.addAttribute("campaign", new Campaign());
		model.addAttribute("action", "add");
		return "admin/campaign-edit";
	}
	
	@PostMapping("add")
	public String addCampaignDo(@Valid Campaign campaign, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			bindingResult.getAllErrors().forEach(error -> log.warn("Erreur lors de la création de la campagne : {}", error.getDefaultMessage()));
			model.addAttribute("action", "add");
			return "admin/campaign-edit";
		}
		
		try {
			campaignService.insertCampaign(campaign);
		} catch (CampaignException e) {
			log.warn("Erreur lors de la création de la campagne [{}] : {}", e.getCampaign().getName(), e.getMessage());
			ObjectError error = new ObjectError("globalError", e.getMessage());
			bindingResult.addError(error);
			model.addAttribute("action", "add");
			return "admin/campaign-edit";
		}
		
		log.info("Campagne [{}] créée avec succès", campaign.getName());		
		return "redirect:/admin/campaign/list";
	}
	
	@GetMapping("edit/{id}")
	public String editCampaignShowForm(@PathVariable Long id, Model model) {
		try {
			model.addAttribute("campaign", campaignService.findById(id));
		} catch (CampaignNotFoundException e) {
			log.warn("La campagne d'ID {} est introuvable", id);
			model.addAttribute("campaigns", campaignService.getCampaignList());
			model.addAttribute("errorMessage", e.getMessage());
			return "admin/campaign-list";
		}
		
		model.addAttribute("action", "edit");
		return "admin/campaign-edit";
	}
	
	@PostMapping("edit")
	public String editCampaignDo(@Valid Campaign campaign, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			bindingResult.getAllErrors().forEach(error -> log.warn("Erreur lors de la modification de la campagne : {}", error.getDefaultMessage()));
			model.addAttribute("action", "edit");
			return "admin/campaign-edit";
		}
		
		try {
			campaignService.updateCampaign(campaign);
		} catch (CampaignException e) {
			log.warn("Erreur lors de la modification de la campagne [{}] : {}", e.getCampaign().getName(), e.getMessage());
			ObjectError error = new ObjectError("globalError", e.getMessage());
			bindingResult.addError(error);
			model.addAttribute("action", "edit");
			return "admin/campaign-edit";
		}
		
		log.info("Campagne {} modifiée avec succès", campaign.getName());
		return "redirect:/admin/campaign/list";
	}
	
	@PostMapping("start/{id}")
	public ResponseEntity<String> startCampaign(@PathVariable Long id) {
		try {
			campaignService.startCampaign(id);
			return new ResponseEntity<>("/admin/campaign/list", HttpStatus.OK);
		} catch (CampaignException e) {
			log.error("Erreur lors du démarrage de la campagne [{}] : {}", e.getCampaign().getName(), e.getMessage());
			return new ResponseEntity<>("Une erreur s'est produite lors du démarrage de la campagne " + e.getCampaign().getName() + " : " + e.getErrorCode().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("close/{id}")
	public ResponseEntity<String> closeCampaign(@PathVariable Long id) {
		try {
			campaignService.closeCampaign(id);
			return new ResponseEntity<>("/admin/campaign/list", HttpStatus.OK);
		} catch (CampaignException e) {
			log.error("Erreur lors de la clôture de la campagne [{}] : {}", e.getCampaign().getName(), e.getMessage());
			return new ResponseEntity<>("Une erreur s'est produite lors de la clôture de la campagne " + e.getCampaign().getName() + " : " + e.getErrorCode().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("delete/{id}")
	public ResponseEntity<String> deleteCampaig(@PathVariable Long id) {
		try {
			campaignService.deleteCampaign(id);
			return new ResponseEntity<>("/admin/campaign/list", HttpStatus.OK);
		} catch (CampaignException e) {
			log.error("Erreur lors de la suppression de la campagne [{}] : {}", e.getCampaign().getName(), e.getMessage());
			return new ResponseEntity<>("Une erreur s'est produite lors de la suppression de la campagne " + e.getCampaign().getName() + " : " + e.getErrorCode().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("export/{id}")
	public void exportCampaign(@PathVariable Long id, @RequestParam String format, HttpServletResponse response) throws IOException {
		try {
			var exportFormat = ExportFormat.getByValue(format);
			var campaign = campaignService.findById(id);
			
			InputStream exportFile = null;
			String filename = null;
			
			switch(exportFormat) {
			case PDF:
				filename = "export_campagne_" + campaign.getName().replaceAll("[^a-zA-Z0-9.-]", "_") + "_" + timeFactory.getLocalDateTimeNow().format(DATE_TIME_FORMAT) + ".pdf";
				exportFile = PDFWriter.writeExportCampaignPDF(campaign);
				response.setContentType("application/pdf");
				break;
			case XLS:
				filename = "export_campagne_" + campaign.getName().replaceAll("[^a-zA-Z0-9.-]", "_") + "_" + timeFactory.getLocalDateTimeNow().format(DATE_TIME_FORMAT) + ".xlsx";
				exportFile = XLSWriter.writeExportCampaignXLS(campaign);
				response.setContentType("application/vnd.ms-excel");
				break;
			default:
				response.sendRedirect("/admin/campaign/list");
				break;
			}
			
			response.addHeader("Content-Disposition", "attachment; filename=" + filename);
			
			try (OutputStream out = response.getOutputStream()) {
				byte[] buffer = new byte[1024];
				
				int bytesRead;
				while ((bytesRead = exportFile.read(buffer)) > 0) {
					out.write(buffer, 0, bytesRead);
				}
			}
			
			response.getOutputStream().flush();
		} catch (IllegalArgumentException e) {
			log.warn("Le format d'export {} est inconnu", format);
			response.sendRedirect("/admin/campaign/list");
		} catch (CampaignNotFoundException e) {
			log.warn("La campagne d'ID {} est introuvable", id);
			response.sendRedirect("/admin/campaign/list");
		} catch (DocumentException e) {
			log.warn("Une erreur s'est produite lors de l'export des distillations de la campagne d'ID {} : {}", id, e.getMessage());
			response.sendRedirect("/admin/campaign/list");
		} catch (EmptyStockForCampaignException e) {
			log.warn(e.getMessage());
			response.sendRedirect("/admin/campaign/list");
		}
	}
}
