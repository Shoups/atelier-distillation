package com.shoups.distillery.web.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.shoups.distillery.enums.ContactType;
import com.shoups.distillery.exception.EmailException;
import com.shoups.distillery.model.ContactForm;
import com.shoups.distillery.service.ContactService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ContactController {

	@Autowired
	private ContactService contactService;
	
	@GetMapping("/contact")
	public String contactShowForm(@RequestParam(value = "type") Optional<String> type, Model model) {
		ContactType contactType;
		
		if (type.isPresent()) {
			contactType = ContactType.getByName(type.get());
		} else {
			contactType = ContactType.INFORMATION;
		}
		
		var contactForm = ContactForm.builder().type(contactType).build();
		
		model.addAttribute("contactForm", contactForm);
		model.addAttribute("contactTypes", ContactType.values());
		return "contact";
	}
	
	@PostMapping("/contact")
	public String contactDo(@Valid ContactForm contactForm, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			bindingResult.getAllErrors().forEach(error -> log.warn("Erreur lors de l'envoi du formulaire de contact : {}", error.getDefaultMessage()));
			model.addAttribute("contactTypes", ContactType.values());
			return "contact";
		}
		
		try {
			contactService.sendContactForm(contactForm);
		} catch (EmailException e) {
			log.warn("Erreur lors de l'envoi du formulaire de contact : {}", e.getMessage());
			ObjectError error = new ObjectError("globalError", e.getMessage());
			bindingResult.addError(error);
			model.addAttribute("contactTypes", ContactType.values());
			return "contact";
		}
		
		log.info("Formulaire de contact envoyé avec succès");		
		return "contact-sent";
	}
}
