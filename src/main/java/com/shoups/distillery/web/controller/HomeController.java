package com.shoups.distillery.web.controller;

import java.security.Principal;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.shoups.distillery.service.UserDetailsServiceImpl;

@Controller
public class HomeController {

	@GetMapping({ "/", "/index", "/home" })
	public String welcome(Principal principal) {
		if (principal != null) {
			User loggedUser = (User) ((Authentication) principal).getPrincipal();
			
			if (UserDetailsServiceImpl.isAdmin(loggedUser)) {
				return "redirect:/admin/home";
			}
		}
		
		return "home";
	}
	
	@GetMapping("/admin/home")
	public String adminHome() {
		return "admin/home";
	}
	
	@GetMapping("/gallery")
	public String gallery() {
		return "gallery";
	}
	
	@GetMapping("/find-us")
	public String findUs() {
		return "find-us";
	}
	
	@GetMapping("/rules")
	public String rules() {
		return "rules";
	}
	
	@GetMapping("/legal")
	public String legal() {
		return "legal";
	}
	
	@GetMapping("/declaration-distillation")
	public String declarationDistillation() {
		return "declaration-distillation";
	}
}
