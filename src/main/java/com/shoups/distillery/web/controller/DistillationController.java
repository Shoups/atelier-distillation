package com.shoups.distillery.web.controller;

import com.itextpdf.text.DocumentException;
import com.shoups.distillery.enums.ExportFormat;
import com.shoups.distillery.exception.*;
import com.shoups.distillery.service.*;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Slf4j
@Controller
@RequestMapping("/admin/distillation")
public class DistillationController {

	private static final DateTimeFormatter DATE_FORMAT_FRENCH = DateTimeFormatter.ofPattern("d MMMM yyyy", Locale.FRENCH);
	private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HHmmss");
	@Autowired
	private DistillationService distillationService;
	
	@Autowired
	private CampaignService campaignService;
	
    @Autowired
    private TimeFactory timeFactory;
    
	@GetMapping("list")
	public String listDistillations(Model model) {
		try {
			var distillations = distillationService.findByCampaign(campaignService.getActiveCampaign());
			model.addAttribute("distillations", distillations);
			model.addAttribute("now", timeFactory.getLocalDateNow());
		} catch (NoActiveCampaignException e) {
			model.addAttribute("errorMessage", e.getMessage());
			return "admin/error";
		}
		
		return "admin/distillation-list";
	}
	
	@GetMapping("view/{id}")
	public String viewDistillation(@PathVariable Long id, Model model) {
		try {
			model.addAttribute("distillation", distillationService.findById(id));
			model.addAttribute("now", timeFactory.getLocalDateNow());
		} catch (DistillationNotFoundException e) {
			log.warn("La distillation d'ID {} est introuvable", id);
			return "redirect:/admin/distillation/list";
		}
		
		return "admin/distillation-view";
	}
	
	@GetMapping("accept/{id}")
	public String acceptDistillationGet(@PathVariable Long id, Model model) {
		try {
			distillationService.acceptDistillation(id);
			return "redirect:/admin/distillation/view/" + id;
		} catch (DistillationException e) {
			log.error("Erreur lors de l'acceptation de la distillation d'ID {} : {}", id, e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return "admin/error";
		}
	}

	@GetMapping("refuse/{id}")
	public String refuseDistillationGet(@PathVariable Long id, Model model) {
		try {
			distillationService.refuseDistillation(id);
			return "redirect:/admin/distillation/view/" + id;
		} catch (DistillationException e) {
			log.error("Erreur lors du refus de la distillation d'ID {} : {}", id, e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return "admin/error";
		}
	}
	
	@PostMapping("accept/{id}")
	public ResponseEntity<String> acceptDistillationPost(@PathVariable Long id) {
		try {
			distillationService.acceptDistillation(id);
			return new ResponseEntity<>("/admin/distillation/list", HttpStatus.OK);
		} catch (DistillationException e) {
			log.error("Erreur lors de l'acceptation de la distillation du [{} {}] : {}", e.getDistillation().getDistillationDate(), e.getDistillation().getPeriod().getLabel(), e.getMessage());
			return new ResponseEntity<>("Une erreur s'est produite lors de l'acceptation de la distillation du " + e.getDistillation().getDistillationDate() + " (" + e.getDistillation().getPeriod().getLabel() + ") : " + e.getErrorCode().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("refuse/{id}")
	public ResponseEntity<String> refuseDistillationPost(@PathVariable Long id) {
		try {
			distillationService.refuseDistillation(id);
			return new ResponseEntity<>("/admin/distillation/list", HttpStatus.OK);
		} catch (DistillationException e) {
			log.error("Erreur lors du refus de la distillation du [{} {}] : {}", e.getDistillation().getDistillationDate(), e.getDistillation().getPeriod().getLabel(), e.getMessage());
			return new ResponseEntity<>("Une erreur s'est produite lors du refus de la distillation du " + e.getDistillation().getDistillationDate() + " (" + e.getDistillation().getPeriod().getLabel() + ") : " + e.getErrorCode().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("export/{date}")
	public void exportDistillations(@PathVariable String date, HttpServletResponse response) throws IOException {
		var exportDate = LocalDate.parse(date, DATE_FORMAT_FRENCH);
		var startDate = exportDate.with(DayOfWeek.MONDAY);
		var endDate = exportDate.with(DayOfWeek.SUNDAY);

		try {
			var distillations = distillationService.findByDistillationDateBetween(startDate, endDate);
			var filename = "Calendrier_Prévisionnel_" + exportDate.getYear() + "_" + timeFactory.getLocalDateTimeNow().format(DATE_TIME_FORMAT) + ".xlsx";

			var exportFile = XLSWriter.writeExportDistillationXLS(startDate, endDate, distillations);
			response.setContentType("application/vnd.ms-excel");
			response.addHeader("Content-Disposition", "attachment; filename=" + filename);

			try (OutputStream out = response.getOutputStream()) {
				byte[] buffer = new byte[1024];

				int bytesRead;
				while ((bytesRead = exportFile.read(buffer)) > 0) {
					out.write(buffer, 0, bytesRead);
				}
			}

			response.getOutputStream().flush();
		} catch (NoDistillationFoundException e) {
			log.warn("Aucune distillation trouvée pour la semaine du {} au {}", DATE_FORMAT_FRENCH.format(startDate), DATE_FORMAT_FRENCH.format(endDate));
			response.sendRedirect("/admin/calendar");
		}
	}
}
