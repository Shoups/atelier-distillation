package com.shoups.distillery.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class LoginController {

	@GetMapping("/login")
	public String login(HttpSession session, Model model) {
		Exception loginException = (Exception) session.getAttribute("SPRING_SECURITY_LAST_EXCEPTION");

		if (loginException != null) {			
			log.warn("Login Exception : {}", loginException.getMessage());			
			model.addAttribute("loginError", "Nom d'utilisateur ou mot de passe incorrect");			
		}
		return "login";
	}

	@GetMapping("/access-denied")
	public String accessDenied(HttpSession session, Model model) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		log.info("Déconnexion de l'utilisateur {}", authentication.getName());
		model.addAttribute("userName", authentication.getName());
		return "access-denied";
	}
}
