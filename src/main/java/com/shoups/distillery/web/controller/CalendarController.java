package com.shoups.distillery.web.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import javax.validation.Valid;

import com.shoups.distillery.service.BusinessDayHelper;
import com.shoups.distillery.service.TimeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.shoups.distillery.enums.CampaignStatus;
import com.shoups.distillery.enums.DistillationStatus;
import com.shoups.distillery.enums.Period;
import com.shoups.distillery.exception.CampaignException;
import com.shoups.distillery.exception.CampaignNotFoundException;
import com.shoups.distillery.exception.EmailException;
import com.shoups.distillery.model.DistillationForm;
import com.shoups.distillery.model.Event;
import com.shoups.distillery.service.CalendarService;
import com.shoups.distillery.web.DistilleryConfiguration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class CalendarController {
	
	private static final DateTimeFormatter CALENDAR_DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssxxx");
	
	private static final DateTimeFormatter CALENDAR_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	@Autowired
	private CalendarService calendarService;
	
	@Autowired
	private DistilleryConfiguration distilleryConfiguration;

	@Autowired
	private TimeFactory timeFactory;

	@GetMapping("/calendar")
	public String showCalendar(Model model) {
		fillModel(model);
		return "calendar";
	}
	
	@GetMapping("/admin/calendar")
	public String showAdminCalendar(Model model) {
		fillModel(model);
		return "admin/calendar";
	}

	private void fillModel(Model model) {
		Map<String, String> campaignCaptions = new HashMap<>();
		Map<String, String> distillationCaptions = new HashMap<>();
		campaignCaptions.put("Campagne de distillation en cours", CampaignStatus.IN_PROGRESS.getColor());
		distillationCaptions.put("Distillation en attente de confirmation", DistillationStatus.WAITING.getColor());
		distillationCaptions.put("Distillation réservée le matin", Period.MORNING.getColor());
		distillationCaptions.put("Distillation réservée l'après-midi", Period.AFTERNOON.getColor());
		distillationCaptions.put("Distillation réservée la journée complète", Period.DAY.getColor());
		distillationCaptions.put("Distillation non disponible", DistillationStatus.NOT_AVAILABLE.getColor());

		model.addAttribute("campaignCaptions", campaignCaptions);
		model.addAttribute("distillationCaptions", distillationCaptions);
		model.addAttribute("displayEventTime", distilleryConfiguration.isCalendarDisplayEventTime());
	}
	
    @GetMapping(value = "/calendar/events", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Event>> getEvents(@RequestParam(value = "start") String start, @RequestParam(value = "end") String end) {
    	var startDate = LocalDate.parse(start, CALENDAR_DATE_TIME_FORMAT);
    	var endDate = LocalDate.parse(end, CALENDAR_DATE_TIME_FORMAT);
    	
    	List<Event> events = new ArrayList<>();
    	
    	var campaignEvents = calendarService.getCampaignEventsBetweenDates(startDate, endDate);
    	var distillationEvents = calendarService.getDistillationEventsBetweenDates(startDate, endDate, true);
    	log.info("{} évènements de distillation trouvés pour la période du {} au {}", distillationEvents.size(), start, end);
    	
    	events.addAll(campaignEvents);
    	events.addAll(distillationEvents);
    	events.addAll(calendarService.getNonWorkingDaysEventsBetweenDates(startDate, endDate));
    	
    	return new ResponseEntity<>(events, HttpStatus.OK);
    }
    
    @GetMapping(value = "/admin/calendar/events", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Event>> getAdminEvents(@RequestParam(value = "start") String start, @RequestParam(value = "end") String end) {
    	var startDate = LocalDate.parse(start, CALENDAR_DATE_TIME_FORMAT);
    	var endDate = LocalDate.parse(end, CALENDAR_DATE_TIME_FORMAT);
    	
    	List<Event> events = new ArrayList<>();
    	
    	var campaignEvents = calendarService.getCampaignEventsBetweenDates(startDate, endDate);
    	var distillationEvents = calendarService.getDistillationEventsBetweenDates(startDate, endDate, false);
    	log.info("{} évènements de distillation trouvés pour la période du {} au {}", distillationEvents.size(), start, end);
    	
    	events.addAll(campaignEvents);
    	events.addAll(distillationEvents);
    	
    	return new ResponseEntity<>(events, HttpStatus.OK);
    }
    
	@GetMapping("/calendar/add")
	public String addDistillationShowForm(Model model, @RequestParam("date") String date) {
		var distillationDate = LocalDate.parse(date, CALENDAR_DATE_FORMAT);

		if (timeFactory.getLocalDateNow().isAfter(distillationDate)) {
			model.addAttribute("warningMessage", "Vous ne pouvez pas réserver pour une journée échue.");
			return "include/warning";
		}

		if (BusinessDayHelper.isHoliday(distillationDate)) {
			model.addAttribute("warningMessage", "Cette journée n'est pas disponible, veuillez sélectionner une autre journée.");
			return "include/warning";
		}

		log.info("Création d'une demande de distillation pour la date du {}", date);		
		
		try {
			var activeCampaign = calendarService.getActiveCampaignForDistillationDate(distillationDate);
			var periods = new ArrayList<>(Arrays.asList(Period.values()));
			
			var distillations = calendarService.getDistillationsByDate(distillationDate);
			if (distillations.size() > 0) {
				periods.remove(Period.DAY);
			}
			
			distillations.forEach(distillation -> periods.remove(distillation.getPeriod()));
			
			model.addAttribute("distillationForm", DistillationForm.builder().campaignId(activeCampaign.getId()).distillationDate(distillationDate).build());
			model.addAttribute("periods", periods);
			return "include/distillation-add";
		} catch (CampaignException e) {
			log.warn("Erreur lors de la création d'une demande de distillation : {}", e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return "include/error";
		}
	}
	
	@PostMapping("/calendar/add")
	public String addDistillationDo(@Valid DistillationForm distillationForm, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			AtomicReference<String> errors = new AtomicReference<>("");
			bindingResult.getAllErrors().forEach(error -> {
				errors.set(errors.get() + "<br/>Le champ " + error.getCode() + " " + error.getDefaultMessage());
				log.warn("Erreur lors de la création d'une demande de distillation : {}", error.getDefaultMessage());
				});
			model.addAttribute("errorMessages", errors);
			return "errors";
		}
		
		try {
			calendarService.createDistillation(distillationForm);
		} catch (CampaignNotFoundException e) {
			log.error("Erreur lors de la création d'une demande de distillation : {}", e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return "calendar";
		} catch (EmailException e) {
			log.error("Erreur lors de l'envoi des mails de création d'une demande de distillation : {}", e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return "calendar";
		}
		
		log.info("Demande de distillation créée avec succès");		
		return "redirect:/calendar";
	}
}
