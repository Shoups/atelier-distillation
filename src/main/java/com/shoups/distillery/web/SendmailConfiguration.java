package com.shoups.distillery.web;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import lombok.Data;

@Data
@Component
@Validated
@ConfigurationProperties(prefix = "sendmail")
public class SendmailConfiguration {

	@NotBlank
	private String templatesPath;
	
	@Email
	private String contactRecipientEmail;

	@Email
	private String webmasterRecipientEmail;

	@Email
	private String senderEmail;

	@NotBlank
	private String senderName;

    @Bean 
    public FreeMarkerConfigurer freemarkerClassLoaderConfig() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_31);
        TemplateLoader templateLoader = new ClassTemplateLoader(this.getClass(), "/" + templatesPath);
        configuration.setTemplateLoader(templateLoader);
        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setConfiguration(configuration);
        return freeMarkerConfigurer; 
    }
}
