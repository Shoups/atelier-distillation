package com.shoups.distillery.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.enums.CampaignStatus;

@Repository
public interface CampaignRepository extends JpaRepository<Campaign, Long> {
	
	List<Campaign> findByOrderByStartDateDesc();
	
	List<Campaign> findByOrderByStatusDescCreationDateDesc();
	
	List<Campaign> findByStatusOrderByCreationDateDesc(CampaignStatus status);
	
	List<Campaign> findByEndDateGreaterThanEqualAndStartDateLessThanEqual(LocalDate startDate, LocalDate endDate);
}