package com.shoups.distillery.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shoups.distillery.entity.AppUser;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {
	
    Optional<AppUser> findByUserNameOrEmail(String userName, String email);
}