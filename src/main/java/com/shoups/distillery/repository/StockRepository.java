package com.shoups.distillery.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shoups.distillery.entity.Distillation;
import com.shoups.distillery.entity.Stock;

@Repository
public interface StockRepository extends JpaRepository<Stock, Long> {
	
	Optional<Stock> findByDistillation(Distillation distillation);
}