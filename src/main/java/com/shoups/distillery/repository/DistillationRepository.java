package com.shoups.distillery.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shoups.distillery.entity.Campaign;
import com.shoups.distillery.entity.Distillation;

@Repository
public interface DistillationRepository extends JpaRepository<Distillation, Long> {
	
	List<Distillation> findByDistillationDate(LocalDate date);
	
	List<Distillation> findByDistillationDateBetween(LocalDate startDate, LocalDate endDate);
	
	List<Distillation> findByCampaignOrderByStatusAscDistillationDateDesc(Campaign campaign);
	
	Optional<Distillation> findByToken(String token);
}