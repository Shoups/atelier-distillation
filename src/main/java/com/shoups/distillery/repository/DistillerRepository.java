package com.shoups.distillery.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shoups.distillery.entity.Distiller;

@Repository
public interface DistillerRepository extends JpaRepository<Distiller, Long> {
	
    Optional<Distiller> findByEmail(String email);
}