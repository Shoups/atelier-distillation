$(document).ready(function() {
	$(":submit").click(function(event) {
		var form = $(".needs-validation")

		if (form[0].checkValidity() === false) {
			event.preventDefault()
			event.stopPropagation()
		}
		form.addClass("was-validated");
	})
})