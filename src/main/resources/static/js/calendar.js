var MORNING_TIME = "06 h - 12 h";
var AFTERNOON_TIME = "12 h - 18 h";
var DAY_TIME = "06 h - 18 h";
var NON_WORKING_DAY_BACKGROUND = "rgb(51, 51, 51)";

$(function() { 
	$(".datepicker").datepicker({
		clearBtn: true,
		format: "dd/mm/yyyy",
		language: "fr",
		autoclose: true,
		todayHighlight: true
	});
});

function handleDateClick(date, element) {
	var content = element.innerHTML;
	var booked = [];

	$("div.fc-event-main-frame", content).each(function() {
		var time = $("div.fc-event-time", $(this));
		booked.push(time.html());
	});
	
	var background = $("div.fc-bg-event", content).css("background-color");

	if (background == NON_WORKING_DAY_BACKGROUND) {
		openErrorModal("Journée non disponible", "Cette journée n'est pas disponible, veuillez sélectionner une autre journée.");
	} else {
		if ((booked.includes(MORNING_TIME) && booked.includes(AFTERNOON_TIME)) || booked.includes(DAY_TIME)) {
			openErrorModal("Journée non disponible", "Cette journée n'est plus disponible, veuillez sélectionner une autre journée.");
		} else {
			openUrlInModal("Nouvelle demande de distillation", '/calendar/add?date=' + date);
		}
	}
}

function handleAdminDateClick(date, element) {
	var content = element.innerHTML;
	var exportDate = $("div.fc-daygrid-day-top a.fc-daygrid-day-number", content).attr('aria-label');
	openExportModal('Exporter les demandes de distillations de la semaine du ' + exportDate + ' ?', '/admin/distillation/export/' + exportDate);
}