$(document).ready(function() {
	$("#modalSubmitButton").click(function(event) {
		event.preventDefault()
		event.stopPropagation()
	
		var modal = $("#modalAction");
		var url = $("#modalActionForm").attr("action");
		
		$.ajax({
			url: url,
			method: "POST",
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			success: function(responseText) {
				modal.modal("hide");
				window.location.href = responseText;
			},
			error: function(xhr) {
				modal.find("#modalActionText").html('<div class="alert alert-danger">' + xhr.responseText + '</div>');
				modal.find("#modalSubmitButton").addClass("disabled").prop(disabled, true);
			}
		});
	});
});

function openModal(title, text, buttonText, buttonCssClass, formAction) {
	var modal = $("#modalAction");
	modal.find("#modalActionLabel").html(title);
	modal.find("#modalActionText").html(text);
	modal.find("#modalActionForm").attr("action", formAction);
	modal.find("#modalSubmitButton").html(buttonText);
	modal.find("#modalSubmitButton").removeClass().addClass("btn").addClass(buttonCssClass);
	modal.modal("show");
}

function openExportModal(title, formAction) {
	var modal = $("#modalExport");
	modal.find("#modalExportLabel").html(title);
	modal.find("#modalExportPDFForm").attr("action", formAction + "?format=PDF");
	modal.find("#modalExportExcelForm").attr("action", formAction + "?format=XLS");
	modal.modal("show");
}

function openErrorModal(title, text) {
	var modal = $("#modalError");
	modal.find("#modalErrorLabel").html(title);
	modal.find("#modalErrorText").html(text);
	modal.modal("show");
}

function openUrlInModal(title, url) {
	var modalDiv = document.createElement("div");
	modalDiv.className = "modal fade";
	modalDiv.setAttribute("id", "newModal");
	modalDiv.setAttribute("tabindex", "-1");
	modalDiv.setAttribute("aria-labelledby", title);
	modalDiv.setAttribute("aria-hidden", "true"); 
	
	modalDiv.innerHTML = "\
			<div class=\"modal-dialog modal-dialog-centered modal-lg\">\
				<div class=\"modal-content\">\
					<div class=\"modal-header\">\
						<h5 class=\"modal-title\" id=\"newModalLabel\">" + title + "</h5>\
						<button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"modal\" aria-label=\"Fermer\"></button>\
					</div>\
					<div id=\"newModalText\" class=\"modal-body\"></div>\
				</div>\
			</div>";
	
	$("footer").after(modalDiv);
	$(".modal-body", modalDiv).load(url);
	$("#newModal").modal("show");
}