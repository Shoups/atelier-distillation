-- ----------------
-- Insert app_users
-- ----------------
-- TODO : ajouter email et faire un or dans repository ? (on peut s'authentifier par login ou mail)
INSERT INTO app_user (user_id, user_name, email, name, firstname, encrypted_password, enabled) VALUES (1, 'schre', 'schre@free.fr', 'Schwoerer', 'René', '$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu', 1);
INSERT INTO app_user (user_id, user_name, email, name, firstname, encrypted_password, enabled) VALUES (2, 'gsc', 'guillaume.schwoerer@gmail.com', 'Schwoerer', 'Guillaume', '$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu', 1);


-- ----------------
-- Insert user_role
-- ----------------
INSERT INTO user_role (id, user_id, role_id) VALUES (1, 1, 1);
INSERT INTO user_role (id, user_id, role_id) VALUES (2, 1, 2);
INSERT INTO user_role (id, user_id, role_id) VALUES (3, 2, 1);
INSERT INTO user_role (id, user_id, role_id) VALUES (4, 2, 2);


-- -------------------------------
-- Test data for table `campaign`
-- -------------------------------
INSERT INTO `campaign` (`id`, `name`, `start_date`, `end_date`, `status`)
VALUES (1, '2019/2020', '2019-09-01', '2020-05-31', 'CLOSED'),
       (2, '2020/2021', '2020-09-01', '2021-05-31', 'CLOSED'),
       (3, '2021/2022', '2021-09-01', '2022-05-31', 'IN_PROGRESS'),
       (4, '2021/2022', '2022-09-01', '2023-05-31', 'INCOMING');


-- -------------------------------
-- Test data for table `distiller`
-- -------------------------------
INSERT INTO `distiller` (`id`, `name`, `firstname`, `address`, `zipcode`, `city`, `phone`, `email`)
VALUES (1, 'Dusse', 'Jean-Claude', '33 Rue des Bronzés', '75000', 'Galaswinda', '06060606', 'jean-claude.dusse@bronzes.fr'),
	   (2, 'Morin', 'Bernard', '33 Rue des Bronzés', '75000', 'Galaswinda', '06060606', 'bernard.morin@bronzes.fr'),
	   (3, 'André', 'Gigi', '33 Rue des Bronzés', '75000', 'Galaswinda', '06060606', 'gigi@bronzes.fr');


-- ----------------------------------
-- Test data for table `distillation`
-- ----------------------------------
INSERT INTO `distillation` (`id`, `distillation_date`, `period`, `fruit`, `quantity`, `status`, `campaign_id`, `distiller_id`)
VALUES (1, '2021-05-18', 'MORNING', 'mirabelles', 100, 'ACCEPTED', 2, 1),
       (2, '2021-05-18', 'AFTERNOON', 'mirabelles', 100, 'ACCEPTED', 2, 2),
       (3, '2021-11-28', 'MORNING', 'mirabelles', 100, 'WAITING', 3, 3),
       (4, '2021-12-19', 'MORNING', 'mirabelles', 100, 'ACCEPTED', 3, 1),
       (5, '2021-12-19', 'AFTERNOON', 'poires', 100, 'ACCEPTED', 3, 2),
       (6, '2021-12-22', 'AFTERNOON', 'poires', 100, 'ACCEPTED', 3, 1),
       (7, '2021-12-22', 'MORNING', 'mirabelles', 100, 'WAITING', 3, 3),
       (8, '2021-12-25', 'AFTERNOON', 'mirabelles', 100, 'REFUSED', 3, 1);