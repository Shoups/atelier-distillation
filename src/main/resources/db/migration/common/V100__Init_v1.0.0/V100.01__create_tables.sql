-- ------------------------------------
-- Table structure for table `app_user`
-- ------------------------------------
CREATE TABLE `app_user` (
  `user_id` bigint NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL COMMENT 'Nom utilisateur',
  `email` varchar(100) NOT NULL COMMENT 'Adresse email',
  `name` varchar(30) NOT NULL COMMENT 'Nom',
  `firstname` varchar(30) NOT NULL COMMENT 'Prénom',
  `encrypted_password` varchar(128) NOT NULL COMMENT 'Mot de passe chiffré',
  `enabled` bit NOT NULL COMMENT 'Activé',
  PRIMARY KEY (`user_id`),
  CONSTRAINT UNIQUE (user_name),
  CONSTRAINT UNIQUE (email)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- ------------------------------------
-- Table structure for table `app_role`
-- ------------------------------------
CREATE TABLE `app_role` (
  `role_id` bigint NOT NULL AUTO_INCREMENT,
  `role_name` varchar(30) NOT NULL COMMENT 'Nom de rôle',
  PRIMARY KEY (`role_id`),
  CONSTRAINT UNIQUE (role_name)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- ------------------------------------
-- Table structure for table `user_role`
-- ------------------------------------
CREATE TABLE `user_role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `role_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT UNIQUE (user_id, role_id),
  CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES app_user(user_id),
  CONSTRAINT fk_role_id FOREIGN KEY (role_id) REFERENCES app_role(role_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- ---------------------------------------------
-- Table structure for table `persistent_logins`
-- ---------------------------------------------
CREATE TABLE `persistent_logins` (
  `username` varchar(50) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -------------------------------------
-- Table structure for table `distiller`
-- -------------------------------------
CREATE TABLE `distiller` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL COMMENT 'Nom du bouilleur de cru ou de la personne ayant la procuration',
  `firstname` varchar(30) NOT NULL COMMENT 'Prénom du bouilleur de cru ou de la personne ayant la procuration',
  `address` varchar(100) NOT NULL COMMENT 'Adresse',
  `zipcode` varchar(10) NOT NULL COMMENT 'Code postal',
  `city` varchar(50) NOT NULL COMMENT 'Ville',
  `phone` varchar(16) NULL COMMENT 'Téléphone',
  `email` varchar(100) NOT NULL COMMENT 'Adresse e-mail',
  `creation_date` datetime NOT NULL DEFAULT NOW() COMMENT 'Date de création',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `distiller` ADD UNIQUE INDEX (`email` ASC);


-- ------------------------------------
-- Table structure for table `campaign`
-- ------------------------------------
CREATE TABLE `campaign` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL COMMENT 'Nom de la campagne de distillation',
  `start_date` date NOT NULL COMMENT 'Date de début',
  `end_date` date NOT NULL COMMENT 'Date de fin',
  `status` ENUM('INCOMING', 'IN_PROGRESS', 'CLOSED') NOT NULL COMMENT 'Etat : INCOMING(A venir), IN_PROGRESS(En cours), CLOSED(Fermée)',
  `creation_date` datetime NOT NULL DEFAULT NOW() COMMENT 'Date de création',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- ----------------------------------------
-- Table structure for table `distillation`
-- ----------------------------------------
CREATE TABLE `distillation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `distillation_date` date NOT NULL COMMENT 'Date de distillation',
  `period` ENUM('MORNING', 'AFTERNOON', 'DAY') NOT NULL COMMENT 'Période de distillation : MORNING(Matin), AFTERNOON(Après-midi), DAY(Journée)',
  `fruit` varchar(30) NOT NULL COMMENT 'Type de fruits',
  `quantity` int NOT NULL COMMENT 'Quantité (kg)',
  `dsa_number` varchar(10) NULL COMMENT 'Numéro DSA',
  `status` ENUM('WAITING', 'ACCEPTED', 'REFUSED') NOT NULL COMMENT 'Etat : WAITING(En attente), ACCEPTED(Acceptée), REFUSED(Refusée)',
  `creation_date` datetime NOT NULL DEFAULT NOW() COMMENT 'Date de création',
  `campaign_id` bigint NOT NULL COMMENT 'Campagne associée',
  `token` varchar(40) NULL COMMENT 'Token',
  `distiller_id` bigint NOT NULL COMMENT 'Bouilleur de cru associé',
  PRIMARY KEY (`id`),
  CONSTRAINT fk_campaign_id FOREIGN KEY (campaign_id) REFERENCES campaign(id),
  CONSTRAINT fk_distiller_id FOREIGN KEY (distiller_id) REFERENCES distiller(id),
  CONSTRAINT UNIQUE (token)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `distillation` ADD INDEX (`distillation_date` ASC);


-- ---------------------------------
-- Table structure for table `stock`
-- ---------------------------------
CREATE TABLE `stock` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `reception_date` datetime NOT NULL DEFAULT NOW() COMMENT 'Date de réception des matières premières',
  `locality` varchar(50) NOT NULL COMMENT 'Localité',
  `quantity` int NOT NULL COMMENT 'Quantité (kg)',
  `pure_alcohol` int NOT NULL COMMENT 'Alcool pur obtenu',
  `franchised_pure_alcohol` int NOT NULL COMMENT 'Alcool pur en franchisé',
  `not_franchised_pure_alcohol` int NOT NULL COMMENT 'Alcool pur en non franchisé',
  `expedition_date` datetime NOT NULL DEFAULT NOW() COMMENT 'Date de réexpédition des produits finis',
  `creation_date` datetime NOT NULL DEFAULT NOW() COMMENT 'Date de création',
  `distillation_id` bigint NOT NULL COMMENT 'Distillation associée',
  PRIMARY KEY (`id`),
  CONSTRAINT fk_distillation_id FOREIGN KEY (distillation_id) REFERENCES distillation(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `distillation` ADD INDEX (`distillation_date` ASC);
