<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
      <p>Bonjour ${distillation.distiller.firstname} ${distillation.distiller.name},</p>
      <p>Nous avons le plaisir de confirmer que votre demande de distillation pour la date du ${distillation.distillationDate?datetime("yyyy-MM-dd")?string('dd/MM/yyyy')} ${distillation.period.label}
      a été acceptée.</p>
      <p>Conservez précieusement cet e-mail, vous en aurez besoin pour remplir la comptabilité des matières une fois la distillation terminée.
      Pour cela, il vous suffira de <a href="${link}">cliquer sur ce lien</a>.
      </p>
      <p>Cordialement,</p>
      <p><em>Atelier de Distillation de Senade</em></p>
    </body>
</html>