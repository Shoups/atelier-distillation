<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
      <p>Bonjour ${distillation.distiller.firstname} ${distillation.distiller.name},</p>
      <p>Nous avons le regret de vous informer que votre demande de distillation pour la date du ${distillation.distillationDate?datetime("yyyy-MM-dd")?string('dd/MM/yyyy')} ${distillation.period.label}
      a été refusée.</p>
      <p>Cordialement,</p>
      <p><em>Atelier de Distillation de Senade</em></p>
    </body>
</html>