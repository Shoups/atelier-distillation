<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
      <p>Bonjour,</p>
      <p>La comptabilité des matières pour la distillation du ${stock.distillation.distillationDate?datetime("yyyy-MM-dd")?string('dd/MM/yyyy')} ${stock.distillation.period.label}
      de l'utilisateur ${stock.distillation.distiller.firstname} ${stock.distillation.distiller.name} (${stock.distillation.distiller.email}) a été complétée.</p>
      <p>Vous en trouverez ci-dessous les détails :</p>
      <p>Type et quantité de fruits : <strong>${stock.distillation.fruit} (${stock.distillation.quantity} kg)</strong></p>
      <#if stock.distillation.dsaNumber??><p>N° DSA : <strong>${stock.distillation.dsaNumber}</strong></p></#if>
      <p>Date de réception des matières premières : <strong>${stock.receptionDate?datetime("yyyy-MM-dd")?string('dd/MM/yyyy')}</strong></p>
      <p>Localité : <strong>${stock.locality}</strong></p>
      <p>Quantité : <strong>${stock.quantity} kg</strong></p>
      <p>Alcool pur obtenu : <strong>${stock.pureAlcohol}</strong></p>
      <p>Alcool pur en franchisé : <strong>${stock.franchisedPureAlcohol}</strong></p>
      <p>Alcool pur en non franchisé : <strong>${stock.notFranchisedPureAlcohol}</strong></p>
      <p>Date de réexpédition des produits finis : <strong>${stock.expeditionDate?datetime("yyyy-MM-dd")?string('dd/MM/yyyy')}</strong></p>
      <p>Vous pouvez modifier la comptabilité des matières <a href="${adminLink}">en cliquant ici</a>.</p>
      <p>Cordialement,</p>
      <p><em>Atelier de Distillation de Senade</em></p>
    </body>
</html>
