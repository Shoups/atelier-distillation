<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
      <p>Bonjour,</p>
      <p>L'utilisateur ${distillationForm.firstname} ${distillationForm.name} (${distillationForm.email}) a créé une nouvelle demande de distillation
      pour la date du ${distillationForm.distillationDate?datetime("yyyy-MM-dd")?string('dd/MM/yyyy')} ${distillationForm.period.label}.</p>
      <p>Vous pouvez visualiser, accepter ou refuser cette demande en <a href="${link}">cliquant ici</a>.
      <p>Cordialement,</p>
      <p><em>Atelier de Distillation de Senade</em></p>
    </body>
</html>
