<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
      <p>Bonjour,</p>
      <p>L'utilisateur ${contactForm.name} (${contactForm.email}) a soumis le formulaire de contact dans la catégorie <em>${contactForm.type.label}</em><p>
      <p>Objet de la demande : ${contactForm.object}<p>
      <p><em>${contactForm.text}</em>
      <p>Cordialement,</p>
      <p><em>Atelier de Distillation de Senade</em></p>
    </body>
</html>
