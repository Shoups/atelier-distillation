<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
      <p>Bonjour ${distillationForm.firstname} ${distillationForm.name},</p>
      <p>Votre demande de distillation pour la date du ${distillationForm.distillationDate?datetime("yyyy-MM-dd")?string('dd/MM/yyyy')} ${distillationForm.period.label}
      a bien été créée.</p>
      <p>Vous devez attendre qu'elle soit validée. Vous recevrez bientôt un e-mail pour confirmer cette demande.</p>
      <p>Cordialement,</p>
      <p><em>Atelier de Distillation de Senade</em></p>
    </body>
</html>