<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>
	
	<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-left bg-light">
		<div class="col-md-7 mx-auto my-5 justified">
			<h1 class="display-4 font-weight-normal">Mentions l�gales</h1>
			<h5>1. Pr�sentation du site</h5>
			<p>En vertu de l'article 6 de la loi n� 2004-575 du 21 juin 2004 pour la confiance dans l'�conomie num�rique,
			il est pr�cis� aux utilisateurs du site distillerie-senade.fr l'identit� des diff�rents intervenants dans le
			cadre de sa r�alisation et de son suivi :<br/>
			Propri�taire : Ren� Schwoerer - <a class="text-muted" href="<c:url value='/contact?type=information'/>">rene.schwoerer@distillerie-senade.fr</a><br/>
			Cr�ateur du site : Guillaume Schwoerer<br/>
			Responsable publication : Guillaume Schwoerer<br/>
			Webmaster : Guillaume Schwoerer - <a class="text-muted" href="<c:url value='/contact?type=webmaster'/>">guillaume.schwoerer@distillerie-senade.fr</a><br/>
			H�bergeur : <a href="https://www.hebergeur.fr/" target="_blank">H�bergeur en attente...</a>
			</p>
			
			<h5>2. Conditions g�n�rales d'utilisation du site et des services propos�s</h5>
			<p>L'utilisation du site distillerie-senade.fr implique l'acceptation pleine et enti�re des conditions g�n�rales
			d'utilisation ci-apr�s d�crites. Ces conditions d'utilisation sont susceptibles d'�tre modifi�es ou compl�t�es � tout moment,
			les utilisateurs du site sont donc invit�s � les consulter de mani�re r�guli�re.<br/>
			Ce site est normalement accessible � tout moment aux utilisateurs. Une interruption pour raison de maintenance technique
			peut �tre toutefois d�cid�e par le distillerie-senade.fr, qui s'efforcera alors de communiquer pr�alablement aux utilisateurs
			les dates et heures de l'intervention.<br/>
			Le site est mis � jour r�guli�rement. De la m�me fa�on, les mentions l�gales peuvent �tre modifi�es � tout moment :
			elles s'imposent n�anmoins � l'utilisateur qui est invit� � s'y r�f�rer le plus souvent possible afin d'en prendre connaissance.
			</p>
			
			<h5>3. Description des services fournis</h5>
			<p>Le site distillerie-senade.fr a pour objet de fournir une information concernant l'ensemble des activit�s de la soci�t�.<br/>
			Distillerie-senade.fr s'efforce de fournir sur le site des informations aussi pr�cises que possible.
			Toutefois, il ne pourra �tre tenue responsable des omissions, des inexactitudes et des carences dans la mise � jour,
			qu'elles soient de son fait ou du fait des tiers partenaires qui lui fournissent ces informations.<br/>
			Toutes les informations indiqu�es sur le site sont donn�es � titre indicative, et sont susceptibles d'�voluer.
			Par ailleurs, les renseignements figurant sur le site ne sont pas exhaustifs. Ils sont donn�s sous r�serve
			de modifications ayant �t� apport�es depuis leur mise en ligne.
			</p>
			
			<h5>4. Limitations contractuelles sur les donn�es techniques</h5>
			<p>Le site Internet ne pourra �tre tenu responsable de dommages mat�riels li�s � son utilisation.
			De plus, l'utilisateur du site s'engage � acc�der au site en utilisant un mat�riel r�cent,
			ne contenant pas de virus et avec un navigateur de derni�re g�n�ration mis-�-jour
			</p>
			
			<h5>5. Propri�t� intellectuelle et contrefa�ons</h5>
			<p>Distillerie-senade.fr est propri�taire des droits de propri�t� intellectuelle ou d�tient les droits d'usage
			sur tous les �l�ments accessibles sur le site, notamment les textes, images, graphismes, logo, ic�nes, sons, logiciels.<br/>
			Toute reproduction, repr�sentation, modification, publication, adaptation de tout ou partie des �l�ments du site,
			quel que soit le moyen ou le proc�d� utilis�, est interdite, sauf autorisation �crite pr�alable de distillerie-senade.fr.<br/>
			Toute exploitation non autoris�e du site ou de l'un quelconque des �l�ments qu'il contient sera consid�r�e comme constitutive
			d'une contrefa�on et poursuivie conform�ment aux dispositions des articles L.335-2 et suivants du Code de Propri�t� Intellectuelle.
			</p>
			
			<h5>6. Limitations de responsabilit�</h5>
			<p>Distillerie-senade.fr ne pourra �tre tenue responsable des dommages directs et indirects caus�s au mat�riel de l'utilisateur,
			lors de l'acc�s au site distillerie-senade.fr, et r�sultant soit de l'utilisation d'un mat�riel ne r�pondant pas aux sp�cifications
			indiqu�es au point 4, soit de l'apparition d'un bug ou d'une incompatibilit�.<br/>
			Distillerie-senade.fr ne pourra �galement �tre tenue responsable des dommages indirects (tels par exemple qu'une perte de march� ou perte d'une chance)
			cons�cutifs � l'utilisation du site.<br/>
			Des espaces interactifs (possibilit� de poser des questions dans l'espace contact) sont � la disposition des utilisateurs.
			Distillerie-senade.fr se r�serve le droit de supprimer, sans mise en demeure pr�alable, tout contenu d�pos� dans cet espace qui contreviendrait
			� la l�gislation applicable en France, en particulier aux dispositions relatives � la protection des donn�es.
			Le cas �ch�ant, distillerie-senade.fr se r�serve �galement la possibilit� de mettre en cause la responsabilit� civile et/ou p�nale de l'utilisateur,
			notamment en cas de message � caract�re raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilis� (texte, photographie...).
			</p>
			
			<h5>7. Gestion des donn�es personnelles</h5>
			<p>En France, les donn�es personnelles sont notamment prot�g�es par la loi n� 78-87 du 6 janvier 1978, la loi n� 2004-801 du 6 ao�t 2004,
			l'article L. 226-13 du Code p�nal et la Directive Europ�enne du 24 octobre 1995.<br/>
			A l'occasion de l'utilisation du site distillerie-senade.fr, peuvent �tre recueillies :
			l'URL des liens par l'interm�diaire desquels l'utilisateur a acc�d� au site, le fournisseur d'acc�s de l'utilisateur,
			l'adresse de protocole Internet (IP) de l'utilisateur.<br/>
			En tout �tat de cause distillerie-senade.fr ne collecte des informations personnelles relatives � l'utilisateur
			que pour le besoin de certains services propos�s par le site. L'utilisateur fournit ces informations en toute connaissance de cause,
			notamment lorsqu'il proc�de par lui-m�me � leur saisie. Il est alors pr�cis� � l'utilisateur du site l'obligation ou non de fournir ces informations.<br/>
			Conform�ment aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative � l'informatique, aux fichiers et aux libert�s,
			tout utilisateur dispose d'un droit d'acc�s, de rectification et d'opposition aux donn�es personnelles le concernant, en effectuant sa demande �crite et sign�e,
			accompagn�e d'une copie du titre d'identit� avec signature du titulaire de la pi�ce, en pr�cisant l'adresse � laquelle la r�ponse doit �tre envoy�e.<br/>
			Aucune information personnelle de l'utilisateur du site n'est publi�e � l'insu de l'utilisateur, �chang�e, transf�r�e, c�d�e ou vendue sur un support quelconque � des tiers.
			Seule l'hypoth�se du rachat de distillerie-senade.fr et de ses droits permettrait la transmission des dites informations � l'�ventuel acqu�reur
			qui serait � son tour tenu de la m�me obligation de conservation et de modification des donn�es vis � vis de l'utilisateur du site.<br/>
			Les bases de donn�es sont prot�g�es par les dispositions de la loi du 1er juillet 1998 transposant la directive 96/9 du 11 mars 1996
			relative � la protection juridique des bases de donn�es.
			</p>

			<h5>8. Liens hypertextes et cookies</h5>
			<p>Le site distillerie-senade.fr contient un certain nombre de liens hypertextes vers d'autres sites.
			Cependant, distillerie-senade.fr n'a pas la possibilit� de v�rifier le contenu des sites ainsi visit�s, et n'assumera en cons�quence aucune responsabilit� de ce fait.<br/>
			La navigation sur le site est susceptible de provoquer l'installation de cookie(s) sur l'ordinateur de l'utilisateur.
			Un cookie est un fichier de petite taille, qui ne permet pas l'identification de l'utilisateur,
			mais qui enregistre des informations relatives � la navigation d'un ordinateur sur un site.
			Les donn�es ainsi obtenues visent � faciliter la navigation ult�rieure sur le site, et ont �galement vocation � permettre diverses mesures de fr�quentation.<br/>
			Le refus d'installation d'un cookie peut entra�ner l'impossibilit� d'acc�der � certains services.
			L'utilisateur peut toutefois configurer son ordinateur de la mani�re suivante, pour refuser l'installation des cookies :<br/>
			Sous Internet Explorer : onglet outil / options internet. Cliquez sur Confidentialit� et choisissez Bloquer tous les cookies. Validez sur Ok.<br/>
			Sous Mozilla Firefox : menu Firefox, puis Pr�f�rences. Onglet Vie priv�e. Choisissez utiliser les param�tres personnalis�s pour l'historique dans R�gles de conservation puis d�cocher Accepter les cookies. Validez sur Ok.<br/>
			Sous Google Chrome : menu Chrome, puis Pr�f�rences. Onglet Options avanc�es puis Param�tres de contenu. Dans Cookies, cochez Interdire � tous les sites de stocker des donn�es. Validez sur Ok.<br/>
			Sous Safari : menu Safari, puis Pr�f�rences. Onglet Confidentialit�, cochez Toujours dans Bloquer les cookies. Validez sur Ok.
			</p>

			<h5>9. Droit applicable et attribution de juridiction</h5>
			<p>Tout litige en relation avec l'utilisation du site distillerie-senade.fr est soumis au droit fran�ais.
			Il est fait attribution exclusive de juridiction aux tribunaux comp�tents de Paris.
			</p>
			
			<h5>10. Les principales lois concern�es</h5>
			<p>Loi n� 78-87 du 6 janvier 1978, notamment modifi�e par la loi n� 2004-801 du 6 ao�t 2004 relative � l'informatique, aux fichiers et aux libert�s.<br/>
			Loi n� 2004-575 du 21 juin 2004 pour la confiance dans l'�conomie num�rique.
			</p>
			
			<h5>11. Lexique</h5>
			<p>Utilisateur : Internaute se connectant, utilisant le site susnomm�.<br/>
			Informations personnelles : � les informations qui permettent, sous quelque forme que ce soit, directement ou non,
			l'identification des personnes physiques auxquelles elles s'appliquent � (article 4 de la loi n� 78-17 du 6 janvier 1978).
			</p>
		</div>
	</div>
	
	<%@include file="include/footer.jsp"%>
	</body>
</html>