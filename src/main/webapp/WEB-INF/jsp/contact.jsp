<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
<script type="text/javascript" src="<c:url value='/js/form.js'/>"></script>
</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>
	
	<div class="container-fluid bg-light">
		<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 ">
			<div class="col-sm-12 col-lg-8 mx-auto">
				<h3 class="display-5 font-weight-normal text-center">Nous contacter</h3>
				<div class="row justify-content-center">
					<div class="col-sm-12 col-lg-6">
						<form:form id="contactForm" action="/contact" modelAttribute="contactForm" method="post" cssClass="needs-validation">
							<div class="container">
								<div class="row g-3 mb-2 justify-content-center">
									<div class="col-lg-auto">
										<form:errors path="" cssClass="invalid-feedback d-block" />
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="type" cssClass="col-form-label">Cat�gorie :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:select path="type" ssClass="form-control" items="${contactTypes}" itemLabel="label" />
										<div class="col-2 invalid-feedback">Veuillez choisir une cat�gorie</div>
										<form:errors path="type" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>
							
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="name" cssClass="col-form-label">Nom et pr�nom :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:input path="name" id="name" placeholder="Nom et pr�nom" cssClass="form-control" required="required" />
										<div class="col-2 invalid-feedback">Veuillez saisir vos nom et pr�nom</div>
										<form:errors path="name" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="email" cssClass="col-form-label">E-mail :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:input path="email" id="email" placeholder="E-mail" cssClass="form-control" required="required" />
										<div class="col-2 invalid-feedback">Veuillez saisir votre e-mail</div>
										<form:errors path="email" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="object" cssClass="col-form-label">Objet :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:input path="object" id="object" placeholder="Objet" cssClass="form-control" required="required" />
										<div class="col-2 invalid-feedback">Veuillez saisir l'objet</div>
										<form:errors path="object" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="text" cssClass="col-form-label">Texte :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:textarea path="text" placeholder="Votre texte" cssClass="form-control" required="required"/>
										<div class="col-2 invalid-feedback"></div>
										<form:errors path="text" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>

								<div class="row g-3 mb-2">
									<div class="col-12 text-center">
										<button type="submit" name="submit" id="submit" class="btn btn-primary">Envoyer le formulaire</button>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-lg-3"></div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%@include file="include/footer.jsp"%>
	</body>
</html>