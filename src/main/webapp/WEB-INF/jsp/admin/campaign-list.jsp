<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="../include/resources.jsp"%>
</head>
	<body>
	<%@include file="../include/header.jsp"%>
	<%@include file="../include/menu.jsp"%>
	
	<div class="container-fluid bg-light">
		<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 ">
			<div class="col-sm-12 col-lg-8 mx-auto">
				<h3 class="display-5 font-weight-normal text-center">Gestion des campagnes de distillation</h3>
				<div class="row text-center">
					<p>Vous pouvez g�rer les campagnes de distillation ici.</p>
					<p><a href="<c:url value='/admin/campaign/add'/>" class="btn btn-primary" role="button">Cr�er une campagne</a>
					
					<div class="row g-3 mb-2 justify-content-center">
						<div class="col-lg-auto invalid-feedback d-block">${errorMessage}</div>
					</div>
					
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Nom</th>
								<th>Date de d�but</th>
								<th>Date de fin</th>
								<th>Date de cr�ation</th>
								<th>�tat</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${campaigns}" var="campaign">
							<tr>
								<td><c:out value="${campaign.name}" /></td>
								<td><tags:formatLocalDate date="${campaign.startDate}" /></td>
								<td><tags:formatLocalDate date="${campaign.endDate}" />
								<td><tags:formatLocalDateTime date="${campaign.creationDate}" /></td>
								<tags:campaignStatus campaign="${campaign}" />
								<tags:campaignAction campaign="${campaign}" />
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<%@include file="../include/footer.jsp"%>
	
	<div class="modal fade" id="modalAction" tabindex="-1" aria-labelledby="Veuillez confirmer l'action" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalActionLabel"></h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fermer"></button>
				</div>
				<div id="modalActionText" class="modal-body"></div>
				<div class="modal-footer">
					<form:form id="modalActionForm" method="post">
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
						<button type="submit" id="modalSubmitButton"></button>
					</form:form>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="modalExport" tabindex="-1" aria-labelledby="Veuillez choisir le format d'export" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalExportLabel"></h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fermer"></button>
				</div>
				<div class="modal-body">Veuillez choisir le format d'export.</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
					<form:form id="modalExportPDFForm" method="post">
						<button type="submit" class="btn btn-danger">PDF</button>
					</form:form>
					<form:form id="modalExportExcelForm" method="post">
						<button type="submit" class="btn btn-success">Excel</button>
					</form:form>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>