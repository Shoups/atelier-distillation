<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="../include/resources.jsp"%>
</head>
	<body>
	<%@include file="../include/header.jsp"%>
	<%@include file="../include/menu.jsp"%>
	
	<div class="container-fluid bg-light">
		<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 ">
			<div class="col-sm-12 col-lg-8 mx-auto">
				<h3 class="display-5 font-weight-normal text-center">Gestion des demandes de distillation</h3>
				<div class="row text-center">
					<p>Vous pouvez g�rer les demandes de distillation ici.</p>
					
					<div class="row g-3 mb-2 justify-content-center">
						<div class="col-lg-auto invalid-feedback d-block">${errorMessage}</div>
					</div>
					
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Nom et pr�nom du bouilleur de cr�</th>
								<th>Adresse et commune</th>
								<th>T�l�phone</th>
								<th>Adresse e-mail</th>
								<th>Date</th>
								<th>Horaire</th>
								<th>Type de fruits</th>
								<th>Quantit�</th>
								<th>N�DSA</th>
								<th>Etat</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${distillations}" var="distillation">
							<tr>
								<td><c:out value="${distillation.distiller.firstname} ${distillation.distiller.name}" /></td>
								<td><c:out value="${distillation.distiller.address} ${distillation.distiller.zipcode} ${distillation.distiller.city}" /></td>
								<td><c:out value="${distillation.distiller.phone}" /></td>
								<td><c:out value="${distillation.distiller.email}" /></td>
								<td><tags:formatLocalDate date="${distillation.distillationDate}" /></td>
								<td><c:out value="${distillation.period.label}" /></td>
								<td><c:out value="${distillation.fruit}" /></td>
								<td><c:out value="${distillation.quantity}" /></td>
								<td><c:out value="${distillation.dsaNumber}" /></td>
								<tags:distillationStatus distillation="${distillation}" />
								<tags:distillationAction distillation="${distillation}" />
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<%@include file="../include/footer.jsp"%>
	
	<div class="modal fade" id="modalAction" tabindex="-1" aria-labelledby="Veuillez confirmer l'action" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalActionLabel"></h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fermer"></button>
				</div>
				<div id="modalActionText" class="modal-body"></div>
				<div class="modal-footer">
					<form:form id="modalActionForm" method="post">
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
						<button type="submit" id="modalSubmitButton"></button>
					</form:form>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>