<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="../include/resources.jsp"%>
</head>
	<body>
	<%@include file="../include/header.jsp"%>
	<%@include file="../include/menu.jsp"%>
	
	<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
		<div class="col-md-5 mx-auto my-5">
			<h1 class="display-4 font-weight-normal">Espace d'administration</h1>
			<p class="lead font-weight-normal">Bienvenue sur l'espace d'administration <c:out value="${user.firstname}" /> <c:out value="${user.name}" /> !</p>
			<a class="btn btn-outline-secondary" href="<c:url value='/admin/calendar'/>">Calendrier</a>
		</div>
	</div>
	
	<%@include file="../include/footer.jsp"%>
	</body>
</html>