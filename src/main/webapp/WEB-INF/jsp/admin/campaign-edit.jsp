<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="../include/resources.jsp"%>
<script type="text/javascript" src="<c:url value='/js/form.js'/>"></script>
</head>
	<body>
	<%@include file="../include/header.jsp"%>
	<%@include file="../include/menu.jsp"%>
	
	<c:choose>
		<c:when test="${action == 'add'}">
			<c:set var="formAction" value="add"></c:set>
			<c:set var="buttonText" value="Cr�er la campagne"></c:set>
		</c:when>
		<c:when test="${action == 'edit'}">
			<c:set var="formAction" value="edit"></c:set>
			<c:set var="buttonText" value="Modifier la campagne"></c:set>
		</c:when>
	</c:choose>
	
	<div class="container-fluid bg-light">
		<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 ">
			<div class="col-sm-12 col-lg-8 mx-auto">
				<h3 class="display-5 font-weight-normal text-center">Cr�er une campagne de distillation</h3>
				<div class="row justify-content-center">
					<div class="col-sm-12 col-lg-6">
						<form:form id="campaignForm" action="/admin/campaign/${formAction}" modelAttribute="campaign" method="post" cssClass="needs-validation">
							<form:hidden path="id"/>
							<form:hidden path="status"/>
							<form:hidden path="creationDate"/>
							<div class="container">
								<div class="row g-3 mb-2 justify-content-center">
									<div class="col-lg-auto">
										<form:errors path="" cssClass="invalid-feedback d-block" />
									</div>
								</div>
							
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="name" cssClass="col-form-label">Nom :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:input path="name" id="name" placeholder="Nom" cssClass="form-control" required="required" />
										<div class="col-2 invalid-feedback">Veuillez saisir le nom</div>
										<form:errors path="name" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>

								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="startDate" cssClass="col-form-label">Date de d�but :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<div class="datepicker date input-group">
											<form:input path="startDate" placeholder="JJ/MM/AAAA" cssClass="form-control" required="required" />
											<div class="input-group-append">
												<span class="input-group-text p-2 calendar"><i class="fa fa-calendar"></i></span>
											</div>
											<div class="col-2 invalid-feedback">Veuillez saisir la date de d�but</div>
											<form:errors path="startDate" cssClass="col-2 invalid-feedback d-block" />
										</div>
									</div>
								</div>

								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="endDate" cssClass="col-form-label">Date de fin :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<div class="datepicker date input-group">
											<form:input path="endDate" placeholder="JJ/MM/AAAA" cssClass="form-control" required="required" />
											<div class="input-group-append">
												<span class="input-group-text p-2 calendar"><i class="fa fa-calendar"></i></span>
											</div>
											<div class="col-2 invalid-feedback">Veuillez saisir la date de fin</div>
											<form:errors path="endDate" cssClass="col-2 invalid-feedback d-block" />
										</div>
									</div>
								</div>
								
								<div class="row g-3 mb-2">
									<div class="col-12 text-center">
										<button type="submit" name="submit" id="submit" class="btn btn-primary">${buttonText}</button>
										<a href="<c:url value='/admin/campaign/list'/>" class="btn btn-secondary" role="button">Retour � la liste</a>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-lg-3"></div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%@include file="../include/footer.jsp"%>
	</body>
</html>