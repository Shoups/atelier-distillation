<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="../include/resources.jsp"%>
<script type="text/javascript" src="<c:url value='/js/form.js'/>"></script>
</head>
	<body>
	<%@include file="../include/header.jsp"%>
	<%@include file="../include/menu.jsp"%>
	
	<div class="container">
		<div class="col-md-5 mx-auto my-5">
			<p class="lead font-weight-normal">D�tail de la demande de distillation</p>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Nom et pr�nom du bouilleur de cru :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${distillation.distiller.firstname} ${distillation.distiller.name} "/></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Adresse :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${distillation.distiller.address} ${distillation.distiller.zipcode} ${distillation.distiller.city}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">T�l�phone :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${distillation.distiller.phone}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Adresse e-mail :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${distillation.distiller.email}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Date de distillation souhait�e :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><tags:formatLocalDate date="${distillation.distillationDate}" /> ${distillation.period.label}</strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Type de fruits :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${distillation.fruit}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Quantit� de fruits (Kg) :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${distillation.quantity}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">N� DSA :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${distillation.dsaNumber}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Date de cr�ation de la demande :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><tags:formatLocalDateTime date="${distillation.creationDate}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Etat :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${distillation.status.label}" /></strong></div>
			</div>
		</div>
		
		<div class="col-12 text-center">
			<c:choose>
				<c:when test="${distillation.status == 'WAITING'}">
					<c:url value="/admin/distillation/accept/${distillation.id}" var="acceptUrl" scope="request" />
					<c:url value="/admin/distillation/refuse/${distillation.id}" var="refuseUrl" scope="request" />
					
					<a href="#" class="btn btn-outline-success" aria-label="Accepter" onclick="openModal('Accepter la demande ?', '�tes-vous s�r d\'accepter cette demande ?', 'Accepter', 'btn-outline-success', '${acceptUrl}');">Accepter</a>
					<a href="#" class="btn btn-outline-danger" aria-label="Refuser" onclick="openModal('Refuser la demande ?', '�tes-vous s�r de refuser la demande ?', 'Refuser', 'btn-outline-danger', '${refuseUrl}');">Refuser</a>
				</c:when>
				<c:when test="${distillation.status == 'ACCEPTED'}">
					<fmt:parseDate value="${distillation.distillationDate}" pattern="yyyy-MM-dd" var="distillationDate" type="date"/>
					<fmt:parseDate value="${now}" pattern="yyyy-MM-dd" var="nowDate" type="date"/>
					<c:if test="${distillationDate < nowDate}">
						<a class="btn btn-info" href="<c:url value='/admin/stock/${distillation.id}' />" aria-label="Remplir la comptabilit� des mati�res">Remplir la comptabilit� des mati�res</a>
					</c:if>
				</c:when>
			</c:choose>
		</div>
		
		<div class="col-12 text-center mt-3">
			<a href="<c:url value='/admin/distillation/list'/>" class="btn btn-secondary" role="button">Retour � la liste</a>
		</div>
	</div>

	<%@include file="../include/footer.jsp"%>
	
	<div class="modal fade" id="modalAction" tabindex="-1" aria-labelledby="Veuillez confirmer l'action" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalActionLabel"></h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fermer"></button>
				</div>
				<div id="modalActionText" class="modal-body"></div>
				<div class="modal-footer">
					<form:form id="modalActionForm" method="post">
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
						<button type="submit" id="modalSubmitButton"></button>
					</form:form>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>