<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>
	
	<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
		<div class="col-md-5 mx-auto my-5">
			<h3 class="mb-3">Le formulaire de contact a �t� envoy� avec succ�s !</h3>
			<p><a class="btn btn-outline-primary" href="<c:url value='/home'/>">Retour � l'accueil</a></p>
		</div>
	</div>
	
	<%@include file="include/footer.jsp"%>
	</body>
</html>