<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
<link rel="stylesheet" media="all" type="text/css" href="<c:url value='/lib/fullcalendar/main.min.css'/>" rel="stylesheet" />
<script src="<c:url value='/lib/fullcalendar/main.min.js'/>"></script>
<script src="<c:url value='/lib/fullcalendar/locales/fr.js'/>"></script>
<script>
	$(document).ready(function() {
		var calendarEl = document.getElementById('calendar');
		var calendar = new FullCalendar.Calendar(calendarEl, {
			initialView: "dayGridMonth",
			locale: "fr",
		    timeZone: "local",
		    displayEventTime: ${displayEventTime},
		    displayEventEnd: ${displayEventTime},
		    
			eventSources : [{
				url: "<c:url value='/calendar/events'/>",
				method: "GET",
				failure: function() {
					openErrorModal("Erreur", "Une erreur s'est produite lors de la r�cup�ration des �v�nements");
				}
			}],
			
			dateClick: function(info) {
				handleDateClick(info.dateStr, info.dayEl);
			}
		});
		calendar.render();
	});
</script>
</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>
	
	<div class="position-relative overflow-hidden text-center bg-light">
		<div class="col-md-5 mx-auto my-5">
			<h1 class="display-4 font-weight-normal">Calendrier</h1>
			<p class="lead font-weight-normal">R�servez votre journ�e de distillation :</p>
			<div id="calendar"></div>
			<p>L�gende :</p>
			<p>
				<c:forEach items="${campaignCaptions}" var="campaignCaption">
					<p><a class="btn btn-light disabled" style="background-color: ${campaignCaption.value}; border-color: ${campaignCaption.value}">${campaignCaption.key}</a></p>
				</c:forEach>
			</p>
			<p>
				<c:forEach items="${distillationCaptions}" var="distillationCaption">
					<p><a class="btn btn-primary disabled" style="background-color: ${distillationCaption.value}; border-color: ${distillationCaption.value}">${distillationCaption.key}</a></p>
				</c:forEach>
			</p>
		</div>
	</div>

	<%@include file="include/footer.jsp"%>
	</body>
</html>