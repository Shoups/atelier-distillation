<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
<script type="text/javascript" src="<c:url value='/js/form.js'/>"></script>
</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>

	<div class="container">
		<div class="col-md-5 mx-auto my-5">
			<p class="lead font-weight-normal">La comptabilit� des mati�res a �t� enregistr�e avec succ�s</p>
			<c:if test="${!isAdmin}">
				<p>Vous pouvez la modifier si besoin en cliquant de nouveau sur le lien envoy� par e-mail</p>
			</c:if>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Nom et pr�nom du bouilleur de cru :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${stock.distillation.distiller.firstname} ${stock.distillation.distiller.name} "/></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Adresse :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${stock.distillation.distiller.address} ${stock.distillation.distiller.zipcode} ${stock.distillation.distiller.city}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">T�l�phone :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${stock.distillation.distiller.phone}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Adresse e-mail :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${stock.distillation.distiller.email}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Date de distillation :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><tags:formatLocalDate date="${stock.distillation.distillationDate}" /> ${stock.distillation.period.label}</strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Type et quantit� de fruits :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${stock.distillation.fruit} (${stock.distillation.quantity} kg)" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">N� DSA :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${stock.distillation.dsaNumber}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Date de r�ception des mati�res premi�res :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><tags:formatLocalDate date="${stock.receptionDate}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Localit� :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${stock.locality}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Quantit� :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${stock.quantity} kg" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Alcool pur obtenu :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${stock.pureAlcohol}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Alcool pur en franchis� :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${stock.franchisedPureAlcohol}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Alcool pur en non franchis� :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><c:out value="${stock.notFranchisedPureAlcohol}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2 justify-content-end">
			<div class="col-lg-auto">
				<div class="col-form-label">Date de r�exp�dition des produits finis :</div>
			</div>
			<div class="col-lg-6">
				<div class="col-form-label"><strong><tags:formatLocalDate date="${stock.expeditionDate}" /></strong></div>
			</div>
		</div>
		
		<div class="row g-3 mb-2">
			<div class="col-12 text-center">
				<a href="<c:url value='stock/${stock.distillation.token}'/>" class="btn btn-secondary" role="button">Modifier</a>
			</div>
		</div>
	</div>
	
	<%@include file="include/footer.jsp"%>
	</body>
</html>