<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>
	
	<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-left bg-light">
		<div id="legal" class="col-md-7 mx-auto my-5">
			<h1 class="display-4 font-weight-normal">Nous trouver</h1>
			<p>L'Atelier de Distillation de Senade se situe au 347 Chemin des Rapailles, 88220 Hadol</p>
			<iframe src="https://www.google.com/maps/d/embed?mid=15fPdTC-0ImHxI5zRrNYH-aANyuCnUau1&ehbc=2E312F" width="800" height="600"></iframe>
		</div>
	</div>
	
	<%@include file="include/footer.jsp"%>
	</body>
</html>