<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
<link rel="stylesheet" href="<c:url value='/lib/simple-lightbox/simple-lightbox.css?v2.11.0'/>" />
<script src="<c:url value='/lib/simple-lightbox/simple-lightbox.js?v2.11.0'/>"></script>
<script>
	$(document).ready(function() {
		var $gallery = new SimpleLightbox('.gallery a', {});
	});
</script>

</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>
	
	<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
		<div class="container col-md-5 mx-auto my-5">
			<h1 class="display-4 font-weight-normal mb-5">Galerie</h1>
			<div class="gallery">
				<a href="img/gallery/01.jpg"><img src="img/gallery/thumbs/01.jpg" alt="" title="La fa�ade de la distillerie"/></a>
				<a href="img/gallery/02.jpg"><img src="img/gallery/thumbs/02.jpg" alt="" title="L'alambic"/></a>
				<a href="img/gallery/03.jpg"><img src="img/gallery/thumbs/03.jpg" alt="" title="Le condenseur"/></a>
				<a href="img/gallery/04.jpg"><img src="img/gallery/thumbs/04.jpg" alt="" title=""/></a>
				<a href="img/gallery/05.jpg"><img src="img/gallery/thumbs/05.jpg" alt="" title=""/></a>
				<a href="img/gallery/06.jpg"><img src="img/gallery/thumbs/06.jpg" alt="" title="Florian s'appr�te � distiller"/></a>
				<a href="img/gallery/07.jpg"><img src="img/gallery/thumbs/07.jpg" alt="" title=""/></a>
				<a href="img/gallery/08.jpg"><img src="img/gallery/thumbs/08.jpg" alt="" title=""/></a>
				<a href="img/gallery/09.jpg"><img src="img/gallery/thumbs/09.jpg" alt="" title=""/></a>
				<a href="img/gallery/10.jpg"><img src="img/gallery/thumbs/10.jpg" alt="" title="Le four � pain et pizza"/></a>
				<a href="img/gallery/11.jpg"><img src="img/gallery/thumbs/11.jpg" alt="" title="Quelques produits locaux"/></a>
				<a href="img/gallery/12.jpg"><img src="img/gallery/thumbs/12.jpg" alt="" title=""/></a>
			</div>
		</div>
	</div>
	
	<%@include file="include/footer.jsp"%>
	</body>
</html>
