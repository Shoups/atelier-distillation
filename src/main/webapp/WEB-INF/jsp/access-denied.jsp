<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>
	
	<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
		<div class="col-md-5 mx-auto my-5">
			<h1 class="display-4 font-weight-normal">
				<c:choose>
					<c:when test="${isAdmin}">
						<c:out value=" ${user.firstname} ${user.name}" />, vous n'avez pas la permission d'afficher cette page.
					</c:when>
					<c:otherwise>
						Vous n'avez pas la permission d'afficher cette page.
					</c:otherwise>
				</c:choose>
			</h1>
		</div>
	</div>
	
	<%@include file="include/footer.jsp"%>
	</body>
</html>