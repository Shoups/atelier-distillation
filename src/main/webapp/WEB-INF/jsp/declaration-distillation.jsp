<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>
	
	<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-left bg-light">
		<div id="legal" class="col-md-7 mx-auto my-5">
			<h1 class="display-4 font-weight-normal">D�claration de distillation � imprimer</h1>
			<p>Lorsque vous distillez, vous devez faire une d�claration de distillation � la douane. Vous pouvez t�l�charger cette d�claration ici et l'imprimer.</p>
			<p>Le num�ro d'alambic � renseigner est le 4911.</p>
			<p class="mt-5"><a class="btn btn-outline-secondary" href="<c:url value='/pdf/Declaration_distillation.pdf'/>" target="_blank">T�l�charger la d�claration de distillation au format PDF</a></p>
		</div>
	</div>
	
	<%@include file="include/footer.jsp"%>
	</body>
</html>