<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
<script type="text/javascript" src="<c:url value='/js/form.js'/>"></script>
</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>

	<c:choose>
		<c:when test="${action == 'add'}">
			<c:set var="buttonText" value="Cr�er la comptabilit� des mati�res"></c:set>
		</c:when>
		<c:when test="${action == 'edit'}">
			<c:set var="buttonText" value="Modifier la comptabilit� des mati�res"></c:set>
		</c:when>
	</c:choose>
	
	<div class="container-fluid bg-light">
		<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 ">
			<div class="col-sm-12 col-lg-8 mx-auto">
				<h3 class="display-5 font-weight-normal text-center">Remplir la comptabilit� des mati�res</h3>
				<div class="row justify-content-center">
					<div class="col-sm-12 col-lg-8">
						<form:form id="campaignForm" action="/stock" modelAttribute="stock" method="post" cssClass="needs-validation">
							<form:hidden path="id"/>
							<form:hidden path="distillation.id"/>
							<div class="container">
								<div class="row g-3 mb-2 justify-content-center">
									<div class="col-lg-auto">
										<form:errors path="" cssClass="invalid-feedback d-block" />
									</div>
								</div>
							
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<span class="col-form-label">Nom et pr�nom du bouilleur de cru :</span>
									</div>
									<div class="col-lg-6">
										<span class="col-form-label"><c:out value="${distillation.distiller.firstname} ${distillation.distiller.name}" /></span>
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<span class="col-form-label">Num�ro DSA :</span>
									</div>
									<div class="col-lg-6">
										<span class="col-form-label"><c:out value="${distillation.dsaNumber}" /></span>
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<span class="col-form-label">Date de distillation :</span>
									</div>
									<div class="col-lg-6">
										<span class="col-form-label"><tags:formatLocalDate date="${distillation.distillationDate}" /> <c:out value="${distillation.period.label}" /></span>
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<span class="col-form-label">Type et quantit� de fruits :</span>
									</div>
									<div class="col-lg-6">
										<span class="col-form-label"><c:out value="${distillation.fruit} (${distillation.quantity} kg)" /></span>
									</div>
								</div>
							
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="receptionDate" cssClass="col-form-label">Date de r�ception des mati�res premi�res :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<div class="datepicker date input-group">
											<form:input path="receptionDate" placeholder="JJ/MM/AAAA" cssClass="form-control" required="required" />
											<div class="input-group-append">
												<span class="input-group-text p-2 calendar"><i class="fa fa-calendar"></i></span>
											</div>
											<div class="col-2 invalid-feedback">Veuillez saisir la date de r�ception des mati�res premi�res</div>
											<form:errors path="receptionDate" cssClass="col-2 invalid-feedback d-block" />
										</div>
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="locality" cssClass="col-form-label">Localit� :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:input path="locality" id="name" placeholder="Localit�" cssClass="form-control" required="required" />
										<div class="col-2 invalid-feedback">Veuillez saisir la localit�</div>
										<form:errors path="locality" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="quantity" cssClass="col-form-label">Quantit� (kg) :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:input path="quantity" id="name" placeholder="Nom" cssClass="form-control" required="required" />
										<div class="col-2 invalid-feedback">Veuillez saisir la quantit�</div>
										<form:errors path="quantity" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="pureAlcohol" cssClass="col-form-label">Alcool pur obtenu :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:input path="pureAlcohol" id="name" placeholder="Nom" cssClass="form-control" required="required" />
										<div class="col-2 invalid-feedback">Veuillez saisir l'alcool pur obtenu</div>
										<form:errors path="pureAlcohol" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="franchisedPureAlcohol" cssClass="col-form-label">Alcool pur en franchis� :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:input path="franchisedPureAlcohol" id="name" placeholder="Nom" cssClass="form-control" required="required" />
										<div class="col-2 invalid-feedback">Veuillez saisir l'alcool pur en franchis�</div>
										<form:errors path="franchisedPureAlcohol" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="notFranchisedPureAlcohol" cssClass="col-form-label">Alcool pur en non franchis� :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:input path="notFranchisedPureAlcohol" id="name" placeholder="Nom" cssClass="form-control" required="required" />
										<div class="col-2 invalid-feedback">Veuillez saisir l'alcool pur en non franchis�</div>
										<form:errors path="notFranchisedPureAlcohol" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="notFranchisedPureAlcohol" cssClass="col-form-label">Alcool pur en non franchis� :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<form:input path="notFranchisedPureAlcohol" id="name" placeholder="Nom" cssClass="form-control" required="required" />
										<div class="col-2 invalid-feedback">Veuillez saisir l'alcool pur en non franchis�</div>
										<form:errors path="notFranchisedPureAlcohol" cssClass="col-2 invalid-feedback d-block" />
									</div>
								</div>
								
								<div class="row g-3 mb-2 justify-content-end">
									<div class="col-lg-auto">
										<form:label path="expeditionDate" cssClass="col-form-label">Date de r�exp�dition des produits finis :</form:label>
									</div>
									<div class="col-lg-6 has-validation">
										<div class="datepicker date input-group">
											<form:input path="expeditionDate" placeholder="JJ/MM/AAAA" cssClass="form-control" required="required" />
											<div class="input-group-append">
												<span class="input-group-text p-2 calendar"><i class="fa fa-calendar"></i></span>
											</div>
											<div class="col-2 invalid-feedback">Veuillez saisir la date de r�exp�dition des produits finis</div>
											<form:errors path="expeditionDate" cssClass="col-2 invalid-feedback d-block" />
										</div>
									</div>
								</div>

								<div class="row g-3 mb-2">
									<div class="col-12 text-center">
										<button type="submit" name="submit" id="submit" class="btn btn-primary">${buttonText}</button>
										<a href="<c:url value='/home'/>" class="btn btn-secondary" role="button">Annuler</a>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-lg-3"></div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%@include file="include/footer.jsp"%>
	</body>
</html>