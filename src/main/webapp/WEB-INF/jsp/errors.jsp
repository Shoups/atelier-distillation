<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>
	
	<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
		<div class="col-md-5 mx-auto my-5">
			<h1 class="display-4 font-weight-normal">Les erreurs suivantes se sont produites :</h1>
			<p class="lead font-weight-normal">${errorMessages}</p>
		</div>
	</div>
	
	<%@include file="include/footer.jsp"%>
	</body>
</html>