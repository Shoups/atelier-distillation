	<nav class="site-header sticky-top py-1">
		<div class="container d-flex flex-column flex-md-row justify-content-between">
			<c:choose>
				<c:when test="${isAdmin}">
					<a class="py-2 d-none d-md-inline-block" href="<c:url value='/admin/home'/>">Accueil</a>
					<a class="py-2 d-none d-md-inline-block" href="<c:url value='/admin/calendar'/>">Calendrier</a>
					<a class="py-2 d-none d-md-inline-block" href="<c:url value='/admin/campaign/list'/>">Campagnes de distillation</a>
					<a class="py-2 d-none d-md-inline-block" href="<c:url value='/admin/distillation/list'/>">Demandes de distillation</a>
					<form:form id="logoutform" action="/logout" method="post">
						<a class="py-2 d-none d-md-inline-block" href="#" onclick="$('#logoutform').submit()">D�connexion</a>
					</form:form>
				</c:when>
				<c:otherwise>
					<a class="py-2 d-none d-md-inline-block" href="<c:url value='/home'/>">Accueil</a>
					<a class="py-2 d-none d-md-inline-block" href="<c:url value='/gallery'/>">Galerie</a>
					<a class="py-2 d-none d-md-inline-block" href="<c:url value='/calendar'/>">R�server</a>
					<a class="py-2 d-none d-md-inline-block" href="<c:url value='/admin/home'/>">Espace d'administration</a>
				</c:otherwise>
			</c:choose>
		</div>
	</nav>
