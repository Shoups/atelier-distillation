	<footer class="container py-5">
		<div class="row">
			<div class="col-12 col-md">
				<ul class="list-unstyled text-small">
					<li><small class="d-block mb-3 text-muted">&copy; 2023</small></li>
					<li><small class="d-block mb-3 text-muted">Atelier de Distillation de Senade</small></li>
				</ul>
			</div>
			<div class="col-6 col-md">
				<h5>Contact</h5>
				<ul class="list-unstyled text-small">
					<li><a class="text-muted" href="<c:url value='/contact'/>">Nous contacter</a></li>
					<li><a class="text-muted" href="<c:url value='/find-us'/>">Nous trouver</a></li>
					<li><a class="text-muted" href="<c:url value='/contact?type=site_problem'/>">Probl�me sur le site</a></li>
				</ul>
			</div>
			<div class="col-6 col-md">
				<h5>A propos</h5>
					<ul class="list-unstyled text-small">
					<li><a class="text-muted" href="<c:url value='/rules'/>">R�glement</a></li>
					<li><a class="text-muted" href="<c:url value='/legal'/>">Mentions l�gales</a></li>
					<li><a class="text-muted" href="<c:url value='/declaration-distillation'/>">D�claration de distillation � imprimer</a></li>
				</ul>
			</div>
		</div>
	</footer>
	
	<div class="modal fade" id="modalError" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalErrorLabel"></h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fermer"></button>
				</div>
				<div class="modal-body">
					<div id="modalErrorText" class="alert alert-danger"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
				</div>
			</div>
		</div>
	</div>