<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<meta name="description" content="Site de la distillerie de Senade">
<meta name="author" content="Guillaume Schwoerer">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="csrf-token" content="${_csrf.token}">

<link rel="stylesheet" media="all" type="text/css" href="<c:url value='/webjars/bootstrap-datepicker/css/bootstrap-datepicker.min.css'/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value='/webjars/bootstrap/css/bootstrap.min.css'/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value='/css/styles.css'/>" />
<link rel="stylesheet" media="all" type="text/css" href="<c:url value='/css/calendar.css'/>" />

<script type="text/javascript" src="<c:url value='/webjars/jquery/jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/webjars/bootstrap/js/bootstrap.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/webjars/bootstrap-datepicker/js/bootstrap-datepicker.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/webjars/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js'/>" charset="UTF-8"></script>
<script type="text/javascript" src="<c:url value='/js/calendar.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/modal.js'/>"></script>
<script src="https://kit.fontawesome.com/f3717cb381.js" crossorigin="anonymous"></script>
