<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

	<form:form id="distillationForm" action="/calendar/add" modelAttribute="distillationForm" method="post" cssClass="needs-validation">
		<form:hidden path="campaignId"/>
		<form:hidden path="distillationDate"/>
		
<!-- 			<div class="row g-3 mb-2 justify-content-center"> -->
<!-- 				<div class="col-lg-auto"> -->
<%-- 					<form:errors path="" cssClass="invalid-feedback d-block" /> --%>
<!-- 				</div> -->
<!-- 			</div> -->
<!-- TODO : gestion des erreurs ? -->
		
		<div class="container">
			<p><strong>Veuillez remplir ce formulaire pour cr�er votre demande distillation pour la date du <tags:formatLocalDate date="${distillationForm.distillationDate}" /></strong></p>
			<div class="row g-3 mb-2 justify-content-end">
				<div class="col-lg-auto">
					<form:label path="name" cssClass="col-form-label">Nom du bouilleur de cru :</form:label>
					<div id="nameHelp" class="form-text">Ou de la personne ayant la procuration</div>
				</div>
				<div class="col-lg-6 has-validation">
					<form:input path="name" id="name" placeholder="Nom" cssClass="form-control" required="required" />
					<div class="col-2 invalid-feedback">Veuillez saisir le nom</div>
					<form:errors path="name" cssClass="col-2 invalid-feedback d-block" />
				</div>
			</div>
			
			<div class="row g-3 mb-2 justify-content-end">
				<div class="col-lg-auto">
					<form:label path="firstname" cssClass="col-form-label">Pr�nom du bouilleur de cru :</form:label>
					<div id="firstnameHelp" class="form-text">Ou de la personne ayant la procuration</div>
				</div>
				<div class="col-lg-6 has-validation">
					<form:input path="firstname" id="firstname" placeholder="Pr�nom" cssClass="form-control" required="required" />
					<div class="col-2 invalid-feedback">Veuillez saisir le pr�nom</div>
					<form:errors path="firstname" cssClass="col-2 invalid-feedback d-block" />
				</div>
			</div>
		
			<div class="row g-3 mb-2 justify-content-end">
				<div class="col-lg-auto">
					<form:label path="address" cssClass="col-form-label">Adresse :</form:label>
				</div>
				<div class="col-lg-6 has-validation">
					<form:input path="address" id="address" placeholder="Adresse" cssClass="form-control" required="required" />
					<div class="col-2 invalid-feedback">Veuillez saisir l'adresse</div>
					<form:errors path="address" cssClass="col-2 invalid-feedback d-block" />
				</div>
			</div>
	
			<div class="row g-3 mb-2 justify-content-end">
				<div class="col-lg-auto">
					<form:label path="zipcode" cssClass="col-form-label">Code postal :</form:label>
				</div>
				<div class="col-lg-6 has-validation">
					<form:input path="zipcode" id="zipcode" placeholder="Code postal" cssClass="form-control" required="required" />
					<div class="col-2 invalid-feedback">Veuillez saisir le code postal</div>
					<form:errors path="zipcode" cssClass="col-2 invalid-feedback d-block" />
				</div>
			</div>
	
			<div class="row g-3 mb-2 justify-content-end">
				<div class="col-lg-auto">
					<form:label path="city" cssClass="col-form-label">Ville :</form:label>
				</div>
				<div class="col-lg-6 has-validation">
					<form:input path="city" id="city" placeholder="Ville" cssClass="form-control" required="required" />
					<div class="col-2 invalid-feedback">Veuillez saisir la ville</div>
					<form:errors path="city" cssClass="col-2 invalid-feedback d-block" />
				</div>
			</div>
			
			<div class="row g-3 mb-2 justify-content-end">
				<div class="col-lg-auto">
					<form:label path="phone" cssClass="col-form-label">T�l�phone :</form:label>
				</div>
				<div class="col-lg-6 has-validation">
					<form:input path="phone" id="phone" placeholder="T�l�phone" cssClass="form-control" required="required" />
					<div class="col-2 invalid-feedback">Veuillez saisir le t�l�phone</div>
					<form:errors path="phone" cssClass="col-2 invalid-feedback d-block" />
				</div>
			</div>
			
			<div class="row g-3 mb-2 justify-content-end">
				<div class="col-lg-auto">
					<form:label path="email" cssClass="col-form-label">Adresse e-mail :</form:label>
				</div>
				<div class="col-lg-6 has-validation">
					<form:input type="email" path="email" id="email" placeholder="Adresse e-mail" cssClass="form-control" required="required" />
					<div class="col-2 invalid-feedback">Veuillez saisir une adresse e-mail valide</div>
					<form:errors path="email" cssClass="col-2 invalid-feedback d-block" />
				</div>
			</div>
	
			<div class="row g-3 mb-2 justify-content-end">
				<div class="col-lg-auto">
					<form:label path="period" cssClass="col-form-label">Horaire :</form:label>
				</div>
				<div class="col-lg-6 has-validation">
					<form:select path="period" ssClass="form-control" items="${periods}" itemLabel="label" />
					<div class="col-2 invalid-feedback">Veuillez choisir un horaire</div>
					<form:errors path="period" cssClass="col-2 invalid-feedback d-block" />
				</div>
			</div>
			
			<div class="row g-3 mb-2 justify-content-end">
				<div class="col-lg-auto">
					<form:label path="fruit" cssClass="col-form-label">Type de fruits :</form:label>
				</div>
				<div class="col-lg-6 has-validation">
					<form:input path="fruit" id="fruit" placeholder="Type de fruits" cssClass="form-control" required="required" />
					<div class="col-2 invalid-feedback">Veuillez saisir le type de fruits</div>
					<form:errors path="fruit" cssClass="col-2 invalid-feedback d-block" />
				</div>
			</div>
			
			<div class="row g-3 mb-2 justify-content-end">
				<div class="col-lg-auto">
					<form:label path="quantity" cssClass="col-form-label">Quantit� de fruits (Kg) :</form:label>
				</div>
				<div class="col-lg-6 has-validation">
					<form:input path="quantity" id="quantity" placeholder="Quantit� de fruits" cssClass="form-control" required="required" />
					<div class="col-2 invalid-feedback">Veuillez saisir la quantit� de fruits</div>
					<form:errors path="quantity" cssClass="col-2 invalid-feedback d-block" />
				</div>
			</div>
			
			<div class="row g-3 mb-2 justify-content-end">
				<div class="col-lg-auto">
					<form:label path="dsaNumber" cssClass="col-form-label">N� DSA :</form:label>
				</div>
				<div class="col-lg-6 has-validation">
					<form:input path="dsaNumber" id="dsaNumber" placeholder="N� DSA" cssClass="form-control" />
					<div class="col-2 invalid-feedback">Veuillez saisir le N� DSA</div>
					<form:errors path="dsaNumber" cssClass="col-2 invalid-feedback d-block" />
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="submit" name="submit" id="submit" class="btn btn-primary">Cr�er la demande</button>
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
			</div>
		</div>
		<div class="col-sm-6 col-lg-3"></div>
	</form:form>
