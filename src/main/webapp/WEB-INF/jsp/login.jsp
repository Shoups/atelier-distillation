<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
</head>
<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>
	
	<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
		<div class="col-md-5 mx-auto my-5">
			<h1 class="display-4 font-weight-normal">Connexion</h1>
			<p class="lead font-weight-normal">Veuillez vous connecter pour acc�der � cette ressource.</p>
			
			<c:if test="${not empty loginError}">
				<div class="alert alert-danger"><c:out value="${loginError}"></c:out></div>
			</c:if>

			<form:form id="loginform" action="/j_spring_security_check" method="post">
				<div class="row g-3 mb-2">
					<div class="col-4">
						<label for="username" class="col-form-label">Nom d'utilisateur :</label>
					</div>
					<div class="col-5">
						<input type="text" name="username" class="form-control" placeholder="Nom d'utilisateur">
					</div>
				</div>
				
				<div class="row g-3 mb-2">
					<div class="col-4">
						<label for="password" class="col-form-label">Mot de passe :</label>
					</div>
					<div class="col-5">
						<input type="password" name="password" class="form-control" placeholder="Mot de passe">
					</div>
				</div>
				
				<div class="row g-3 mb-2">
					<div class="col-4">
						<label for="remember-me" class="col-form-label">Se souvenir de moi ?</label>
					</div>
					<div class="col-5">
						<input type="checkbox" name="remember-me" class="form-check-input form-control p-0 mt-2">
					</div>
				</div>
				
				<button type="submit" name="submit" class="btn btn-primary">Se connecter</button>
			</form:form>
		</div>
	</div>
	<%@include file="include/footer.jsp"%>
</body>
</html>