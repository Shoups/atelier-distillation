<!DOCTYPE html>
<html>
<head>
<title>Atelier de Distillation de Senade</title>
<%@include file="include/resources.jsp"%>
</head>
	<body>
	<%@include file="include/header.jsp"%>
	<%@include file="include/menu.jsp"%>
	
	<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-left bg-light">
		<div class="col-md-7 mx-auto my-5 justified">
			<h4 class="display-6 font-weight-normal">R�glement d'utilisation de l'Atelier de Distillation de Senade</h4>
			<h5>Objet</h5>
			<p>Le pr�sent r�glement a pour objet de d�terminer les conditions dans lesquelles doit �tre utilis� l'atelier de distillation.<br/>
			L'Atelier de distillation est un lieu priv� mis � disposition par le propri�taire du b�timent en p�riode l�gale de distillation,
			des Hadolais, ainsi que des personnes ext�rieures � la commune, en contrepartie du versement d'une redevance dont le montant
			est d�termin� par le Syndicat Agricole de HADOL.
			</p>
			
			<h5>P�riode d'ouverture</h5>
			<p>L'atelier est ouvert en fonction des dates communiqu�es chaque ann�e par la Direction R�gionale des Douanes et Droits Indirects de Lorraine.<br/>
			La distillation est permise, � l'exception des dimanches et jours f�ri�s, de 6 h � 19 h, pendant la p�riode l�gale d'ouverture.<br/>
			L'atelier n'est pas accessible le dimanche et les jours f�ri�s.
			</p>
			
			<h5>Utilisation du local</h5>
			<p>Deux places de stationnement sont mises � disposition de l'utilisateur (pas plus). 
			La plate-forme en demi-cercle � l'entr�e du local servira � charger et d�charger les f�ts et facilitera le nettoyage en cas de fuite de mati�re.
			L'utilisateur engage sa pleine et enti�re responsabilit� quant � l'utilisation de l'alambic dont il est cens� conna�tre le mode d'emploi.
			L'utilisateur est enti�rement responsable, de l'acc�s des visiteurs � l'atelier pendant les op�rations de distillation.
			La porte d'acc�s au local doit rester ouverte lors de la vidange des mati�res distill�es.<br/>
			L'utilisateur est enti�rement responsable des infractions envers la l�gislation li�e � la production d'alcool.
			L'utilisateur assurera le nettoyage des lieux et du mat�riel mis � disposition (voir liste en annexe).
			Il est �galement charg� de l'extinction des lumi�res au moment de son d�part, et de la fermeture du local.
			Il devra informer le responsable de l'atelier de tout probl�me rencontr�, tant pour les locaux que pour le mat�riel mis � disposition.
			En cas de d�t�rioration et/ou manque de mat�riel (inventaire en annexe), facturation sera faite aux personnes en fonction du pr�judice subi et/ou la caution ne sera pas revers�e.<br/>
			Toute sous-location est interdite.<br/>
			Le Syndicat Agricole de Hadol a le droit de retirer l'autorisation d'utiliser les lieux en cas de fraude
			ou de manquement � la bonne utilisation du local ou du mat�riel, sans �tre tenu � un d�dommagement ou au remboursement de l'acompte,
			de m�me qu'il n'est pas tenu � verser d'indemnit� quelconque si, pour des raisons de s�curit�, de force majeure, d'ordre public ou de non-conformit� � l'objet initial de la mise � disposition,
			il se trouvait dans l'impossibilit� de mettre l'atelier � disposition.<br/>
			L'accord de mise � disposition ne dispense pas les b�n�ficiaires de requ�rir par ailleurs toutes les autorisations administratives �ventuellement n�cessaires,
			notamment le service des imp�ts. Le Syndicat Agricole de Hadol ne saurait en aucun cas �tre rendu responsable du d�faut des d�clarations
			et tenu au paiement des taxes, imp�ts ou p�nalit�s dus par l'utilisateur.
			</p>
			
			<h5>Mise � disposition et r�servation</h5>
			<p>Les r�servations se font aupr�s du responsable de l'atelier, aux heures ouvrables, 15 jours au moins avant la date pr�vue d'utilisation.
			Une r�servation n'est d�finitive que si le r�glement a �t� effectu� et l'attestation des douanes fournie. Le retrait des cl�s s'effectue la veille aupr�s du responsable.
			Le retour des cl�s s'effectue d�s le lendemain du jour d'utilisation ou d�s le lundi suivant en cas d'utilisation le samedi en main propre au responsable de l'atelier.
			L'�tat des lieux sera assur� par l'utilisateur entrant, auquel il appartiendra de pr�venir le responsable de l'atelier en cas de constat de d�faut ou de d�g�t. 
			</p>
			
			<h5>Assurance et responsabilit�s</h5>
			<p>Il appartient � l'utilisateur de contracter les assurances n�cessaires permettant de garantir sa responsabilit� d'utilisateur � l'�gard du propri�taire du local,
			du Syndicat Agricole et des tiers. L'utilisateur s'engage � pouvoir justifier qu'il est titulaire d'un tel contrat avant tout d�but d'occupation effective
			au moyen d'une attestation �tablissant l'�tendue de la responsabilit� garantie.<br/>
			Le b�timent est mis � disposition en son bon �tat habituel sans que l'utilisateur puisse �ventuellement exercer un recours contre le propri�taire du b�timent ou le Syndicat Agricole de Hadol
			pour raison soit de mauvais �tat, soit de vice apparent ou cach�. Le propri�taire du b�timent et le Syndicat Agricole de Hadol d�clinent toute responsabilit� en cas de vol,
			sinistre ou d�t�rioration du mat�riel et des objets de toute nature entrepos�s ou utilis�s dans la salle par l'utilisateur.<br/>
			Ils ne pourront en aucun cas �tre tenus responsables des cons�quences d'une mauvaise utilisation de l'alambic.<br/>
			L'utilisateur et le Syndicat Agricole de Hadol renoncent � tous recours contre le propri�taire.
			</p>
			
			<h5>Fonctionnement</h5>
			<p>L'alambic est mis � disposition par journ�e enti�re pour un m�me utilisateur. La journ�e n'est pas fractionnable et toute journ�e commenc�e est due.<br/>
			Les utilisateurs am�neront le bois (cass�) n�cessaire pour distiller. Ne pas casser de bois sur place.<br/>
			Apr�s distillation, les r�sidus de cuite (marcs de raisins, fruits divers, racines, cendres) sont enlev�s par l'utilisateur.<br/>
			Les f�ts de mati�re et r�cipients de produit fini doivent �tre �tiquet�s au nom du bouilleur de cru.
			</p>
			
			<h5>Tarifs</h5>
			<p>Les tarifs sont fix�s par d�cision du Syndicat Agricole de Hadol et r�visables.<br/>
			Pour la campagne 2022/2023, les tarifs sont les suivants :
			<ul>
				<li>5 &euro; par cuite (adh�rents au Syndicat Agricole de Hadol ou adh�rents FDSEA)</li>
				<li>Prix de la carte d'adh�rent au Syndicat Agricole de Hadol : 15 &euro;</li>
			</ul>
			<p>Le paiement est exig� � la r�servation. Un ch�que de caution de 200 &euro; sera �galement exig�. Celui-ci sera rendu apr�s l'�tat des lieux �tabli par l'entrant suivant.
			</p>
			
			<p>Un exemplaire de ce r�glement sera affich� � l'atelier et un autre attach� � la cl� du local.
			La remise de la cl� se fera en main propre et vaudra acceptation du pr�sent r�glement.
			</p>
			<p>R�glement adopt� par le propri�taire du b�timent et le Syndicat Agricole de Hadol 15 mars 2016.</p>
			<p class="mt-5"><a class="btn btn-outline-secondary" href="<c:url value='/pdf/Reglement-2022-2023.pdf'/>" target="_blank">T�l�charger le r�glement au format PDF</a></p>
		</div>
	</div>
	
	<%@include file="include/footer.jsp"%>
	</body>
</html>