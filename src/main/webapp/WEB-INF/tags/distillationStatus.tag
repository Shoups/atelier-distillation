<%@ attribute name="distillation" type="com.shoups.distillery.entity.Distillation" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
	<c:when test="${distillation.status == 'WAITING'}">
		<td class="table-info"><c:out value="${distillation.status.label}" /></td>
	</c:when>
	<c:when test="${distillation.status == 'ACCEPTED'}">
		<td class="table-success"><c:out value="${distillation.status.label}" /></td>
	</c:when>
	<c:when test="${distillation.status == 'REFUSED'}">
		<td class="table-secondary"><c:out value="${distillation.status.label}" /></td>
	</c:when>
</c:choose>
