<%@ attribute name="campaign" type="com.shoups.distillery.entity.Campaign" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/admin/campaign/start/${campaign.id}" var="startUrl" scope="request" />
<c:url value="/admin/campaign/close/${campaign.id}" var="closeUrl" scope="request" />
<c:url value="/admin/campaign/delete/${campaign.id}" var="deleteUrl" scope="request" />
<c:url value="/admin/campaign/export/${campaign.id}" var="exportUrl" scope="request" />

<c:choose>
	<c:when test="${campaign.status == 'INCOMING'}">
		<td>
			<a href="#" aria-label="D�marrer" onclick="openModal('D�marrer la campagne ?', '�tes-vous s�r de d�marrer la campagne ${campaign.name} ?', 'D�marrer', 'btn-outline-success', '${startUrl}');"><i aria-hidden="true" class="fa fa-play fa-fw text-success" title="D�marrer"></i></a>
			<a href="/admin/campaign/edit/${campaign.id}" aria-label="Modifier"><i aria-hidden="true" class="fa fa-pencil-square-o fa-fw text-primary" title="Modifier"></i></a>
			<a href="#" aria-label="Supprimer" onclick="openModal('Supprimer la campagne ?', '�tes-vous s�r de supprimer la campagne ${campaign.name} ?', 'Supprimer', 'btn-outline-danger', '${deleteUrl}');"><i aria-hidden="true" class="fa fa-times fa-fw text-danger" title="Supprimer"></i></a>
		</td>
	</c:when>
	<c:when test="${campaign.status == 'IN_PROGRESS'}">
		<td>
			<a href="/admin/campaign/edit/${campaign.id}" aria-label="Modifier"><i aria-hidden="true" class="fa fa-pencil-square-o fa-fw text-primary" title="Modifier"></i></a>
			<a href="#" aria-label="Cl�turer" onclick="openModal('Cl�turer la campagne ?', '�tes-vous s�r de cl�turer la campagne ${campaign.name} ?', 'Cl�turer', 'btn-outline-warning', '${closeUrl}');"><i aria-hidden="true" class="fa fa-stop fa-fw text-muted" title="Cl�turer"></i></a>
			<a href="#" aria-label="Exporter" onclick="openExportModal('Exporter les distillations de la campagne ${campaign.name} ?', '${exportUrl}');"><i aria-hidden="true" class="fa fa-download fa-fw text-muted" title="Exporter"></i></a>
		</td>
	</c:when>
	<c:when test="${campaign.status == 'CLOSED'}">
		<td>
			<a href="#" aria-label="Supprimer" onclick="openModal('Supprimer la campagne ?', '�tes-vous s�r de supprimer la campagne ${campaign.name} ?', 'Supprimer', 'btn-outline-danger', '${deleteUrl}');"><i aria-hidden="true" class="fa fa-times fa-fw text-danger" title="Supprimer"></i></a>
			<a href="#" aria-label="Exporter" onclick="openExportModal('Exporter les distillations de la campagne ${campaign.name} ?', '${exportUrl}');"><i aria-hidden="true" class="fa fa-download fa-fw text-muted" title="Exporter"></i></a>
		</td>
	</c:when>
</c:choose>
