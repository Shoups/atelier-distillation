<%@ attribute name="distillation" type="com.shoups.distillery.entity.Distillation" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url value="/admin/distillation/accept/${distillation.id}" var="acceptUrl" scope="request" />
<c:url value="/admin/distillation/refuse/${distillation.id}" var="refuseUrl" scope="request" />

<td>
	<a href="/admin/distillation/view/${distillation.id}" aria-label="Voir"><i aria-hidden="true" class="fa fa-info fa-fw text-primary" title="Voir"></i></a>
	
	<c:choose>
		<c:when test="${distillation.status == 'WAITING'}">
			<a href="#" aria-label="Accepter" onclick="openModal('Accepter la demande ?', '�tes-vous s�r d\'accepter cette demande ?', 'Accepter', 'btn-outline-success', '${acceptUrl}');"><i aria-hidden="true" class="fa fa-check fa-fw text-success" title="Accepter"></i></a>
			<a href="#" aria-label="Refuser" onclick="openModal('Refuser la demande ?', '�tes-vous s�r de refuser la demande ?', 'Refuser', 'btn-outline-danger', '${refuseUrl}');"><i aria-hidden="true" class="fa fa-times fa-fw text-danger" title="Refuser"></i></a>
		</c:when>
		<c:when test="${distillation.status == 'ACCEPTED'}">
			<fmt:parseDate value="${distillation.distillationDate}" pattern="yyyy-MM-dd" var="distillationDate" type="date"/>
			<fmt:parseDate value="${now}" pattern="yyyy-MM-dd" var="nowDate" type="date"/>
			<c:if test="${distillationDate < nowDate}">
				<a href="/stock/${distillation.token}" aria-label="Remplir la comptabilit� des mati�res"><i aria-hidden="true" class="fa fa-calculator fa-fw text-info" title="Remplir la comptabilit� des mati�res"></i></a>
			</c:if>
		</c:when>
	</c:choose>
</td>