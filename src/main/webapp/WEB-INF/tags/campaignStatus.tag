<%@ attribute name="campaign" type="com.shoups.distillery.entity.Campaign" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
	<c:when test="${campaign.status == 'INCOMING'}">
		<td class="table-info"><c:out value="${campaign.status.label}" /></td>
	</c:when>
	<c:when test="${campaign.status == 'IN_PROGRESS'}">
		<td class="table-success"><c:out value="${campaign.status.label}" /></td>
	</c:when>
	<c:when test="${campaign.status == 'CLOSED'}">
		<td class="table-secondary"><c:out value="${campaign.status.label}" /></td>
	</c:when>
</c:choose>
